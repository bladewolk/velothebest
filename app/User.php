<?php

namespace App;

use App;
use App\Models\Order;
use App\Models\OrderProduct;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'region', 'address', 'permission', 'city'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function orders()
    {
        return $this->hasManyThrough(OrderProduct::class, Order::class);
    }

    public function scopeWithOrders($query)
    {
        return $query->with(['orders' => function ($q) {
            $q->leftJoin('products', 'order_products.product_id', '=', 'products.id')
                ->addselect('products.slug', 'order_products.*')
                ->leftJoin('products_localizations', 'order_products.product_id', '=', 'products_localizations.product_id')
                ->where('products_localizations.locale', App::getLocale())
                ->addSelect('products_localizations.name');
        }]);
    }
}
