<?php

namespace App\Providers;
use Storage;
use App\Models\Features;
use App\Models\FeatureVariant;
use App\Models\Variant;
use App\Models\Picture;
use Illuminate\Support\ServiceProvider;
use App\Models\Product;
use App\Http\Controllers\Admin\AdminProductsController;
use App\Models\Order;
use League\Glide\ServerFactory;
use App\Models\Banner;
use App\Models\News;
use App\Models\Categorie;
use App\Models\Option;
use App\Models\Brand;
use App\Models\Label;
use App\Models\Delivery;
use App\Models\Payment;
use App\Models\Slider;
use File;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // new
        Banner::deleting(function($model){
            if (File::exists(public_path('uploads/blocks/'). $model->image))
                File::delete(public_path('uploads/blocks/'). $model->image);
        });

        News::deleting(function($model){
            $model->images->each(function($image){
                $image->delete();
            });
            $model->localization()->delete();
        });

        Categorie::deleting(function($model){
            $model->images->each(function($image){
                $image->delete();
            });
            $model->localization()->delete();
        });

        Features::deleting(function($model){
            $model->variants()->each(function($variant) use ($model){
                $variant->localization()->delete();
                $variant->delete();
            });
            $model->variants()->detach();
            $model->localization()->delete();
        });

        Variant::deleting(function($model){
            $model->localization()->delete();
            FeatureVariant::where('variant_id', $model->id)->delete();
        });

        Option::deleting(function($model){
            $model->localization()->delete();
        });

        Brand::deleting(function($model){
            $model->images->first()->delete();
            $model->localization()->delete();
        });

        Label::deleting(function($model){
            $model->localization()->delete();
        });

        Delivery::deleting(function($model){
            $model->localization()->delete();
        });

        Payment::deleting(function($model){
            $model->localization()->delete();
        });

        Product::deleting(function($model){
            $model->images->each(function($image){
                $image->delete();
            });
            $model->localization()->delete();
            $model->comments()->delete();
            $model->features()->delete();
            $model->characteristicks()->delete();
            $model->associated()->delete();
        });
        
        Slider::deleting(function($model){
            Picture::deleteFromStorage($model->image);
            Picture::deleteFromStorage($model->image_background);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('League\Glide\Server', function ($app) {
            $storageDriver = Storage::getDriver();

            return ServerFactory::create([
                'source' => $storageDriver,
                'cache' => $storageDriver,
                'cache_path_prefix' => '.cache',
                'base_url' => '/img/'
            ]);
        });
    }
}
