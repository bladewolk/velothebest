<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Excel;
use DB;
use File;
use App\Models\Product;

class ImportProducts implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;

    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $reader = Excel::load(public_path('uploads/temp/catalog.csv'), function ($reader) {
        }, 'windows-1251');
        $catalog = [];
        foreach ($reader->all() as $index => $row) {
            $catalog[intval($row->code)] = $row;
        }
        File::delete(public_path('uploads/temp/catalog.csv'));
//        Price.csv
        $reader = Excel::load(public_path('uploads/temp/price.csv'), function ($reader) {
        }, 'windows-1251');
        $price = [];
        foreach ($reader->all() as $index => $row) {
            $price[intval($row->code)] = $row->price;
        }
        File::delete(public_path('uploads/temp/price.csv'));

//        Stock.csv
        $reader = Excel::load(public_path('uploads/temp/stock.csv'), function ($reader) {
        }, 'windows-1251');
        $stock = ['in_stock'];
        foreach ($reader->all() as $index => $row) {
            $stock[intval($row->first())] = $row->last();
        }
        File::delete(public_path('uploads/temp/stock.csv'));
//MERGE
        foreach ($catalog as $key => $val) {
            if (isset($price[$key]))
                $val['price'] = $price[$key];
            if (isset($stock[$key]))
                $val['in_stock'] = $stock[$key];
            $catalog[$key] = $val;
        }
//        WRITE TO DATABASE
        foreach ($catalog as $item) {
            $product = Product::create([
                'code' => intval($item->code),
                'cat_id' => 2,
                'in_stock' => intval($item->in_stock),
                'price' => is_numeric($item->price) ? $item->price : intval($item->price),
                'visible' => true,
                'status' => intval($item->in_stock) > 0 ? 1 : 0,
            ]);
            $product->localization()->create([
                'name' => $item->name,
                'locale' => 'ru'
            ]);
            $product->localization()->create([
                'name' => $item->name,
                'locale' => 'uk'
            ]);
        }

    }
}
