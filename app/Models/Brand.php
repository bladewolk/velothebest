<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;

class Brand extends Model
{
    protected $fillable = ['slug'];

    public function localization()
    {
        return $this->hasMany(BrandsLocalization::class);
    }

    public function images()
    {
        return $this->morphMany(Picture::class, 'picturetable');
    }

    public function scopeConvertForSelect($query)
    {
        return $query->leftJoin('brands_localizations', 'brands.id', '=', 'brands_localizations.brand_id')
            ->whereLocale('ru')
            ->select('brands.*', 'brands_localizations.name as name')
            ->pluck('name', 'id')
            ->toArray();
    }

    public function scopeJoinLocalization($query)
    {
        return $query->leftJoin('brands_localizations', 'brands.id', '=', 'brands_localizations.brand_id')
            ->where('brands_localizations.locale', App::getLocale())
            ->select('brands.*', 'brands_localizations.name', 'brands_localizations.body', 'brands_localizations.meta_title',
                'brands_localizations.meta_keywords', 'brands_localizations.meta_description');
    }
}
