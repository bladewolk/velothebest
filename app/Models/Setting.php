<?php

namespace App\Models;

use File;
use Image;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['site_name', 'description', 'phones', 'info', 'slider_speed', 'currency_uah', 'facebook', 'instagram', 'youtube'];

    public function images()
    {
        return $this->morphMany(Picture::class, 'picturetable');
    }

    public function getPhonesAttribute($item)
    {
        $phones = [];
        foreach (explode(';', $item) as $item) {
            $phones[] = [
                'phone' => preg_replace('/(\(\d+\))/', "<span>$1</span>", $item),
                'tel' => preg_replace('/\s/', '', $item),
            ];
        }
        return $phones;
    }

    public function setPhonesAttribute($item)
    {
        $this->attributes['phones'] = implode($item, ';');
    }

}
