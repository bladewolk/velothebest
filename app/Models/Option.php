<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    public function localization(){
        return $this->hasMany(OptionLocalization::class, 'option_id');
    }

    public function scopeConvertForSelect($query){
    	return $query->leftJoin('option_localizations', 'options.id', '=', 'option_localizations.option_id')
            ->select('option_localizations.name', 'options.*')
            ->whereLocale('ru')
            ->orderBy('option_localizations.name', 'asc')
            ->pluck('name', 'id')
            ->put(0, 'Нет')
            ->toArray();
    }
}
