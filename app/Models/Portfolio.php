<?php

namespace App\Models;

use App\Http\Controllers\Admin\AdminPortfolioController;
use Illuminate\Database\Eloquent\Model;
use Image;


class Portfolio extends Model
{
    protected $fillable = ['name', 'body', 'slug', 'meta_title', 'meta_keywords', 'meta_description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function images()
    {
        return $this->morphMany(Picture::class, 'picturetable');
    }

    public static function boot()
    {
        parent::boot();

        self::deleting(function ($model) {
            $imageIDs = $model->images()->pluck('id');
            Picture::deleteImages($imageIDs, AdminPortfolioController::getParams());
        });
    }

}
