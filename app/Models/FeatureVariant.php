<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeatureVariant extends Model
{
    protected $fillable = ['feature_id', 'variant_id'];
}
