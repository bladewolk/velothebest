<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;

class Delivery extends Model
{
    public function localization(){
    	return $this->hasMany(DeliveryLocalization::class);
    }

    public function scopeJoinLocalization($query){
    	return $query->leftJoin('delivery_localizations', 'deliveries.id', '=', 'delivery_localizations.delivery_id')
    		->where('delivery_localizations.locale', App::getLocale())
    		->addSelect('delivery_localizations.name', 'deliveries.id');
    }
}
