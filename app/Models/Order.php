<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\OrderProduct;

class Order extends Model
{
    protected $fillable = ['status', 'name', 'phone', 'total_price', 'email', 'region', 'city', 'comment', 'delivery_id',
        'payment_id', 'currency', 'user_id'];

    public function products()
    {
        return $this->hasMany(OrderProduct::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // Join product name and slug to relation by product_id
    public function scopeWithProduct($query)
    {
        return $query->with(['products' => function ($q) {
            $q->leftJoin('products', 'order_products.product_id', '=', 'products.id')
                ->addSelect('products.slug', 'order_products.*', 'products.in_stock');
            $q->leftJoin('products_localizations', 'order_products.product_id', '=', 'products_localizations.product_id')
                ->where('products_localizations.locale', 'ru')
                ->addSelect('products_localizations.name');
        }]);
    }
}
