<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class ProductQuestion extends Model
{
    protected $fillable = ['product_id', 'status', 'message', 'email'];

    public function product(){
    	return $this->belongsTo(Product::class);
    }
}
