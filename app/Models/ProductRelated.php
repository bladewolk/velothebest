<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductRelated extends Model
{
    protected $fillable = ['product_id', 'related_product_id'];

    public function product(){
    	return $this->belongsTo(Product::class, 'related_product_id', 'id');
    }
}
