<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentLocalization extends Model
{
    protected $fillable = ['name', 'locale', 'payment_id'];
}
