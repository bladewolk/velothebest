<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategorieFeature extends Model
{
    protected $fillable = ['feature_id', 'category_id'];
}
