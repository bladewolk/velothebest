<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryLocalization extends Model
{
	protected $fillable = ['name', 'locale'];
}
