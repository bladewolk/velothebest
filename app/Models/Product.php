<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;
use App\Models\ProductRelated;
use App\Models\Features;
use App\Models\ProductGroup;
use App\Models\Color;

class Product extends Model
{
    protected $fillable = ['slug', 'status', 'brand_id', 'size', 'color_id', 'in_stock', 'code', 'price',
        'price_discount', 'cat_id', 'main_feature', 'main_feature_text', 'visible', 'relgroup', 'video', 'label_id'];

    public function localization()
    {
        return $this->hasMany(ProductsLocalizations::class);
    }

    public function group(){
        return $this->hasOne(ProductGroup::class, 'id', 'relgroup');
    }

    public function mainFeature()
    {
        return $this->hasOne(Option::class, 'id', 'main_feature');
    }

    public function label()
    {
        return $this->hasOne(Label::class, 'id', 'label_id');
    }

    public function features()
    {
        return $this->hasMany(ProductFeatures::class, 'product_id', 'id');
    }

    public function characteristicks()
    {
        return $this->hasMany(ProductOption::class);
    }

    public function associated()
    {
        return $this->hasMany(ProductRelated::class);
    }

    public function setPriceDiscountAttribute($value)
    {
        $this->attributes['price_discount'] = floatval($value);
    }

    public function comments()
    {
        return $this->hasMany(ProductComment::class);
    }

    // Изображения товара. Главная картинка всегда будет первой!
    public function images()
    {
        return $this->morphMany(Picture::class, 'picturetable');
    }

    public function color(){
        return $this->hasOne(Color::class, 'id', 'color_id');
    }


    // Admin
    public function scopeSoputForSelect($query)
    {
        return $query->leftJoin('products_localizations', 'products.id', '=', 'products_localizations.product_id')
            ->where('products_localizations.locale', 'ru')
            ->pluck('products_localizations.name', 'products_localizations.product_id')
            ->toArray();
    }

    // CATALOG show Item
    public function scopeWithLocalization($query){
        return $query->with(['localization' => function($q){
            $q->where('locale', App::getLocale());
        }]);
    }


    // Join Localized label
    public function scopeAddLabel($query)
    {
        return $query->leftJoin('label_localizations', 'products_localizations.label_id', 'label_localizations.label_id')
            ->select('label_localizations.description as desc', 'label_localizations.label_id', 'products_localizations.label_id');
    }

    public function scopeWithCarousel($query)
    {
        return $query->with(['associated' => function ($q) {
            $q->leftJoin('products', 'product_relateds.product_id', '=', 'products.id');
        }]);
    }

    // return item with joining data by current locale TODO remove if new solution finded
    public function scopeAddCarousel($query)
    {
        $locale = App::getLocale();
        return $query->with(['associated' => function ($q) use ($locale) {
            $q->select('product_relateds.related_product_id', 'product_relateds.product_id',
                'products_localizations.label_text', 'products.id');

            $q->leftJoin('products', 'product_relateds.related_product_id', '=', 'products.id')
                ->addSelect('products.status', 'products.code', 'products.price',
                    'products.price_discount', 'products.slug');

            $q->leftJoin('products_localizations', 'product_relateds.related_product_id', 'products_localizations.product_id')
                ->where('products_localizations.locale', $locale)
                ->addSelect('products_localizations.name', 'products_localizations.label_text', 'products_localizations.label_id');

            $q->leftJoin('labels', 'products_localizations.label_id', '=', 'labels.id')
                ->addSelect('labels.color as label_color');


            $q->leftJoin('label_localizations', function ($join) use ($locale) {
                $join->on('label_localizations.label_id', '=', 'products_localizations.label_id')
                    ->where('label_localizations.locale', $locale);
            })->addSelect('label_localizations.description as label_description');
        }]);
    }

    // Add product relation
    public function scopeWithProductLocalization($query)
    {
        return $query->with(['localization' => function ($q) {
            $q->whereLocale(App::getLocale());
            $q->with(['label' => function ($rel) {
                $rel->leftJoin('label_localizations', 'labels.id', '=', 'label_localizations.label_id')
                    ->select('labels.*', 'label_localizations.description')
                    ->where('label_localizations.locale', App::getLocale());
            }]);
        }]);
    }

    // Get RELATION Product -> characteristics by current locale
    public function scopeWithProductCharacteristics($query)
    {
        $locale = App::getLocale();

        return $query->with(['characteristicks' => function ($q) use ($locale) {
            $q->leftJoin('option_localizations', 'product_options.option_id', '=', 'option_localizations.option_id')
                ->where('option_localizations.locale', $locale)
                ->where('product_options.locale', App::getLocale())
                ->addSelect('product_options.value', 'product_options.product_id', 'option_localizations.name');
        }]);
    }


    public function scopeWithMainFeatureName($query)
    {
        $query->with(['mainFeature' => function ($q) {
            $q->leftJoin('option_localizations', 'options.id', '=', 'option_localizations.option_id')
                ->where('option_localizations.locale', App::getLocale())
                ->addSelect('option_localizations.name', 'options.*');
        }]);
    }

    public function scopeJoinLocalization($query)
    {
        return $query->leftJoin('products_localizations', 'products.id', '=', 'products_localizations.product_id')
            ->addSelect('products_localizations.name', 'products.*', 'products_localizations.locale')
            ->where('products_localizations.locale', App::getLocale());
    }

}
