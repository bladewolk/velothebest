<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeatureLocalization extends Model
{
    protected $fillable = ['name', 'locale', 'feature_id'];
}
