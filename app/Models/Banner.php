<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use File;
use Image;

class Banner extends Model
{
    protected $fillable = ['image', 'number', 'url'];

    public static function upadePicture($request, $id)
    {
        $sizes = [
            '1' => [390, 460],
            '2' => [290, 220],
            '3' => [290, 220],
            '4' => [290, 220],
            '5' => [290, 220],
            '6' => [260, 460],
        ];

        $model = self::whereNumber($id)->first();

        $name = time() . '.' . $request->file('image')->getClientOriginalExtension();
        if (File::exists('uploads/blocks/' . $model->image))
            File::delete('uploads/blocks/' . $model->image);

        Image::make($request->file('image'))->fit($sizes[$model->number][0], $sizes[$model->number][1])
            ->save('uploads/blocks/' . $name);

        $model->update([
            'image' => $name,
            'url' => $request->url
        ]);

        return;
    }
}
