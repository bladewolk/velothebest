<?php

namespace App\Models;

use Image;
use File;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = ['image', 'image_background'];

    public function images()
    {
        return $this->morphMany(Picture::class, 'picturetable');
    }
}
