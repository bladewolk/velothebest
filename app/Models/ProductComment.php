<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductComment extends Model
{
    protected $fillable = ['product_id', 'name', 'email', 'rating', 'comment', 'status'];

    public function product(){
    	return $this->belongsTo(Product::class);
    }
}
