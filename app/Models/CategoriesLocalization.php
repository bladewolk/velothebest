<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoriesLocalization extends Model
{
    protected $fillable = ['categorie_id', 'name', 'body', 'locale', 'meta_title', 'meta_keywords', 'meta_description'];
}
