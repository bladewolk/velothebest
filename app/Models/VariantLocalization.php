<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VariantLocalization extends Model
{
    protected $fillable = ['name', 'locale', 'variant_id'];
}
