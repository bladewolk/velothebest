<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageLocalization extends Model
{
    protected $fillable = ['name', 'body', 'meta_title', 'meta_keywords', 'meta_description'];
}
