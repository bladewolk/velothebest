<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Baum\Node;
use App;

class Page extends Node
{
//   Different tree in one table by custom row TYPE
    protected $scoped = ['type'];
    protected $fillable = ['type', 'slug'];
    protected $guarded = ['id', 'lft', 'rgt', 'parent_id'];
    protected $orderColumn = 'lft';
    public $search = [];

    public function localization(){
        return $this->hasMany(PageLocalization::class);
    }

    public function scopeJoinLocalization($query){
        return $query->leftJoin('page_localizations', 'pages.id', '=', 'page_localizations.page_id')
                ->where('page_localizations.locale', App::getLocale())
                ->addSelect('page_localizations.*');
    }

    public function scopeWithLocalization($query)
    {
        return $query->with(['localization' => function ($q){
            $q->where('locale', App::getLocale());
        }]);   
    }


    public static function boot()
    {
        parent::boot();

        self::deleting(function ($model) {
            PageLocalization::wherePageId($model->id)->delete();
        });
    }

}
