<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsLocalizations extends Model
{
    protected $fillable = ['name', 'body', 'meta_title', 'meta_description', 'meta_keywords', 'new_id', 'locale'];

}
