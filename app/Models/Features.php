<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Features extends Model
{
    public function localization()
    {
        return $this->hasMany(FeatureLocalization::class, 'feature_id');
    }

    public function variants()
    {
        return $this->belongsToMany(Variant::class, 'feature_variants', 'feature_id', 'variant_id');
    }

    public function scopeWithVariantsRus($query){
    	return $query->with(['variants' => function ($q) {
            $q->leftJoin('variant_localizations', 'variants.id', '=', 'variant_localizations.variant_id');
            $q->select('variants.*', 'variant_localizations.name');
            $q->whereLocale('ru');
        }])
            ->leftJoin('feature_localizations', 'features.id', '=', 'feature_localizations.feature_id')
            ->whereLocale('ru')
            ->select('features.*', 'feature_localizations.name');
    }
}
