<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Label extends Model
{
//    Лейбы ппродуктов
    protected $fillable = ['color', 'name'];

    public function localization()
    {
        return $this->hasMany(LabelLocalization::class);
    }
}
