<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Label;

class ProductsLocalizations extends Model
{
    protected $fillable = ['name', 'product_id', 'label_id', 'label_text', 'locale', 'description', 'meta_title',
        'meta_keywords', 'meta_description'];

        public function label(){
        	return $this->hasMany(Label::class, 'id', 'label_id');
        }
}
