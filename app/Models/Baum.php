<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Baum\Node;

class Baum extends Node
{
    //

    public static function rebuildTree($request)
    {
        $model = "App\\Models\\" . $request->model;
        $find = self::findInData($request->data, $request->item);
        if ($find['left'] != null) {
            $left = $model::find($find['left']);
            $model::find($request->item)->moveToRightOf($left);
            return;
        }
        if ($find['right'] != null) {
            $right = $model::find($find['right']);
            $model::find($request->item)->moveToLeftOf($right);
            return;
        }
        if ($find['root'] != null) {
            $root = $model::find($find['root']);
            $model::find($request->item)->makeChildOf($root);
            return;
        }

        return;
    }

    public static function findInData($data, $id, $root = null)
    {
        foreach ($data as $key => $item) {
            if ($item['children']) {
                $closure = self::findInData($item['children'], $id, $item['id']);
            }

            if ($item['id'] == $id) {
                if (isset($data[$key - 1]['id']))
                    $left = $data[$key - 1]['id'];
                if (isset($data[$key + 1]['id']))
                    $right = $data[$key + 1]['id'];

                return [
                    'left' => isset($data[$key - 1]['id']) ? $data[$key - 1]['id'] : null,
                    'right' => isset($data[$key + 1]['id']) ? $data[$key + 1]['id'] : null,
                    'root' => isset($root) ? $root : null
                ];
            }
        }

        return isset($closure) ? $closure : null;
    }
}
