<?php

namespace App\Models;

use App\Http\Controllers\Admin\AdminNewsController;
use Illuminate\Database\Eloquent\Model;
use App;

class News extends Model
{
    protected $fillable = ['type', 'slug'];

    public function localization()
    {
        return $this->hasMany(NewsLocalizations::class, 'new_id', 'id');
    }

    public function images()
    {
        return $this->morphMany(Picture::class, 'picturetable');
    }

    public function scopeJoinLocalization($query)
    {
        return $query->leftJoin('news_localizations', 'news.id', '=', 'news_localizations.new_id')
            ->where('news_localizations.locale', App::getLocale())
            ->addSelect('news.id', 'news.slug','news.created_at', 'news_localizations.name', 'news_localizations.body',
                'news_localizations.meta_title', 'news_localizations.meta_description', 'news_localizations.meta_keywords'
                );
    }
}
