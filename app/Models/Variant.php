<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    public function localization()
    {
        return $this->hasMany(VariantLocalization::class);
    }

}
