<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Option;

class ProductOption extends Model
{
    protected $fillable = ['product_id', 'option_id', 'value', 'locale'];

    public function option(){
    	return $this->belongsTo(Option::class);
    }
}
