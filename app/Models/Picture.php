<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Image;
use File;

class Picture extends Model
{
    protected $fillable = ['name'];

    public function picturetable()
    {
        return $this->morphTo();
    }

    public function getNameAttribute($image)
    {
        if (empty($image))
            return 'ERROR Picture Not Found';

        return $image;
    }
    // Save slide image 
    public static function saveSlide($item, $image, $tableRow = null)
    {
        $name = time() . '.' . $image->getClientOriginalExtension();
        Image::make($image)->save(storage_path('/app/images/') . '' . $name);
        $item->update([$tableRow => $name]);
    }
    // Update Slide image/image_background
    // $old - old name image (need to delete file)
    public static function updateSlide($item, $image, $tableRow = null, $old = '')
    {
        if ($old && File::exists(storage_path('app/images/') . $item->image)){
            File::delete(storage_path('app/images/') . $item->image);
            File::deletedirectory(storage_path('app/.cache/images/') . $item->image);
        }
        
        $name = time() . '.' . $image->getClientOriginalExtension();
        Image::make($image)->save(storage_path('/app/images/') . '' . $name);
        $item->update([$tableRow => $name]);
    }

    public static function savePictureToStorage($item, $images)
    {
        if (!is_array($images)) {
            $images = [$images];
        }
        foreach ($images as $image) {
            $name = rand(0,100) .''.time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->save(storage_path('/app/images/') . '' . $name);
            $item->images()->create(['name' => $name]);
        }
        return;
    }

    public static function updateOneInStorage($item, $image)
    {
        if ($item->images->first()){
            File::delete(storage_path('/app/images/') . '' . $item->images->first()->name);
            File::deletedirectory(storage_path('app/.cache/images/'). $item->images->first()->name);
        }
        $name = time() . '.' . $image->getClientOriginalExtension();
        Image::make($image)->save(storage_path('/app/images/') . '' . $name);
        if ($item->images->first())
            $item->images->first()->update(['name' => $name]);
        else
            $item->images()->create(['name' => $name]);
    }

    public static function deleteFromStorage($name)
    {
        if (File::exists(storage_path('/app/images/') . '' . $name))
            File::deletedirectory(storage_path('/app/.cache/images/') . '' . $name);
            File::delete(storage_path('/app/images/') . '' . $name);
        return;
    }
    // Update LogoType
    public static function updateLogo($model, $image){
        if ($model->images->count() && File::exists(storage_path('app/images/') . $model->images->first()->name)){
            File::deletedirectory(storage_path('app/.cache/images/') . $model->images->first()->name);
            File::delete(storage_path('app/images/') . $model->images->first()->name);        
        }
        $name = time() . '.' . $image->getClientOriginalExtension();
        Image::make($image)->save(storage_path('/app/images/') . $name);
        $model->images()->create([
                'name' => $name
            ]);
        return;
    }

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($model) {
            self::deleteFromStorage($model->name);
        });
    }
}
