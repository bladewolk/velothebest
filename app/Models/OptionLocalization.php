<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OptionLocalization extends Model
{
    protected $fillable = ['name', 'locale', 'option_id'];
}
