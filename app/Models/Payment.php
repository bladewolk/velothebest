<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;

class Payment extends Model
{
    public function localization(){
    	return $this->hasMany(PaymentLocalization::class);
    }

    public function scopeJoinLocalization($query){
    	return $query->leftJoin('payment_localizations', 'payments.id', '=', 'payment_localizations.payment_id')
    		->where('payment_localizations.locale', App::getLocale())
    		->addSelect('payments.id', 'payment_localizations.name');
    }
}
