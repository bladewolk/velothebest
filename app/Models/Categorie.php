<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Baum\Node;
use DB;
use App;

class Categorie extends Node
{
//    protected $scoped = ['type'];
    protected $fillable = ['slug'];
    protected $guarded = ['id', 'lft', 'rgt', 'parent_id'];
    protected $orderColumn = 'lft';
    public $search = [];

    public function localization()
    {
        return $this->hasMany(CategoriesLocalization::class);
    }

    public function images()
    {
        return $this->morphMany(Picture::class, 'picturetable');
    }

    public function childrens()
    {
        return $this->hasMany(self::class, 'parent_id', 'categorie_id');
    }

    public function features(){
        return $this->hasMany(CategorieFeature::class, 'category_id');
    }

    public function scopeConverForSelectOnlyRus($query){
        return $query->leftJoin('categories_localizations', 'categories.id', '=', 'categories_localizations.categorie_id')
            ->whereLocale('ru')
            ->where('categories.parent_id','<>', NULL)
            ->select('categories.*', 'categories_localizations.name')
            ->pluck('name', 'id')
            ->toArray();
    }


    public function scopeJoinLocalization($query){
        return $query->leftJoin('categories_localizations', 'categories.id', '=', 'categories_localizations.categorie_id')
            ->whereLocale(App::getLocale())
            ->addSelect('categories_localizations.name', 'categories.*');
    }
}
