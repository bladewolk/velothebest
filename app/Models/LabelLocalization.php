<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LabelLocalization extends Model
{
    protected $fillable = ['color', 'description', 'label_id', 'locale'];

}
