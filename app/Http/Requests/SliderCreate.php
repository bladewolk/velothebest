<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SliderCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd(request()->all());
        return [
            'image' => 'mimes:jpeg,jpg,png,gif|max:10000',
            'image_background' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
        ];
    }

    public function messages(){
        return [
            'image_background.required' => 'Вы не выбрали фоновое изображение',
            '*.mimes' => 'Загруженный файл не соответсвует формату изображения (jpeg, jpg, png, gif)',
            '*.max' => 'Превышен максимальный размер файла! (10 мб.)',
        ];
    }
}
