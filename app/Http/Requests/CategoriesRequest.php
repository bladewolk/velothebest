<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->method() == 'PATCH'){
            return [
                'slug' => 'required|between:1,255|unique:categories,slug,'. request()->segment(count(request()->segments())),
                'name.*' => 'required|between:1,255',
                'body.*' => 'max:1500',
                'meta_title.*' => 'max:255',
                'meta_keywords.*' => 'max:255',
                'meta_description.*' => 'max:255',
                'image' => 'mimes:jpeg,jpg,png,gif|max:10000',
            ];
        }else{
            return [
                'slug' => 'required|between:1,255|unique:categories',
                'name.*' => 'required|between:1,255',
                'body.*' => 'max:1500',
                'meta_title.*' => 'max:255',
                'meta_keywords.*' => 'max:255',
                'meta_description.*' => 'max:255',
                'image' => 'mimes:jpeg,jpg,png,gif|max:10000',
            ];
        }
    }

    public function messages(){
        return [
            'slug.unique' => 'Данный URL уже используется!',
            'slug.required' => 'Заполните URL страницы',
            'name.*.required' => 'Заполните поле Заголовок для всех локализаций',
            'name.*.between' => 'Имя должно быть в диапазоне от 1 до 255 символов',
            'body.*.max' => 'Превышена максимальная длина описания (1500 символов)',
            'meta_title.*' => 'Слишком длинное значение meta_title!',
            'meta_keywords.*' => 'Слишком длинное значение meta_keywords!',
            'meta_description.*' => 'Слишком длинное значение meta_description!',
            'image.mimes' => 'Загруженный файл не соответсвует формату изображения (jpeg, jpg, png, gif)',
            'image.max' => 'Превышен максимальный размер файла! (10 мб.)',
        ];
    }
}
