<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->method() == 'PATCH'){
            if (request()->ajax() == false)
             return [
                'slug' => 'required|between:1,255|unique:products,slug,'. request()->segment(count(request()->segments())),
                'in_stock' => 'required|min:0',
                'code' => 'required|unique:products,code,'. request()->segment(count(request()->segments())),
                'price' => 'required|min:0',
                'name.*' => 'required',
                'description.*' => 'max:1500',
                'meta_title.*' => 'max:255',
                'meta_keywords.*' => 'max:255',
                'meta_description.*' => 'max:255',
                'video.*' => 'required|max:255',
                'image' => 'mimes:jpeg,jpg,png,gif|max:10000',
                'image.*' => 'mimes:jpeg,jpg,png,gif|max:10000',
            ];
            return [];
        } else{
            return [
                'slug' => 'required|between:1,255|unique:products',
                'in_stock' => 'required|min:0',
                'code' => 'required|unique:products',
                'price' => 'required|min:0',
                'name.*' => 'required',
                'description.*' => 'max:1500',
                'meta_title.*' => 'max:255',
                'meta_keywords.*' => 'max:255',
                'meta_description.*' => 'max:255',
                'video.*' => 'required|max:255',
                'image' => 'required|mimes:jpeg,jpg,png,gif|max:10000',
                'image.*' => 'mimes:jpeg,jpg,png,gif|max:10000',
            ];
        }
    }

    public function messages(){
        return [
            'slug.unique' => 'Данный URL уже используется!',
            'slug.required' => 'Заполните URL страницы',
            'in_stock.required' => 'Вы не ввели количество товаров на складе',
            'in_stock.min' => 'Минимальное количество товаров 0',
            'code.required' => 'Введите код товара',
            'code.unique' => 'Данный код уже используется',
            'price.required' => 'Введите цену товара',
            'price.min' => 'Цена не может быть отрицательной',
            'name.*.required' => 'Заполните название товара для всех локализаций',
            'description.*.max' => 'Превышена максимальная длина описания (1500 символов)',
            'meta_title.*' => 'Слишком длинное значение meta_title!',
            'meta_keywords.*' => 'Слишком длинное значение meta_keywords!',
            'meta_description.*' => 'Слишком длинное значение meta_description!',
            'video.max' => 'Ссылка на видео превышает 255 символов!',
            'image.required' => 'Нужно загрузить основное изображение товара!',
            'image.mimes' => 'Загруженный файл не соответсвует формату изображения (jpeg, jpg, png, gif)',
            'image.max' => 'Превышен максимальный размер файла! (10 мб.)',
            'images.*.mimes' => 'Дополнительные изображения не соответсвует формату изображения (jpeg, jpg, png, gif)',
            'image.*.max' => 'Дополнительные изображения - превышем объем одного изображения! (10 мб.)',
        ];
    }
}
