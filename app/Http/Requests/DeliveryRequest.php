<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeliveryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name.*' => 'required|between:1,255',
        ];
    }

    public function messages(){
        return [
            '*.required' => 'Необходимо заполнить способ доставки для всех языков',
            '*.between' => 'Длина вводимого текста должна быть от 1 до 255 символов', 
        ];
    }
}
