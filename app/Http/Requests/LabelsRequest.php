<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LabelsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'color' => 'required|between:1,255',
            'name' => 'required|between:1,255',
            'description.*' => 'required|between:1,255',
        ];
    }

    public function messages(){
        return [
            'color.required' => 'Вам нужно выбрать цвет!',
            'name.required' => 'Введите название',
            'description.*.required' => 'Введите текст лейбы для всех локализаций',
            '*.between' => 'Длина вводимого текста должна быть от 1 до 255 символов',
        ];
    }
}
