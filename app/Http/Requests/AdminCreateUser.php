<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminCreateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'required',
            'permission' => 'required',
            'password' => 'required|confirmed',
            'email' => 'email|required',
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Заполните поле имя',
            'phone.required' => 'Заполните поле телефон',
            'permission.required' => 'Нужно выбрать права пользователя',
            'password.required' => 'Введите пароль',
            'email.required' => 'Введите Email',
            'password.confirmed' => 'Ввведенные пароли не совпадают',
        ];
    }
}
