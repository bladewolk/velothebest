<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CurrencyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'currency_uah' => 'required|min:1',
        ];
    }

    public function messages(){
        return [
            'currency_uah.required' => 'Введите значение поля USD',
            'currency_uah.min' => 'Введенное значение не должно быть меньше 1',
        ];
    }
}
