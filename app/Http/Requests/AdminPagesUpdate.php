<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminPagesUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd(request()->all());
        return [
            'slug' => 'required|between:1,255|unique:pages,slug,'. request()->segment(count(request()->segments())),
            'name.*' => 'required|min:5|max:255|between:5,255',
            'body.*' => 'max:1500',
            'meta_title.*' => 'max:255',
            'meta_keywords.*' => 'max:255',
            'meta_description.*' => 'max:255',

        ];
    }

    public function messages(){
        return [
            'slug.unique' => 'Данный URL уже используется!',
            'slug.required' => 'Заполните URL страницы',
            'name.*.required' => 'Заполните поле Заголовок для всех локализаций',
            'body.*.max' => 'Превышена максимальная длина текста страницы (1500 символов)',
            'meta_title.*' => 'Слишком длинное значение meta_title!',
            'meta_keywords.*' => 'Слишком длинное значение meta_keywords!',
            'meta_description.*' => 'Слишком длинное значение meta_description!',
        ];
    }
}
