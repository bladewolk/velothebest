<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Categorie;
use App\Models\Page;
use App\Models\Setting;
use Session;
use App;

class BaseController extends Controller
{
    public function __construct()
    {
        $locale = \App::getLocale();

        $topLinks = Page::whereType('top')
            ->with(['localization' => function($q){
                $q->where('locale', App::getLocale());
                $q->select('name', 'page_id');
            }])
            ->orderBy('lft', 'asc')
            ->get();

        $brands = Brand::latest()
            ->leftJoin('brands_localizations', 'brands.id', '=', 'brands_localizations.brand_id')
            ->with('images')
            ->whereLocale($locale)
            ->select('brands.*', 'brands_localizations.name as name')
            ->get();


        $mainLinks = Page::whereType('main')
            ->leftJoin('page_localizations', 'pages.id', '=', 'page_localizations.page_id')
            ->whereLocale($locale)
            ->where('slug', '<>', '/')
            ->orderBy('lft', 'asc')
            ->select('pages.*', 'page_localizations.name')
            ->get();

        $categories = Categorie::leftJoin('categories_localizations', 'categories.id', '=', 'categories_localizations.categorie_id')
            ->whereLocale($locale)
            ->with(['childrens' => function ($q) use ($locale) {
                $q->leftJoin('categories_localizations', 'categories.id', '=', 'categories_localizations.categorie_id')
                    ->select('categories.*', 'categories_localizations.name')
                    ->whereLocale($locale);
                $q->with('images');
            }])
            ->whereParentId(null)
            ->orderBy('lft', 'asc')
            ->select('categories.slug as slug', 'categories_localizations.*')
            ->get();

        view()->share([
            'topLinks' => $topLinks,
            'mainLinks' => $mainLinks,
            'settings' => Setting::first(),
            'brands' => $brands,
            'categories' => $categories,
        ]);
    }
}
