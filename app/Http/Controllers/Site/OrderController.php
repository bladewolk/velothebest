<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use Session;
use App\Models\Product;
use Auth;
use App\Models\Delivery;
use App\Models\Payment;
use App\Models\Order;
use App\Models\Setting;

class OrderController extends BaseController
{
    public function addToCart(Request $request)
    {
        if (Session::has('cart')) {
            if (!in_array($request->product_id, Session::get('cart')))
                Session::push('cart', $request->product_id);
        } else {
            Session::push('cart', $request->product_id);
        }

        $this->calcTotalPrice();
        return trans('messages.in_cart');
    }

    public function showCart()
    {
        $items = [];

        if (Session::has('cart'))
            $items = Product::whereIn('products.id', Session::get('cart'))
                ->withProductLocalization()
                ->with('images')
                ->get();

        $delivery = Delivery::joinLocalization()
            ->get();

        $payment = Payment::joinLocalization()
            ->get();

        return view('site.cart', [
            'items' => $items,
            'delivery' => $delivery,
            'payment' => $payment
        ]);
    }

    // Delete Item From Cart
    public function dropFromCart(Request $request)
    {
        Session::put('cart', array_diff(Session::get('cart'), [$request->product_id]));
        if (count(Session::get('cart')) == 0) {
            Session::forget('cart');
            Session::forget('totalPrice');
            return 0;
        }

        return 1;
    }

    // MakeOrder
    public function makeOrder(Request $request)
    {
        $currency = Setting::select('currency_uah')->first()->currency_uah;
        $order = Order::create(['currency' => $currency]);
        $totalPrice = 0;

        foreach ($request->items as $index => $value) {
            $temp = Product::select('id', 'price', 'price_discount')->find($value);
            $totalPrice += $temp->price_discount > 0 ? $temp->price_discount * $request->counts[$index] : $temp->price * $request->counts[$index];
            $order->products()->create([
                'product_id' => $temp->id,
                'count' => $request->counts[$index],
                'price' => $temp->price_discount > 0 ? $temp->price_discount : $temp->price
            ]);
        }

        if (Auth::guest())
            $order->update(array_merge($request->except('_token', 'items', 'counts'), ['total_price' => $totalPrice]));

        if (Auth::user())
            $order->update(array_merge(Auth::user()->toArray(), ['total_price' => $totalPrice, 'user_id' => Auth::user()->id], $request->except('_token', 'items', 'counts')));

        Session::forget('cart');
        Session::forget('totalPrice');

        return redirect()->back();
    }


    // Total cart Price
    public function calcTotalPrice()
    {
        $inCart = Session::get('cart');
        $itemsInCart = Product::whereIn('id', $inCart)
            ->select('price', 'price_discount')
            ->get();

        $totalPrice = 0;
        foreach ($itemsInCart as $item) {
            if ($item->price_discount > 0)
                $totalPrice += $item->price_discount;
            else
                $totalPrice += $item->price;
        }

        Session::put('totalPrice', $totalPrice);

        return;
    }

}
