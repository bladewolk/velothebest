<?php

namespace App\Http\Controllers\Site;

use App\Http\Requests\UpdateUserInfoRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PersonalController extends BaseController
{
    /**
     * @return mixed
     */
    public function index()
    {
        if (Auth::guest())
            return redirect('/');

        return view('site.personal.personal', [
            'user' => Auth::user()
        ]);
    }

    /**
     * @param Request $request
     */
    public function update(Request $request)
    {
        if ($request->row == 'info') {
            Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|unique:users,email,' . Auth::user()->id,
                'city' => 'required|max:100',
                'address' => 'required|max:100',
                'region' => 'required|max:100',
            ], [
                '*.required' => trans('messages.enter_this_field'),
                '*.max' => trans('messages.too_long_input'),
                'email.unique' => trans('messages.email_already_exist'),
            ])->validate();
            Auth::user()->update($request->all());

            return;
        }
        if ($request->row == 'password') {
            Validator::make($request->all(), [
                'password' => 'required|confirmed|min:6|max:50',
            ], [
                'password.confirmed' => trans('messages.password_is_diff'),
                '*.min' => trans('messages.too_short_input'),
                '*.max' => trans('messages.too_long_input')
            ])->validate();
            Auth::user()->update(['password' => bcrypt($request->password)]);
            return;
        }
//        Auth::user()->update($request->all());
        return;
    }

//    History of user orders
    public function history()
    {
        return view('site.personal.history', [
            'user' => Auth::user()->withOrders()->first()
        ]);
    }
}
