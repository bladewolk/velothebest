<?php

namespace App\Http\Controllers\Site;

use App\Models\Brand;
use App\Models\Page;
use App\Models\Product;
use App\Models\ProductOption;
use Illuminate\Http\Request;

class PageController extends BaseController
{
    public function index($page)
    {
        $page = Page::whereSlug($page)->joinLocalization()->firstOrFail();

        return view('site.template', [
            'current_page' => $page
        ]);
    }

    public function showBrand($brand)
    {
         $item = Brand::whereSlug($brand)
            ->joinLocalization()
            ->firstOrFail();

        $breadcrumbs = [
            'home' => '/',
            $item->name => '/brand/' . $item->slug
        ];
       
        return view('site.show_brand', [
            'breadcrumbs' => $breadcrumbs,
            'current_page' => $item
        ]);
    }
}
