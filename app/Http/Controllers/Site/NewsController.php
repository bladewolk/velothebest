<?php

namespace App\Http\Controllers\Site;

use App\Models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends BaseController
{
    public function index($new = null){
        if ($new)
            return $this->showNew($new);

        $news = News::joinLocalization()->with('images')->paginate(12);
        return view('site.news', [
            'items' => $news
        ]);
    }

    public function showNew($item){
        $item = News::whereSlug($item)->joinLocalization()->firstOrFail();

        $breadcrumbs = [
            'home' => '/',
            trans('messages.news') => '/news',
            $item->name => '/news/'.$item->slug
        ];

        return view('site.show_new', [
            'current_page' => $item,
            'breadcrumbs' => $breadcrumbs
        ]);
    }
}
