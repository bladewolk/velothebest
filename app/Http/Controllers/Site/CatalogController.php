<?php

namespace App\Http\Controllers\Site;

use App\Models\CategorieFeature;
use App\Models\Features;
use App\Models\PageLocalization;
use App\Models\Product;
use App\Models\Categorie;
use App\Models\ProductFeatures;
use App\Models\ProductOption;
use App\Models\ProductRelated;
use App\Models\Setting;
use Illuminate\Http\Request;
use App;
use View;
use App\Models\Color;
use App\Models\CategoriesLocalization;

class CatalogController extends BaseController
{
    public function index()
    {
       abort(404);
    }

    public function parse($name, Request $request)
    {
        $locale = App::getLocale();
        $categorie = Categorie::whereSlug($name)
            ->first();

        if ($categorie && $categorie->parent_id === null)
            return $this->showSubcategories($categorie);

        if ($categorie) {
            if ($categorie->parent_id === null)
                return $this->showSubcategories($categorie);

            return $this->showItems($categorie, $request);
        }

        if ($item = Product::whereSlug($name)->first())
            return $this->showItem($item);

        abort(404);
    }

    // Отображение подкатегорий
    public function showSubcategories($category)
    {
        $breadcrumbs = [
            'home' => '/',
            $category->localization()->whereLocale(App::getLocale())->first()->name => '/catalog/' . $category->slug
        ];

        $subcategories = $category->children()
            ->joinLocalization()
            ->with('images')
            ->get();

        return view('site.catalog.subcategory', [
            'subcategories' => $subcategories,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    // Карточка товара
    public function showItem($item)
    {
        $product = Product::whereId($item->id)
            ->with('localization')
            ->with('images')
            ->with('comments')
            ->with('group.products.color')
            ->with('associated.product')
            ->with('characteristicks')
            ->with('mainFeature.localization')
            ->whereVisible(true)
            // ->withMainFeatureName()
            ->firstOrFail();

        $delivery = PageLocalization::wherePageId(1)
            ->whereLocale(App::getLocale())
            ->select('body')
            ->first();

        $quant = PageLocalization::wherePageId(3)
            ->whereLocale(App::getLocale())
            ->select('body')
            ->first();

        $category = Categorie::where('categories.id', $product->cat_id)->with('localization')->first();
        $breadcrumbs = [
            'home' => '/',
            $category->parent->localization->where('locale', App::getLocale())->first()->name => '/catalog/' . $category->parent->slug,
            $category->localization->where('locale', App::getLocale())->first()->name => '/catalog/' . $category->slug,
            $product->localization->first()->name => '/catalog/' . $product->slug,
        ];

        return view('site.product.product', [
            'item' => $product,
            'delivery' => $delivery->body,
            'quant' => $quant->body,
            'current_page' => $product->localization->first(),
            'breadcrumbs' => $breadcrumbs
        ]);
    }

//    Отобразить товары из данной категории каталога
    protected function showItems($category, $request)
    {
        $paginate = 24;
        $locale = App::getLocale();
        
        $items = Product::whereVisible(true)
            ->withLocalization()
            ->with('comments')
            ->with('images')
            ->with('label.localization')
            ->whereCatId($category->id);

        //      Если присутсвтуют параметры фильтрации
        if ($request->features) {
            $items->whereHas('features', function ($q) use ($request) {
                $q->whereIn('variant_id', $request->features);
            });
        }

        if ($request->sort) {
            $temp = explode('|', $request->sort);
            $items->orderBy($temp[0], $temp[1]);
        } else
            $items->orderBy('products.id', 'desc');


//      Если присутствует сортирока по цене
        $settings = Setting::first();

        if ($request->currency == 'usd')
            $curs = 1;
        else
            $curs = $settings->currency_uah;

        if ($request->minPrice)
            $items->where('products.price', '>=', floor($request->minPrice / $curs));
        if ($request->maxPrice)
            $items->where('products.price', '<=', ceil($request->maxPrice / $curs));

        //      Переменные  для ценового слайдера с учетом локали
        $maxPrice = ceil(Product::where('cat_id', $category->id)->max('price') * $curs);
        $minPrice = round(Product::where('cat_id', $category->id)->min('price') * $curs);
        $currentMinPrice = null;
        $currentMaxPrice = null;

        if ($request->minPrice && is_numeric($request->minPrice)) {
            $currentMinPrice = $request->minPrice;
        }
        if ($request->maxPrice && is_numeric($request->maxPrice)) {
            $currentMaxPrice = $request->maxPrice;
        }

        // Количество товаров на страницу
        if ($request->paginate && in_array($request->paginate, [24, 48, 72])) {
            $paginate = $request->paginate;
        }

        // Фильтры текущей категории ID
        $category_filters = $category->features->pluck('feature_id')->toArray();
        
        // Фильтры - сбор ID вариантов которые имеюют товары данной категории
        $product_has_filters = ProductFeatures::whereHas('product', function($q){
            $q->where('cat_id', 2);
        })->pluck('variant_id')->toArray();
        // Достаем фильтры
        $filters = Features::whereIn('id', $category_filters)
        ->whereHas('variants', function($q) use ($product_has_filters){
            $q->whereIn('variant_id', $product_has_filters);
        })
        ->with(['variants' => function($q) use ($product_has_filters){
            $q->whereIn('variant_id', $product_has_filters);
        }])->get();


        if ($request->ajax()) {
            if ($request->has('additems')) {
                $items->paginate(24);

                $result['products'] = View::make('site.catalog.items', [
                    'items' => $items
                ])->render();

                $result['bottom'] = View::make('site.catalog.items-bottom', [
                    'items' => $items
                ])->render();

                return json_encode($result);
            }
            $items = $items->paginate($paginate);
            $result['products'] = View::make('site.catalog.items', [
                'items' => $items
            ])->render();

            $result['filters'] = View::make('site.catalog.activeFilters', [
                'filters' => $filters
            ])->render();

            $result['bottom'] = View::make('site.catalog.items-bottom', [
                'items' => $items
            ])->render();

            $result['finded'] = View::make('site.catalog.countFinded', [
                'items' => $items
            ])->render();

            return json_encode($result);
        }

        // BREADCRUMBS
        $categoryRoot = $category->getRoot(); 
        $breadcrumbs = [
            'home' => '/',
            $categoryRoot->localization->where('locale', App::getLocale())->first()->name => '/catalog/' . $categoryRoot->slug,
            $category->localization->where('locale', App::getLocale())->first()->name => '/catalog/' . $category->slug
        ];

        return view('site.catalog.index', [
            'items' => $items->paginate($paginate),
            'maxPrice' => intval($maxPrice),
            'minPrice' => intval($minPrice),
            'filters' => $filters,
            'currentMaxPrice' => intval($currentMaxPrice),
            'currentMinPrice' => intval($currentMinPrice),
            'breadcrumbs' => $breadcrumbs
        ]);
    }
}
