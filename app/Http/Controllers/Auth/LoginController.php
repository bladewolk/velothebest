<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function authenticated(Request $request)
    {
        return redirect()->back();
    }

    public function login(Request $request)
    {
        $validator = $this->validate($request, [
            $this->username() => 'required',
            'password' => 'required',
            // new rules here
        ], [
            '*.required' => trans('messages.enter_this_field'),
        ]);
        
        if ($request->ajax()) {
            if (Auth::attempt($request->except('_token')))
                return [
                    'status' => 'success',
                    'messages' => trans('messages.user_login_success')
                ];
            else {
                return [
                    'status' => 'fail',
                    'messages' => trans('messages.user_login_fail')
                ];
            }
        }

        if (Auth::attempt($request->except('_token'))) {
            return redirect()->back();
        } else {
            return redirect()->back()->withErrors(['0' => 'Введенные данные не соотвутсвуют нашим записям!']);
        }
    }
}
