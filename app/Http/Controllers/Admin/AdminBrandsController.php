<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use App\Models\BrandsLocalization;
use App\Models\Picture;
use Illuminate\Http\Request;

class AdminBrandsController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.brands.index', [
            'title' => 'Бренды',
            'items' => Brand::latest()
                ->leftJoin('brands_localizations', 'brands.id', '=', 'brands_localizations.brand_id')
                ->select('brands.*', 'brands_localizations.name')
                ->whereLocale('ru')
                ->paginate(20)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brands.create', [
            'title' => 'Создание записи',
            'item' => new Brand()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $brand = Brand::create($request->all());
        $locales = array_keys($request->name);

        foreach ($locales as $key) {
            $temp = [];
            foreach ($request->except('slug', 'type', '_token', 'image') as $row => $value) {
                $temp[$row] = $value[$key];
            }
            BrandsLocalization::create(array_merge($temp, ['brand_id' => $brand->id, 'locale' => $key]));
        }

        if ($request->file('image')) {
            Picture::savePictureToStorage($brand, $request->file('image'));
        }

        return redirect()->route('brands.index')->with('success', 'Запись создана');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        return view('admin.brands.edit', [
            'title' => 'Редактирование записи',
            'item' => $brand
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        $brand->update($request->all());
        $locales = array_keys($request->name);

        foreach ($locales as $key) {
            $temp = [];
            foreach ($request->except('slug', 'type', '_token', 'image', '_method') as $row => $value) {
                $temp[$row] = $value[$key];
            }
            BrandsLocalization::where('brand_id', $brand->id)->whereLocale($key)->update($temp);
        }

        if ($request->file('image')) {
            Picture::updateOneInStorage($brand, $request->file('image'));
        }

        return redirect()->route('brands.index')->with('success', 'Запись обновлена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if (empty($request->check))
            return redirect()->route('brands.index')->with('danger', 'Ничего не удалено');

        Brand::destroy($request->check);
        return redirect()->route('brands.index')->with('success', 'Удалено');
    }
}
