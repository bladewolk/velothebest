<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Http\Request;

class AdminHomeController extends AdminBaseController
{
    public function index()
    {
        return view('admin.home', [
            'title' => 'DvaCom'
        ]);
    }

    public function productManagment()
    {
        return view('admin.product_managment', [
            'title' => 'DvaCom'
        ]);
    }
}
