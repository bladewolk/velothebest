<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use App\Models\Categorie;
use App\Models\CategorieFeature;
use App\Models\CategoriesLocalization;
use App\Models\Features;
use App\Models\Picture;
use Illuminate\Http\Request;
use App\Http\Requests\CategoriesRequest;

class AdminCategoriesController extends AdminBaseController
{

    public function index()
    {
        return view('admin.categories.index', [
            'title' => 'Управление категориями',
            'count' => Categorie::count(),
            'items' => json_encode(array_values(Categorie::
            leftJoin('categories_localizations', 'categories.id', '=', 'categories_localizations.categorie_id')
                ->where('categories_localizations.locale', 'ru')
                ->select('categories.*', 'categories_localizations.name')
                ->orderBy('lft', 'asc')
                ->get()
                ->toHierarchy()->toArray()))
        ]);
    }


    public function create()
    {
        $filters = Features::leftJoin('feature_localizations', 'features.id', '=', 'feature_localizations.feature_id')
            ->with(['variants' => function ($q) {
                $q->leftJoin('variant_localizations', 'variants.id', '=', 'variant_localizations.variant_id')
                    ->select('variants.*', 'variant_localizations.name')
                    ->whereLocale('ru');
            }])
            ->whereLocale('ru')
            ->select("features.*", 'feature_localizations.name')
            ->get();

        return view('admin.categories.create', [
            'title' => 'Создание категории',
            'filters' => $filters,
            'item' => new Categorie(),
            'category_filter' => []
        ]);
    }


    public function store(CategoriesRequest $request)
    {
        $categorie = Categorie::create($request->all());
        $locales = array_keys($request->name);

        foreach ($locales as $key) {
            $temp = [];
            foreach ($request->except('slug', '_token', 'image', 'filters') as $row => $value) {
                $temp[$row] = $value[$key];
            }
            CategoriesLocalization::create(array_merge($temp, ['locale' => $key, 'categorie_id' => $categorie->id]));
        }

//        Create filters
        if ($request->has('filters')) {
            foreach ($request->filters as $id) {
                CategorieFeature::create([
                    'category_id' => $categorie->id,
                    'feature_id' => $id
                ]);
            }
        }

        if ($request->file('image')) {
            Picture::savePictureToStorage($categorie, $request->file('image'));
        }

        return redirect()->route('categories.index')->with('success', 'Запись создана');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
//        $filters = Features::leftJoin('feature_localizations', 'features.id', '=', 'feature_localizations.feature_id')
//            ->with(['variants' => function ($q) {
//                $q->leftJoin('variant_localizations', 'variants.id', '=', 'variant_localizations.variant_id')
//                    ->select('variants.*', 'variant_localizations.name')
//                    ->whereLocale('ru');
//            }])
//            ->whereLocale('ru')
//            ->select("features.*", 'feature_localizations.name')
//            ->get();

        $filters = Features::leftJoin('feature_localizations', 'features.id', '=', 'feature_localizations.feature_id')
            ->whereLocale('ru')
            ->select("features.*", 'feature_localizations.name')
            ->get();

        $categoryFilter = CategorieFeature::where('category_id', $id)
            ->pluck('feature_id')->toArray();

        return view('admin.categories.edit', [
            'title' => 'Редактирование записи',
            'item' => Categorie::find($id),
            'filters' => $filters,
            'category_filter' => $categoryFilter,
        ]);
    }


    public function update(CategoriesRequest $request, $id)
    {
        $categorie = Categorie::find($id);
        $categorie->update($request->all());
        $locales = array_keys($request->name);

        foreach ($locales as $key) {
            $temp = [];
            foreach ($request->except('slug', 'type', '_token', 'image', '_method', 'filters') as $row => $value) {
                $temp[$row] = $value[$key];
            }
            CategoriesLocalization::where('categorie_id', $categorie->id)->whereLocale($key)->update($temp);
        }
        if ($request->file('image')) {
            Picture::updateOneInStorage($categorie, $request->file('image'));
        }

//        Filters
        if ($request->filters) {
            $filters = CategorieFeature::where('category_id', $id)
                ->pluck('feature_id')->toArray();
//        DIff to delete
            $diff = array_diff($filters, $request->filters);
//      Diff to create
            $create = array_diff($request->filters, $filters);

            CategorieFeature::where('category_id', $id)
                ->whereIn('feature_id', $diff)
                ->delete();

            foreach ($create as $newFilter) {
                CategorieFeature::create([
                    'category_id' => $id,
                    'feature_id' => $newFilter
                ]);
            }
        } else {
            CategorieFeature::where('category_id', $id)
                ->delete();
        }

        return redirect()->route('categories.index')->with('success', 'Запись обновлена');
    }


    public function destroy(Request $request)
    {
        if ($request->action == 'destroy') {
            Categorie::destroy($request->checked);

            return json_encode(array_values(Categorie::
            leftJoin('categories_localizations', 'categories.id', '=', 'categories_localizations.categorie_id')
                ->where('categories_localizations.locale', 'ru')
                ->select('categories.*', 'categories_localizations.name')
                ->orderBy('lft', 'asc')
                ->get()
                ->toHierarchy()->toArray()));
        } else
            Categorie::find($request->id)->delete();

        return;
    }
}
