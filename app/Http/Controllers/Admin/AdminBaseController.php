<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Baum;
use Illuminate\Http\Request;
use Session;
use App\Models\Order;
use App\Models\ProductQuestion;
use App\Models\Feedback;
use App\Models\ProductComment;


class AdminBaseController extends Controller
{
    public $navigation = [
        'content' => [
            'Страницы основного меню' => 'master/page?type=main',
            'Страницы топ меню' => 'master/page?type=top',
            'Слайдер' => 'master/slider',
            'Баннер' => 'master/banner',
            'Валюта' => 'master/currency',
            'Новости' => 'master/news',
        ],
        'product' => [
            'Категории' => 'master/products_management/categories',
            'Параметры фальтриции' => 'master/products_management/features',
            'Характеристики продуктов' => 'master/products_management/options',
            'Бренды' => 'master/products_management/brands',
            'Лейбы' => 'master/products_management/labels',
            'Цвета' => 'master/products_management/colors',
            'Продукты' => 'master/products_management/products',
            'Группы продуктов' => 'master/products_management/groups',
            'Доставка' => 'master/products_management/delivery',
            'Оплата' => 'master/products_management/payment',
        ],
    ];


    public function __construct()
    {
        view()->share([
            'new_orders' => Order::whereStatus(0)->count(),
            'new_questions' => ProductQuestion::whereStatus(false)->count(),
            'navigation' => $this->navigation,
            'feedback' => Feedback::whereStatus(false)->count(),
            'comments' => ProductComment::whereStatus(false)->count()
        ]);
    }

    public function rebuildTree(Request $request)
    {
        Baum::rebuildTree($request);
        return;
    }
}
