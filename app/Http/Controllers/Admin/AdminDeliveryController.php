<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Delivery;
use App\Models\DeliveryLocalization;
use App\Http\Requests\DeliveryRequest;

class AdminDeliveryController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.delivery.index',[
            'title' => 'Способы доставки',
            'items' => Delivery::paginate(20)
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeliveryRequest $request)
    {
        $item = Delivery::create([]);
        foreach ($request->name as $key => $value) {
            $item->localization()->create([
                'name' => $value,
                'locale' => $key,
                ]);
        }

        return redirect()->route('delivery.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DeliveryLocalization::where('delivery_id', $request->id)
            ->whereLocale('ru')
            ->update([
                'name' => $request->name_ru
                ]);

        DeliveryLocalization::where('delivery_id', $request->id)
            ->whereLocale('uk')
            ->update([
                'name' => $request->name_uk
                ]);

        return;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->check)
            Delivery::destroy($request->check);

        return redirect()->route('delivery.index')->with('success', 'Записи удалены');                    
    }
}
