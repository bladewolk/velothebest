<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use App\Models\Categorie;
use App\Models\Color;
use App\Models\Features;
use App\Models\Label;
use App\Models\Option;
use App\Models\Product;
use App\Models\ProductFeatures;
use App\Models\ProductOption;
use App\Models\ProductsLocalizations;
use Illuminate\Http\Request;
use LaravelLocalization;
use Session;
use Illuminate\Support\Facades\Input;
use App\Models\ProductRelated;
use App\Models\Picture;
use App\Http\Requests\ProductsRequest;

class AdminProductsController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        TODO add cat_id filter
        $items = Product::with('images')
            ->with('group')
            ->orderBy('id', 'desc')
            ->leftJoin('products_localizations', 'products.id', '=', 'products_localizations.product_id')
            ->leftJoin('categories_localizations', 'products.cat_id', '=', 'categories_localizations.categorie_id')
            ->select('products.*', 'products_localizations.name', 'categories_localizations.name as cat_name')
            ->where('products_localizations.locale', 'ru')
            ->where('categories_localizations.locale', 'ru');

        if (Input::has('cat_id'))
            $items->where('cat_id', Input::get('cat_id'));

        if (Input::has('order'))
            $items->orderBy('products.' . Input::get('order'), Input::get('by'));

        If (Product::count() == 0)
            Session::flash('info', 'У вас пока нет продуктов');


        $categories = Categorie::joinLocalization()
            ->where('parent_id', '<>', null)
            ->pluck('name', 'id')
            ->toArray();

        return view('admin.products.index', [
            'title' => 'Продукты',
            'categories' => $categories,
            'items' => $items->paginate(40)
        ]);
    }


    public function create()
    {
        $filters = Features::withVariantsRus()
            ->get();

        $categories = Categorie::converForSelectOnlyRus();

        $options = Option::convertForSelect();
        $options[0] = 'Нет';
        ksort($options);

        $status = [
            '1' => 'В наличии',
            '0' => 'Нет в наличии',
            '2' => 'Под заказ',
        ];

        $brands = Brand::convertForSelect();
        $brands[0] = 'Нет';
        ksort($brands);

        $colors = array_merge(['0' => 'Нет'], Color::pluck('name', 'id')->toArray());

        $soput = Product::with('localization')->take(500)->get();

        $labels = Label::pluck('name', 'id')->put(0, 'Нет')->toArray();
        ksort($labels);

        return view('admin.products.create', [
            'labels' => $labels,
            'status' => $status,
            'brands' => $brands,
            'title' => 'Создание продукта',
            'item' => new Product(),
            'options' => $options,
            'product_options' => [],
            'colors' => $colors,
            'categories' => $categories,
            'filters' => $filters,
            'product_filters' => [],
            'soput' => $soput,
            'soputSelected' => []
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    // ProductsRequest
    public function store(ProductsRequest $request)
    {
        $item = new Product();
        $item->fill($request->all())->save();
        $locales = array_keys(LaravelLocalization::getSupportedLocales());
        foreach ($locales as $locale) {
            $temp = [];
            foreach ($request->only('name', 'label_text', 'description', 'meta_title', 'meta_keywords', 'meta_description') as $row => $value) {
                if (empty($value[$locale]) == false)
                    $temp[$row] = $value[$locale];
            }
            ProductsLocalizations::create(array_merge($temp, ['product_id' => $item->id, 'locale' => $locale]));
        }

        // Характеристики продукта
        if ($request->option){
            $temp = [];
            foreach ($request->option as $optionID => $array) {
                foreach ($array as $optionLocale => $optionText) {
                    if (empty($optionText) == false){
                        $temp[] = [
                            'option_id' => $optionID,
                            'value' => $optionText,
                            'product_id' => $item->id,
                            'locale' => $optionLocale
                        ];
                    }
                }
            }
            ProductOption::insert($temp);    
        }
        

//        Параметры фильтрации продукта
        if ($request->product_features) {
            foreach ($request->product_features as $feature) {
                ProductFeatures::create([
                    'product_id' => $item->id,
                    'variant_id' => $feature
                ]);
            }
        }

//      Сопутствуюзщие товары
        if ($request->soputSelected){
            foreach ($request->soputSelected as $value) {
                $item->associated()->create(['related_product_id' => $value]);
            }
        }

        // Главное изображение товара
        if ($image = $request->file('image'))
            Picture::savePictureToStorage($item, $image);
//            Picture::savePicturesTest($item, $image, Product::$imageParams);

        if ($image = $request->file('images'))
            foreach ($request->file('images') as $image) {
                Picture::savePictureToStorage($item, $image);
            }

        return redirect()->route('products.index')->with('success', 'Запись создана');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    // Use This route to copy existing product with all features
    public function show($id)
    {
        $item = Product::find($id);

        $newItem = Product::create($item->toArray());

        foreach ($item->localization as $value) {
            $newItem->localization()->create($value->toArray());
        }

        foreach ($item->characteristicks as $value) {
            $newItem->characteristicks()->create($value->toArray());
        }

        foreach ($item->associated as $value) {
            $newItem->associated()->create($value->toArray());
        }

        foreach ($item->features as $value) {
            $newItem->features()->create($value->toArray());
        }

        return redirect()->route('products.edit', $newItem->id)->with('success', 'Копия создана');
    }

    public function edit($id, Request $request)
    {
        //Выбранные сопутствующие товары
        $temp = ProductRelated::where('product_id', $id)->pluck('related_product_id')->toArray();
        $soputSelected = Product::whereIn('id', $temp)
            ->with('localization')
            ->get();

        if ($request->ajax()) {
             return $this->loadAjax($request);
        }

        $item = Product::with('localization')->with('characteristicks')->find($id);

        $filters = Features::withVariantsRus()
            ->get();

        $colors = Color::pluck('name', 'id')->put(0, 'Нет')->toArray();
        ksort($colors);

        $status = ['1' => 'В наличии','0' => 'Нет в наличии','2' => 'Под заказ'];

        $labels = Label::pluck('name', 'id')->put(0,'Нет')->toArray();
        ksort($labels);

        $options = Option::convertForSelect();
        ksort($options);

        $brands = Brand::convertForSelect();

        $categories = Categorie::converForSelectOnlyRus();

        $product_features = ProductFeatures::whereProductId($id)
            ->pluck('variant_id')
            ->toArray();

        // Cопутствующие товары
        $soput = Product::where('cat_id', $item->cat_id)
            ->where('id', '<>', $id)
            ->take(500)
            ->with('localization')
            ->get();


        return view('admin.products.edit', [
            'title' => 'Редактирование записи',
            'item' => $item,
            'status' => $status,
            'brands' => $brands,
            'labels' => $labels,
            'options' => $options,
            'colors' => $colors,
            'categories' => $categories,
            'filters' => $filters,
            'product_filters' => $product_features,
            'soput' => $soput,
            'soputSelected' => $soputSelected
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    // ProductsRequest
    public function update(Request $request, $id)
    {
        if ($request->ajax())
            return $this->ajaxUpdate($request);

        $item = Product::find($id);
        $item->update($request->all());
        $locales = array_keys(LaravelLocalization::getSupportedLocales());
       
        foreach ($locales as $locale) {
            $temp = [];
            foreach ($request->only('name', 'label_text', 'description', 'meta_title', 'meta_keywords', 'meta_description') as $row => $value) {
                $temp[$row] = $value[$locale];
            }
            ProductsLocalizations::whereProductId($id)->whereLocale($locale)->update($temp);
        }

        if ($request->option){
            $item->characteristicks()->delete();

            $temp = [];
            foreach ($request->option as $optionID => $array) {
                foreach ($array as $optionLocale => $optionText) {
                    if (empty($optionText) == false){
                        $temp[] = [
                            'option_id' => $optionID,
                            'value' => $optionText,
                            'product_id' => $item->id,
                            'locale' => $optionLocale
                        ];
                    }
                }
            }
            ProductOption::insert($temp);    
        }

        foreach ($request->option as $locale => $array) {
            foreach ($array as $key => $value) {
                if (empty($request->option_text[$locale][$key]) == false) {
                    $r = ProductOption::where(function ($q) use ($locale, $request, $item, $key) {
                        $q->where('product_id', $item->id);
                        $q->where('locale', $locale);
                        $q->where('option_id', $request->option[$locale][$key]);
                    })->firstOrCreate([
                        'option_id' => $request->option[$locale][$key],
                        'product_id' => $item->id,
                        'locale' => $locale
                    ]);
                    if ($r)
                        $r->update(['value' => $request->option_text[$locale][$key]]);
                } else {
                    $r = ProductOption::where(function ($q) use ($locale, $request, $item, $key) {
                        $q->where('product_id', $item->id);
                        $q->where('locale', $locale);
                        $q->where('option_id', $request->option[$locale][$key]);
                    })->first();
                    if ($r)
                        $r->delete();
                }
            }
        }

//       Параметры фильтрации продукта
        if ($request->product_features) {
            $current = ProductFeatures::whereProductId($id)
                ->pluck('variant_id')->toArray();

            $create = array_diff($request->product_features, $current);
            $delete = array_diff($current, $request->product_features);

            ProductFeatures::whereIn('variant_id', $delete)->delete();
            foreach ($create as $item) {
                ProductFeatures::create([
                    'product_id' => $id,
                    'variant_id' => $item
                ]);
            }
        }

//      Сопутствующие товары
        if ($request->soputSelected) {
            $existingSoput = ProductRelated::whereProductId($id)->pluck('related_product_id')->toArray();
            $del = array_diff($existingSoput, $request->soputSelected);
            $create = array_diff($request->soputSelected, $existingSoput);

            if (count($del))
                $item->associated()->whereIn('related_product_id', $del)->delete();
            if (count($create)) {
                foreach ($create as $id) {
                    $item->associated()->create(['related_product_id' => $id]);
                }
            }
        } else
            ProductRelated::whereProductId($id)->delete();

//      Обновление основного изображения товара
        if ($image = $request->file('image'))
            Picture::updateOneInStorage($item, $image);

        // Изменение дополнительных изображений
        if ($image = $request->file('images')){
             foreach($request->file('images') as $image){
                Picture::savePictureToStorage($item, $image);
            }
        }

        // удаление дополнительных изображений
        if ($request->deleteImages){
            $item->images()->whereIn('id', $request->deleteImages)->delete();
        }
        return redirect()->back()->with('success', 'Запись обновлена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->check)
            Product::destroy($request->check);

        return redirect()->back()->with('success', 'Записи удалены');
    }

    public function AjaxUpdate($request)
    {
        Product::find($request->id)->update(['visible' => !$request->visible]);
        return $request->id;
    }

    // Подгрузка сопутсвтующих товаров
    public function loadAjax(Request $request)
    {
        if ($request->id){
            $soputSelected = ProductRelated::where('product_id', $request->id)->pluck('related_product_id')->toArray();
            
            $soput = Product::where('cat_id', $request->cat_id)
            ->where('id', '<>', $request->id)
            ->whereNotIn('id', $soputSelected)
            ->skip(500 * $request->skip)
            ->take(500)
            ->with('localization')
            ->get();

            if (!$soput->isEmpty())
                return $soput;
            return 0;

        } else
        {
             $soput = Product::where('cat_id', $request->cat_id)
            ->skip(500 * $request->skip)
            ->take(500)
            ->with('localization')
            ->get();

            if (!$soput->isEmpty())
                return $soput;
            return 0;
        }
    }   
}