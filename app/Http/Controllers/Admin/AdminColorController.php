<?php

namespace App\Http\Controllers\Admin;

use App\Models\Color;
use Illuminate\Http\Request;
use App\Http\Requests\ColorRequest;

class AdminColorController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.colors.index', [
            'title' => 'Цвета продуктов',
            'items' => Color::paginate(20)
        ]);
    }

    public function create()
    {
        //
    }

    public function store(ColorRequest $request)
    {
        Color::create($request->all());
        return redirect()->route('colors.index')->with('success', 'Запись создана');
    }


    public function show($id)
    {
        //
    }

    /**
     * @param Color $color
     * @return mixed
     */
    public function edit(Color $color)
    {
        return view('admin.colors.edit', [
            'title' => 'Редактирование',
            'item' => $color
        ]);
    }

    /**
     * @param Request $request
     * @param Color $color
     * @return mixed
     */
    public function update(ColorRequest $request, Color $color)
    {
        $color->update($request->all());
        return redirect()->route('colors.index')->with('success', 'Записи обновлена');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function destroy(Request $request)
    {
        Color::destroy($request->check);

        return redirect()->route('colors.index')->with('success', 'Записи удалены');
    }
}
