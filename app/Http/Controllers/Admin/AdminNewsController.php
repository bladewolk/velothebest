<?php

namespace App\Http\Controllers\Admin;

use App\Models\News;
use App\Models\NewsLocalizations;
use App\Models\Picture;
use Illuminate\Http\Request;
use App\Http\Requests\NewsRequest;
use Session;

class AdminNewsController extends AdminBaseController
{
    public function index()
    {
        $message = '';
        $count = News::count();
        if ($count == 0)
            Session::flash('warning', 'У вас нет новостей');
        return view('admin.news.index', [
            'title' => 'Новости',
            'count' => News::count(),
            'items' => News::latest()
                ->leftJoin('news_localizations', 'news.id', '=', 'news_localizations.new_id')
                ->select('news.*', 'news_localizations.name')
                ->whereLocale('ru')
                ->paginate(24)
        ]);
    }

    public function create()
    {
        return view('admin.news.create', [
            'title' => 'Создание новости',
            'page' => new News()
        ]);
    }

    public function store(NewsRequest $request)
    {
        $new = News::create($request->all());
        $locales = array_keys($request->name);
        $insert = [];

        foreach ($locales as $key) {
            $temp = [];
            foreach ($request->except('slug', 'type', '_token', 'image') as $row => $value) {
                $temp[$row] = $value[$key];
            }
            NewsLocalizations::create(array_merge($temp, ['new_id' => $new->id, 'locale' => $key]));
        }

        if ($image = $request->file('image')) {
            Picture::savePictureToStorage($new, $image);
        }

        return redirect()->route('news.index')->with('success', 'Новость создана');
    }

    public function show($id)
    {
        //
    }


    public function edit(News $news)
    {
        return view('admin.news.edit', [
            'title' => 'Редактирование новости',
            'page' => $news,
        ]);
    }

    public function update(NewsRequest $request, News $news)
    {
        $news->update($request->all());
        $locales = array_keys($request->name);

        foreach ($locales as $key) {
            $temp = [];
            foreach ($request->except('slug', 'type', '_token', 'image', '_method') as $row => $value) {
                $temp[$row] = $value[$key];
            }
            NewsLocalizations::whereNewId($news->id)->whereLocale($key)->update($temp);
        }
        if ($request->image)
            Picture::updateOneInStorage($news, $request->image);

        return redirect()->route('news.index')->with('success', 'Новость обновлена');
    }


    public function destroy(Request $request)
    {
        if (empty($request->check) == false) {
            News::destroy($request->check);
            return redirect()->route('news.index')->with('success', 'Удалено');
        } else
            return redirect()->route('news.index')->with('danger', 'Ничего не удалено');
    }
}
