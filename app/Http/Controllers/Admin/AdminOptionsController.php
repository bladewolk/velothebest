<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use App\Models\Option;
use App\Models\OptionLocalization;
use App\Models\ProductOption;
use App\Models\ProductOptionLocalization;
use App\Http\Requests\ProductOptionsRequest;
use Illuminate\Http\Request;
use Session;
use LaravelLocalization;

class AdminOptionsController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $count = Option::count();
        if ($count == 0)
            Session::flash('warning', 'Записей нет');

        return view('admin.options.index', [
            'title' => 'Характеристики',
            'count' => $count,
            'items' => Option::latest()->with(['localization' => function ($q) {
                $q->whereLocale('ru');
            }])->paginate(100)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.options.create', [
            'title' => 'Создание записи',
            'items' => new Option()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductOptionsRequest $request)
    {
        $item = Option::create([]);
        $locales = array_keys(LaravelLocalization::getSupportedLocales());

        foreach ($locales as $locale) {
            $temp = [];
            foreach ($request->except('_token') as $row => $value) {
                $temp[$row] = $value[$locale];
            }
            OptionLocalization::create(array_merge($temp, ['option_id' => $item->id, 'locale' => $locale]));
        }

        return redirect()->route('options.index')->with('success', 'Запись создана');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.options.edit', [
            'title' => 'Редактирование записи',
            'items' => Option::find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductOptionsRequest $request, $id)
    {
        $item = Option::find($id);
        $locales = array_keys(LaravelLocalization::getSupportedLocales());

        foreach ($locales as $locale) {
            $temp = [];
            foreach ($request->except('_token', '_method') as $row => $value) {
                $temp[$row] = $value[$locale];
            }
            OptionLocalization::whereOptionId($id)
                ->whereLocale($locale)
                ->update($temp);
        }

        return redirect()->route('options.index')->with('success', 'Запись обновлена');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function destroy(Request $request)
    {
        if ($request->check) {
            Option::destroy($request->check);
            return redirect()->route('options.index')->with('success', 'Записи удалены');
        }

        return redirect()->route('options.index')->with('info', 'Выберите хотя бы один обьект для удаления');
    }
}
