<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use App\Http\Requests\AdminPortfolioRequest;
use App\Models\Image;
use App\Models\Picture;
use App\Models\Portfolio;
use Illuminate\Http\Request;

class AdminPortfolioController extends AdminBaseController
{
    //Parameters for saved and delete images
    public $params = [
        [
            'width' => 600,
            'height' => 600,
            'prefix' => '',
            'path' => '/uploads/'
        ]];

    //Model can take image params by this static method!
    public static function getParams()
    {
        $obj = new self;
        return $obj->params;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.portfolio.index', [
            'title' => 'Управление портфолио',
            'count' => Portfolio::count(),
            'items' => Portfolio::paginate(15)
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.portfolio.create', [
            'title' => 'Создание альбома',
            'item' => new Portfolio()
        ]);
    }

    /**
     * @param AdminPortfolioRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminPortfolioRequest $request)
    {
        if ($request->file('image')) {
            Picture::savePicture(Portfolio::find($request->id), $request->image, $this->params);
            return redirect()->route('portfolio.show', ['id' => $request->id])->with('success', 'Изображение добавлено');
        }

        Portfolio::create($request->all());
        return redirect()->route('portfolio.index')->with('success', 'Альбом создан');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $id)
    {
        $portfolio = Portfolio::with('images')->find($id);

        return view('admin.portfolio.show', [
            'title' => $portfolio->name,
            'model' => Portfolio::find($id),
            'items' => Portfolio::find($id)->images()->get(),
            'count' => Portfolio::find($id)->images()->count()
        ]);
    }

    /**
     * @param Portfolio $portfolio
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Portfolio $portfolio)
    {
        return view('admin.portfolio.edit', [
            'title' => 'Редактирование альбома',
            'item' => $portfolio,
        ]);
    }

    /**
     * @param AdminPortfolioRequest $request
     * @param Portfolio $portfolio
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminPortfolioRequest $request, Portfolio $portfolio)
    {
        $portfolio->update($request->all());
        return redirect()->route('portfolio.index')->with('success', 'Слайд Обновлен');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        if ($request->picture && $request->check)
            Picture::deleteImages($request->check, $this->params);
        else {
            Portfolio::destroy($request->check);
        }

        return redirect()->back()->with('success', 'Успешно удалено');
    }
}
