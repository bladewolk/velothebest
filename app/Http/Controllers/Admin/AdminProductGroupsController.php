<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Models\ProductGroup;
use App\Models\Categorie;
use Illuminate\Http\Request;

class AdminProductGroupsController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.product_groups.index', [
            'title' => 'Группы продуктов',
            'items' => ProductGroup::latest()->paginate(24)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ProductGroup::create($request->all());
        return redirect()->route('groups.index')->with('success', 'Запись создана');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
         $inGroup = Product::where('relgroup', $id)
            ->leftJoin('products_localizations', 'products.id', '=', 'products_localizations.product_id')
            ->where('products_localizations.locale', 'ru')
            ->select('products.id', 'products_localizations.name')
            ->pluck('name', 'id')
            ->toArray();

        if ($request->ajax()){

            return Product::where('relgroup', '<>', $id)->orWhere('relgroup', null)
            ->with('localization')
            ->where('cat_id', $request->cat_id)
            ->skip($request->skip * 500)
            ->take(500)
            ->get();
        }

        $categories = Categorie::leftJoin('categories_localizations', 'categories.id', '=', 'categories_localizations.categorie_id')
            ->where('categories_localizations.locale', 'ru')
            ->where('categories.parent_id', '<>', null)
            ->select('categories.*', 'categories_localizations.name')
            ->pluck('name', 'id')
            ->toArray();

        $products = Product::leftJoin('products_localizations', 'products.id', '=', 'products_localizations.product_id')
            ->select('products.id', 'products.relgroup', 'products_localizations.name')
            ->where('products.relgroup', '!=', $id)->orWhere('products.relgroup', null)
            ->where('products_localizations.locale', 'ru')
            ->take(500)
            ->distinct()
            ->orderBy('products.id', 'desc')
            ->pluck('name', 'id')
            ->toArray();


        return view('admin.product_groups.edit', [
            'title' => 'Товары группы: ' . ProductGroup::find($id)->name,
            'allProducts' => $products,
            'inGroup' => $inGroup,
            'item' => ProductGroup::find($id),
            'categories' => $categories,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ProductGroup::find($id)->update($request->all());
        if (empty($request->to))
            $request->to = [];

        $have = Product::where('relgroup', $id)->pluck('id')->toArray();

        if (empty($have)){
            Product::whereIn('id', $request->to)->get()->each(function ($item, $key) use ($id) {
                $item->update(['relgroup' => $id]);
            }); 
            return redirect()->back()->with('success', 'Обновлено');                
        }
        
        $remove = array_diff($have, $request->to);
        $add = array_diff($request->to, $have);

        if (empty($remove) == false)
            Product::whereIn('id', $remove)->get()->each(function($item, $key){
                $item->update(['relgroup' => null]);
            });
        if (empty($add) == false)
            Product::whereIn('id', $add)->get()->each(function($item, $key) use ($id) {
                $item->update(['relgroup' => $id]);
            });

        return redirect()->back()->with('success', 'Обновлено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->check)
            ProductGroup::destroy($request->check);

        return redirect()->back()->with('success', 'Записи удалены');
    }
}
