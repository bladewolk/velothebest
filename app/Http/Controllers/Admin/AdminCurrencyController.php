<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Requests\CurrencyRequest;

class AdminCurrencyController extends AdminBaseController
{
    public function index()
    {
        return view('admin.currency.index', [
            'title' => 'Курс валют',
            'item' => Setting::first()
        ]);
    }

    public function update(CurrencyRequest $request)
    {
        Setting::first()->update($request->all());
        return redirect()->route('currency.index')->with('success', 'Запись обновлена');
    }
}
