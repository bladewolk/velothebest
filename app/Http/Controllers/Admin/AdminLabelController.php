<?php

namespace App\Http\Controllers\Admin;

use App\Models\Label;
use App\Models\LabelLocalization;
use Illuminate\Http\Request;
use LaravelLocalization;
use App\Http\Requests\LabelsRequest;

class AdminLabelController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $labels = Label::leftJoin('label_localizations', 'labels.id', '=', 'label_localizations.label_id')
            ->whereLocale('ru')
            ->select('labels.*', 'label_localizations.description')
            ->get();

        return view('admin.labels.index', [
            'title' => 'Лейбы',
            'count' => Label::count(),
            'items' => $labels
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.labels.create', [
            'title' => 'Создание',
            'item' => new Label()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(LabelsRequest $request)
    {
        $locales = array_keys(LaravelLocalization::getSupportedLocales());
        $label = Label::create($request->all());

        foreach ($locales as $locale) {
            LabelLocalization::create([
                'label_id' => $label->id,
                'description' => $request->description[$locale],
                'locale' => $locale
            ]);
        }

        return redirect()->route('labels.index')->with('success', 'Запись создана');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Label $label)
    {
        return view('admin.labels.edit', [
            'title' => 'Редактирование записи',
            'item' => $label
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(LabelsRequest $request, Label $label)
    {
        $locales = array_keys(LaravelLocalization::getSupportedLocales());
        $label->update($request->all());

        foreach ($locales as $locale) {
            LabelLocalization::whereLabelId($label->id)
                ->whereLocale($locale)
                ->update(['description' => $request->description[$locale]]);
        }

        $label->update($request->all());
        return redirect()->route('labels.index')->with('success', 'Запись обновлена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->has('check'))
            Label::destroy($request->check);

        return redirect()->route('labels.index')->with('success', 'Удалено');
    }
}
