<?php

namespace App\Http\Controllers\Admin;

use App\Models\Banner;
use Illuminate\Http\Request;
use App\Http\Requests\BannerUpdate;

class AdminBannerController extends AdminBaseController
{
    public function index()
    {
        /**
         * 
         */
        return view('admin.banner.index', [
            'title' => 'Баннер',
            'items' => Banner::pluck('image', 'number')->toArray()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.banner.edit', [
            'item' => Banner::whereNumber($id)->first(),
            'title' => 'Редактирование записи',
            'id' => $id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(BannerUpdate $request, $id)
    {
        $model = Banner::whereNumber($id)->first();
        if ($model){
            $model->update($request->all());
        } else{
            Banner::create(array_merge($request->except('image'), ['number' => $id]));
        }

        if ($request->file('image'))
            Banner::upadePicture($request, $id);

        return redirect()->route('banner.index')->with('success', 'Баннер обновлен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Banner::whereNumber($id)->first()->delete();
    }
}
