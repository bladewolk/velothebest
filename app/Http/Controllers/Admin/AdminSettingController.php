<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use App\Http\Requests\AdminSettingsRequest;
use App\Models\Picture;
use App\Models\Setting;
use Illuminate\Http\Request;


class AdminSettingController extends AdminBaseController
{
    private $params = [
        [
            'width' => 180,
            'height' => 110,
            'prefix' => '',
            'path' => '/uploads/'
        ]
    ];

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        return view('admin.settings.index', [
            'title' => 'Настройки',
            'settings' => Setting::firstOrCreate([])
        ]);
    }

    public function update(Request $request, Setting $setting)
    { 
        if ($logo = $request->file('logo'))
            Picture::updateLogo($setting, $logo);
            
        $setting->update($request->all());

        return redirect()->back()->with('success', 'Настройки обновлены');
    }
}
