<?php

namespace App\Http\Controllers\Admin;

use App\Models\Picture;
use App\Models\Product;
use Illuminate\Http\Request;
use Excel;
use Image;
use League\Csv\Reader;
use App\Jobs\ImportProducts;
use Queue;


class AdminImportController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.import.index', [
            'title' => 'Импорт товаров'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        excel
//        Catalog.csv
       // if ($request->file('catalog'))
       //     $request->file('catalog')->move(public_path('uploads/temp/'), 'catalog.csv');
       // if ($request->file('price'))
       //     $request->file('price')->move(public_path('uploads/temp/'), 'price.csv');
       // if ($request->file('stock'))
       //     $request->file('stock')->move(public_path('uploads/temp/'), 'stock.csv');

       // // $this->dispatch(new ImportProducts());
       // Queue::push(new ImportProducts());
       // return back()->with('success', 'Импорт начался');

        if (!$request->file('catalog') || !$request->file('price') || !$request->file('stock'))
            return redirect()->back()->with('error', 'Нужны все 3 файла!');
        
        $reader = Excel::load($request->file('catalog'), function ($reader) {
        }, 'windows-1251');
        $catalog = [];
        foreach ($reader->all() as $index => $row) {
            $catalog[intval($row->code)] = $row;
        }
//        Price.csv
        $reader = Excel::load($request->file('price'), function ($reader) {
        }, 'windows-1251');
        $price = [];
        foreach ($reader->all() as $index => $row) {
            $price[intval($row->code)] = $row->price;
        }
//        Stock.csv
        $reader = Excel::load($request->file('stock'), function ($reader) {
        }, 'windows-1251');
        $stock = ['in_stock'];
        foreach ($reader->all() as $index => $row) {
            $stock[intval($row->first())] = $row->last();
        }
////MERGE
        foreach ($catalog as $key => $val) {
            if (isset($price[$key]))
                $val['price'] = $price[$key];
            if (isset($stock[$key]))
                $val['in_stock'] = $stock[$key];
            $catalog[$key] = $val;
        }
//        WRITE TO DATABASE
        foreach ($catalog as $item) {
            $product = Product::create([
                'code' => intval($item->code),
                'slug' => preg_replace('/\s/', '-', $item->name) . $item->code,
                'cat_id' => 2,
                'in_stock' => intval($item->in_stock),
                'price' => is_numeric($item->price) ? $item->price : intval($item->price),
                'visible' => true
            ]);
            $product->localization()->create([
                'name' => $item->name,
                'locale' => 'ru'
            ]);
            $product->localization()->create([
                'name' => $item->name,
                'locale' => 'ua'
            ]);
        }
//************************************************************
//        LEAGUE
//        $path = $request->file('catalog')->getRealPath();
//        $reader = Reader::createFromPath($path);
//        $catalog = [];
//        $header = ['code', 'cat_id', 'name'];
//        foreach ($reader as $index => $row) {
//            dd(1);
//            $temp = explode(';', $row[0]);
//            $catalog[$temp[0]] = array_combine($header, array_merge($temp));
//        }
//
//        $path = $request->file('price')->getRealPath();
//        $reader = Reader::createFromPath($path);
//        $price = [];
//        $header = ['code', 'price'];
//
//        foreach ($reader as $index => $row) {
//            $temp = explode(';', $row[0]);
//            $price[intval($temp[0])] = array_combine($header, $temp);
//        }
//
//        $path = $request->file('price')->getRealPath();
//        $reader = Reader::createFromPath($path);
//        $stock = [];
//        $header = ['code', 'in_stock'];
//
//        foreach ($reader as $index => $row) {
//            $temp = explode(';', $row[0]);
//            $stock[intval($temp[0])] = array_combine($header, $temp);
//        }
//
//        foreach ($catalog as $key => $val) {
//            $val['price'] = $price[$key]['price'];
//            $val['in_stock'] = $stock[$key]['in_stock'];
//            $catalog[$key] = $val;
//        }
//
//        $counter = 0;
//        foreach ($catalog as $item) {
//            dd($item);
//            $item['cat_id'] = 2;
//            $product = Product::create($item);
//            $product->localization()->create([
//                'name' => $item['name'],
//                'locale' => 'ru'
//            ]);
//
//            if ($counter == 5)
//                return;
//        }
        return redirect()->route('import.index')->with('success', 'Успех!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
