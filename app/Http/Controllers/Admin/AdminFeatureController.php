<?php

namespace App\Http\Controllers\Admin;

use App\Models\FeatureLocalization;
use App\Models\Features;
use App\Models\FeatureVariant;
use App\Models\FeatureVariantLocalization;
use App\Models\Variant;
use App\Models\VariantLocalization;
use Illuminate\Http\Request;
use LaravelLocalization;

class AdminFeatureController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Features::with('localization')->paginate(20);

        return view('admin.features.index', [
            'title' => 'Параметры фильтрации',
            'items' => $items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $locales = array_keys(LaravelLocalization::getSupportedLocales());

        if ($request->filter == 'filter') {
            $feature = Features::create([]);
            foreach ($locales as $locale) {
                FeatureLocalization::create([
                    'feature_id' => $feature->id,
                    'name' => $request->name[$locale],
                    'locale' => $locale
                ]);
            }
        }

        if ($request->filter == 'variant') {
            $variant = Variant::create([]);
            FeatureVariant::create([
                'feature_id' => $request->feature_id,
                'variant_id' => $variant->id
            ]);
            foreach ($locales as $locale) {
                VariantLocalization::create([
                    'variant_id' => $variant->id,
                    'name' => $request->name[$locale],
                    'locale' => $locale
                ]);
            }
        }

        return redirect()->back()->with('success', 'Запись создана');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feature = Features::leftJoin('feature_localizations', 'features.id', '=', 'feature_localizations.feature_id')
            ->whereLocale('ru')
            ->select('features.*', 'feature_localizations.name')
            ->find($id);

        $variants = Features::whereId($id)
            ->with(['variants' => function ($q) {
                $q->with('localization');
            }])->first();

        return view('admin.features.edit', [
            'count' => $variants->variants->count(),
            'title' => 'Фильтр - ' . $feature->name,
            'feature' => $feature,
            'items' => $variants->variants
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->parent == 1) {
            FeatureLocalization::where('feature_id', $request->id)
                ->whereLocale('ru')
                ->update([
                    'name' => $request->name_ru
                ]);
            FeatureLocalization::where('feature_id', $request->id)
                ->whereLocale('uk')
                ->update([
                    'name' => $request->name_uk
                ]);
        } else {
            VariantLocalization::where('variant_id', $request->id)
                ->whereLocale('ru')
                ->update([
                    'name' => $request->name_ru
                ]);
            VariantLocalization::where('variant_id', $request->id)
                ->whereLocale('uk')
                ->update([
                    'name' => $request->name_uk
                ]);
        }

        return null;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function destroy(Request $request)
    {
        if ($request->parent) {
            Features::destroy($request->check);
        } else {
            Variant::destroy($request->check);
        }
        return redirect()->back()->with('success', 'Записи удалены');
    }
}
