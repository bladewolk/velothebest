<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use App\Http\Requests\AdminSliderDestroyRequest;
use App\Http\Requests\SliderCreate;
use App\Http\Requests\SliderUpdate;
use App\Models\Picture;
use App\Models\Slider;
use Illuminate\Http\Request;

class AdminSliderController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.slider.index', [
            'title' => 'Сдайдер',
            'count' => Slider::count(),
            'items' => Slider::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.create', [
            'title' => 'Создание слайда',
            'item' => new Slider()
        ]);
    }

    public function store(SliderCreate $request)
    {
        $slide = Slider::create([]);

        if ($request->file('image'))
            Picture::saveSlide($slide, $request->file('image'), 'image');
        
        if ($request->file('image_background')) {
            Picture::saveSlide($slide, $request->file('image_background'), 'image_background');
        }

        return redirect()->route('slider.edit', $slide->id)->with('success', 'Слайд создан');
    }

    /**
     * @param Slider $slider
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Slider $slider)
    {
        return view('admin.slider.edit', [
            'title' => 'Редактирование слайда',
            'item' => $slider
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(SliderUpdate $request, Slider $slider)
    {
        if ($request->file('image'))
            Picture::updateSlide($slider, $request->file('image'), 'image', $slider->image);

        if ($request->file('image_background'))
            Picture::updateSlide($slider, $request->file('image_background'), 'image_background', $slider->image_background);

        return redirect()->back()->with('success', 'Слайд Обновлен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->check)
            Slider::destroy($request->check);

        return redirect()->back()->with('success', 'Слайды удалены');
    }
}
