<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\PaymentLocalization;

class AdminPaymentController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.payment.index',[
            'title' => 'Способы оплаты',
            'items' => Payment::paginate(20)
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = Payment::create([]);
        foreach ($request->name as $key => $value) {
            $item->localization()->create([
                'payment_id' => $item->id,
                'name' => $value, 
                'locale' => $key
                ]);
        }

        return redirect()->route('payment.index')->with('success', 'Запись создана');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        PaymentLocalization::where('payment_id', $request->id)
            ->whereLocale('ru')
            ->update([
                'name' => $request->name_ru
                ]);

        PaymentLocalization::where('payment_id', $request->id)
            ->whereLocale('uk')
            ->update([
                'name' => $request->name_uk
                ]);

        return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->check)
            Payment::destroy($request->check);

        return redirect()->back()->with('success', 'Записи удалены');
    }
}
