<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AdminPageCreateRequest;
use App\Http\Requests\AdminPagesUpdate;
use App\Models\Categorie;
use App\Models\Page;
use App\Models\PageLocalization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AdminPageController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.page.index', [
            'title' => 'WEB страницы',
            'type' => Input::get('type'),
            'count' => Page::whereType(Input::get('type'))->orderBy('lft', 'asc')->get()->count(),
            'pages' => json_encode(array_values(Page::whereType(Input::get('type'))
                ->leftJoin('page_localizations', 'pages.id', '=', 'page_localizations.page_id')
                ->where('page_localizations.locale', 'ru')
                ->select('pages.*', 'page_localizations.name')
                ->orderBy('lft', 'asc')
                ->get()
                ->toHierarchy()->toArray()))
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.page.create', [
            'title' => 'Создание страницы',
            'type' => Input::get('type'),
            'page' => new Page()
        ]);
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $page = Page::create($request->all());
        $locales = array_keys($request->name);
        $insert = [];

        foreach ($locales as $key) {
            $temp = [];
            foreach ($request->except('slug', 'type', '_token') as $row => $value) {
                $temp[$row] = $value[$key];
            }
            $insert[] = array_merge($temp, ['page_id' => $page->id, 'locale' => $key]);
        }

        PageLocalization::insert($insert);
        return redirect()->route('page.index', ['type' => $request->type])->with('success', 'Страница создана');
    }


    public function edit(Page $page)
    {
        return view('admin.page.edit', [
            'title' => 'Редактирование страницы',
            'type' => Input::get('type'),
            'page' => $page,
        ]);
    }

    /**
     * @param Request $request
     * @param Page $page
     * @return mixed
     */
    public function update(AdminPagesUpdate $request, Page $page)
    {
        $page->update($request->all());
        $locales = array_keys($request->name);

        foreach ($locales as $key) {
            $update = [];
            foreach ($request->except('slug', 'type', '_token', '_method') as $row => $value) {
                $update[$row] = $value[$key];
            }
            PageLocalization::wherePageId($page->id)
                ->whereLocale($key)
                ->update($update);
        }

        return redirect()
            ->route('page.index', ['type' => Input::get('type')])
            ->with('success', 'Запись обновлена');
    }


    public function destroy(Request $request)
    {
        if ($request->action == 'destroy') {
            Page::whereIn('id', $request->checked)->delete();

            return json_encode(array_values(Page::whereType($request->type)
                ->orderBy('lft', 'asc')
                ->get()
                ->toHierarchy()
                ->toArray()));
        } else
            Page::find($request->id)->delete();

        return;
    }
}
