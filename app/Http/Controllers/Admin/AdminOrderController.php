<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderProduct;

class AdminOrderController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $statusColors = ['rgba(0, 188, 212, 0.68)', 'rgba(91, 192, 222, 0.23)', 'rgba(146, 210, 72, 0.25)', 'rgba(255, 87, 34, 0.22)'];
        $statusText = ['Новый', 'Принят', 'Выполнен', 'Отменен'];
        $orders = Order::latest();

        if ($request->input('sort') && $request->input('by'))
            $orders->orderBy($request->input('sort'), $request->input('by'));

        return view('admin.orders.index', [
            'title' => 'Заказы',
            'items' => $orders->paginate(25),
            'statusColors' => $statusColors,
            'statusText' => $statusText
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = [
            0 => 'Новый',
            1 => 'Принят',
            2 => 'Выполнен',
            3 => 'Отменен',
        ];
        $order = Order::with('user')
            ->with('products.product.localization')
            ->find($id);

        return view('admin.orders.edit', [
            'title' => 'Заказ №' . $order->id,
            'item' => $order,
            'status' => $status
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->ajax()) {
            $status = [
                1 => 'Принят',
                2 => 'Выполнен',
                3 => 'Отменен',
            ];
            $statusColors = ['rgba(60, 151, 162, 0.18)', 'rgba(91, 192, 222, 0.23)', 'rgba(146, 210, 72, 0.25)', 'rgba(255, 87, 34, 0.22)'];

            $newStatus = 0;
            if ($request->status == 'apply')
                $newStatus = 1;
            if ($request->status == 'done')
                $newStatus = 2;
            if ($request->status == 'cancel')
                $newStatus = 3;

            $item = Order::find($id);

            if ($newStatus == 3 && $item->status == 1)
                $this->Cancel($item);

            if ($newStatus == 1)
                $this->Apply($item);
            
            $item->update(['status' => $newStatus]);

            return [
                'status' => $request->status,
                'id' => $id,
                'color' => $statusColors[$newStatus],
                'statusText' => $status[$newStatus]
            ];
        }

        $order = Order::with('products.product')->find($id); 
        $newCounts = array_combine($request->orderProductId, $request->count);
        $newTotalPrice = 0;

        foreach ($order->products as $item) {
            $item->update(['count' => $newCounts[$item->id]]);
            $newTotalPrice += $newCounts[$item->id] * $item->price;
        }

        $order->update([
                'total_price' => $newTotalPrice,
                'status' => isset($request->status) ? $request->status : 0,
            ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->ajax()) {
            OrderProduct::find($request->id)->delete();
            return;
        }

        Order::destroy($request->check);
        return redirect()->back()->with('success', 'Записи удалены');
    }

    // Если статус заказа = Принят, пересчитываем все значения товаров на складе.
    private function Apply($order){
        foreach ($order->products as $orderProduct) {
            $orderProduct->product->update([
                    'in_stock' => $orderProduct->product->in_stock - $orderProduct->count
                ]);
        }
        return;
    }

    // Если заказ
    private function Cancel($order){
        foreach ($order->products as $orderProduct) {
            $orderProduct->product->update([
                    'in_stock' => $orderProduct->product->in_stock + $orderProduct->count
                ]);
        }
        return;
    }
}
