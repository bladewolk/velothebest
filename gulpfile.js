var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var jsmin = require('gulp-jsmin');
var rename = require('gulp-rename');

gulp.task('serve', ['sass'], function () {

    browserSync.init({
        proxy: "http://velothebest/"
    });

    gulp.watch("resources/assets/sass/*.scss", ['sass']);
    gulp.watch("resources/assets/js/*.js", ['minify']);
    gulp.watch("resources/assets/sass/admin/*.scss", ['sassadmin']);
    gulp.watch("resources/views/**/*.*.php").on('change', browserSync.reload);
    gulp.watch("app/Http/**/*.php").on('change', browserSync.reload);
    gulp.watch("public/js/*.js").on('change', browserSync.reload);

});

gulp.task('sass', function () {
    return gulp.src("resources/assets/sass/*.scss")
        .pipe(sass({outputStyle: 'compressed'}))
        .on('error', catchErr)
        .pipe(gulp.dest("public/css"))
        .pipe(browserSync.stream());
});

gulp.task('sassadmin', function () {
    return gulp.src("resources/assets/sass/admin/*.scss")
        .pipe(sass({outputStyle: 'compressed'}))
        .on('error', catchErr)
        .pipe(gulp.dest("public/admin/css"))
        .pipe(browserSync.stream());
});

function catchErr(error) {
    console.log(error);
    this.emit('end');
}

gulp.task('minify', function () {
    gulp.src('resources/assets/js/*.js')
        .pipe(jsmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('public/js'));
});


gulp.task('default', ['serve']);