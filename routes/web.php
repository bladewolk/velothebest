<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// TODO home page slider image by url! 


Auth::routes();



Route::group(['namespace' => 'Admin', 'prefix' => 'master', 'middleware' => ['auth', 'isMaster']], function () {
    Route::get('/', 'AdminHomeController@index')->name('master');
    Route::get('/products_management', 'AdminHomeController@productManagment')->name('product_managment');

    Route::resource('users', 'AdminUserController');
    Route::resource('settings', 'AdminSettingController', ['only' => ['edit', 'update']]);
    Route::resource('slider', 'AdminSliderController', ['except' => ['show']]);
    Route::resource('banner', 'AdminBannerController', ['except' => ['show', 'create', 'store']]);

    Route::post('rebuildtree', 'AdminBaseController@rebuildTree')->name('rebuildTree');


    Route::resource('page', 'AdminPageController', ['except' => ['show']]);
    Route::resource('portfolio', 'AdminPortfolioController');
    Route::resource('news', 'AdminNewsController', ['except' => ['show']]);
    Route::resource('currency', 'AdminCurrencyController', ['except' => ['show', 'destroy', 'create', 'edit']]);

    Route::group(['prefix' => 'products_management'], function () {
        Route::resource('categories', 'AdminCategoriesController', ['except' => ['show']]);
        Route::resource('brands', 'AdminBrandsController', ['except' => ['show']]);
        Route::resource('products', 'AdminProductsController');
        Route::get('products/loadAjax', 'AdminProductsController@loadAjax');
        Route::resource('labels', 'AdminLabelController', ['except' => ['show']]);
        Route::resource('options', 'AdminOptionsController', ['except' => ['show']]);
        Route::resource('colors', 'AdminColorController', ['except' => ['show', 'create']]);
        Route::resource('features', 'AdminFeatureController', ['except' => ['show']]);
        Route::resource('delivery', 'AdminDeliveryController', ['except' => ['show', 'create']]);
        Route::resource('payment', 'AdminPaymentController', ['except' => ['show', 'create']]);
        Route::resource('groups', 'AdminProductGroupsController');

    });

    // Orders
    Route::resource('order', 'AdminOrderController');
    Route::resource('comments', 'AdminCommentsController', ['except' => ['show', 'create', 'edit', 'store']]);
    Route::resource('questions', 'AdminProductQuestion');
    Route::resource('feedback', 'AdminFeedbackController');
    Route::resource('import', 'AdminImportController');

});


Route::group(['prefix' => LaravelLocalization::setLocale()], function () {
    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
    Route::get('/', 'Site\HomeController@index');
    Route::get('/loadAjax/{data}', 'Site\HomeController@ajax');
    Route::get('/loadCart/{data}', 'Site\HomeController@loadCart');
    Route::post('/makeComment', 'Site\HomeController@makeComment');
    Route::post('question', 'Site\HomeController@makeQuestion');
    Route::post('feedback', 'Site\HomeController@makeFeedback');
    Route::get('/search', 'Site\HomeController@search');
    Route::get('catalog', 'Site\CatalogController@index');
    Route::get('catalog/{name}', 'Site\CatalogController@parse');
    Route::get('/personal', 'Site\PersonalController@index');
    Route::get('/personal/history', 'Site\PersonalController@history');
    Route::post('/personal/update', 'Site\PersonalController@update');
    Route::get('/cart', 'Site\OrderController@showCart');
    Route::get('/brand/{brand}', 'Site\PageController@showBrand');
	//Glide
    Route::get('img/{path}', function ($path, League\Glide\Server $server, Illuminate\Http\Request $request) {
        if ($request->w > 2000 || $request->h > 1000)
            return null;
        $server->outputImage('/images/' . $path, $request->all());
    })->where('path', '.+');
    Route::get('news/{new?}', 'Site\NewsController@index');
    Route::post('/addToCart', 'Site\OrderController@addToCart');
    Route::get('/makeconment', 'Site\HomeController@makeconment');
    Route::post('/dropFromCart', 'Site\OrderController@dropFromCart');
    Route::post('makeOrder', 'Site\OrderController@makeOrder');
    Route::get('/logout', function () {
        Auth::logout();
        return redirect('/');
    });
    Route::get('{page}', 'Site\PageController@index');
});