<?php

use Illuminate\Database\Seeder;

class Settings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Setting::create([
            'phones' => ['(050) 74 82 196', '(050) 74 82 195', '(050) 74 82 194'],
            'slider_speed' => 777,
            'site_name' => 'VeloTheBest',
            'currency_uah' => 28.73
        ]);
    }
}
