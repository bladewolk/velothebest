<?php

use Illuminate\Database\Seeder;

class Options extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translit = array(

            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'yo', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'й' => 'j', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'x', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shh',
            'ь' => '\'', 'ы' => 'y', 'ъ' => '\'\'',
            'э' => 'e\'', 'ю' => 'yu', 'я' => 'ya',
            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'YO', 'Ж' => 'Zh', 'З' => 'Z',
            'И' => 'I', 'Й' => 'J', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'X', 'Ц' => 'C',
            'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SHH',
            'Ь' => '\'', 'Ы' => 'Y\'', 'Ъ' => '\'\'',
            'Э' => 'E\'', 'Ю' => 'YU', 'Я' => 'YA',

        );
        $locales = ['ua', 'ru'];

        $array = [
            'Рама', 'Вилка', 'Рулевые чашки', 'Манетки', 'Задний переключатель', 'Передний переключатель', 'Шатуны', 'Каретка',
            'Цепь', 'Кассета', 'Тормоза', 'Колеса', 'Покрышки', 'Вынос', 'Руль', 'Грипсы', 'Подседельный штырь', 'Седло'
        ];

        foreach ($array as $value) {
            $item = \App\Models\Option::create([]);
            foreach ($locales as $locale) {
                if ($locale == 'uk') {
                    \App\Models\OptionLocalization::create([
                        'option_id' => $item->id,
                        'locale' => $locale,
                        'name' => strtr($value, $translit)
                    ]);
                } else {
                    \App\Models\OptionLocalization::create([
                        'option_id' => $item->id,
                        'locale' => $locale,
                        'name' => $value
                    ]);
                }
            }
        }
    }
}
