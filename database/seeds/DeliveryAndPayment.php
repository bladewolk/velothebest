<?php

use Illuminate\Database\Seeder;
use App\Models\Delivery;
use App\Models\Payment;

class DeliveryAndPayment extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locales = ['ru', 'ua'];

        for($i = 0; $i< 4; ++$i){
            $item = Delivery::create([]);
			foreach ($locales as $key) {
	    		$item->localization()->create([
	    		'name' => 'Доставка'. str_random(2).''.$key,
	    		'locale' => $key
    		]);
        	}
		}
        

        for($i = 0; $i< 4; ++$i){
    	   $item = Payment::create([]);
         	foreach ($locales as $key) {
				$item->localization()->create([
	    		'name' => 'Оплата'. str_random(2).''.$key,
	    		'locale' => $key
	    		]);
	        }
        }
     
    }
}
