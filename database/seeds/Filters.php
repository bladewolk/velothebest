<?php

use App\Models\FeatureLocalization;
use App\Models\Features;
use App\Models\FeatureVariant;
use App\Models\Variant;
use App\Models\VariantLocalization;
use Illuminate\Database\Seeder;

class Filters extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filters = [
            'Размер колес' => [
                '12', '13', '14', '15', '16'
            ],
            'Длина руля' => [
                '15см', '16cм', '30см'
            ],
            'Размер протектора' => [
                '10mm', '12mm', '15mm'
            ]
        ];

        foreach ($filters as $filterName => $array) {
            $feature = Features::create([]);
            FeatureLocalization::create([
                'feature_id' => $feature->id,
                'locale' => 'ru',
                'name' => $filterName
            ]);
            FeatureLocalization::create([
                'feature_id' => $feature->id,
                'locale' => 'ua',
                'name' => $filterName
            ]);
            foreach ($array as $variant) {
                $var = Variant::create([]);
                VariantLocalization::create([
                    'variant_id' => $var->id,
                    'name' => $variant,
                    'locale' => 'ru'
                ]);
                VariantLocalization::create([
                    'variant_id' => $var->id,
                    'name' => $variant,
                    'locale' => 'ua'
                ]);
                FeatureVariant::create([
                    'feature_id' => $feature->id,
                    'variant_id' => $var->id
                ]);
            }
        }
    }
}
