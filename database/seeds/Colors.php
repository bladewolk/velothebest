<?php

use Illuminate\Database\Seeder;

class Colors extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colors = [
            "Черный" => "#000000",
            "Белый" => "#ffffff",
            "Серый" => "#a3a3a3",
            "Голубой" => "#2cadcd",
            "Красный" => "#ff1c1c",
        ];

        foreach ($colors as $name => $value) {
            \App\Models\Color::create([
                'name' => $name,
                'value' => $value
            ]);
        }
    }
}
