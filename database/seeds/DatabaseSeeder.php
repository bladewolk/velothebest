<?php

use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        User::insert([
            'name' => 'admin',
            'email' => 'dvacom@user.u',
            'password' => bcrypt('world'),
            'permission' => 'root'
        ]);


        $this->call('Categories');
        $this->command->info('Categories table data inserted');

        $this->call('DeliveryAndPayment');
        $this->command->info('DeliveryAndPayment tables data inserted');

        $this->call('Labels');
        $this->command->info('Labels table data inserted');

        $this->call('Brands');
        $this->command->info('Brands table data inserted');

        $this->call('Settings');
        $this->command->info('Settings table data inserted');

        $this->call('Pages');
        $this->command->info('Pages table data inserted');

        $this->call('Options');
        $this->command->info('Options table data inserted');

        $this->call('Colors');
        $this->command->info('Colors table data inserted');

        $this->call('Filters');
        $this->command->info('Filters table data inserted');

        $this->call('Products');
        $this->command->info('Products table data inserted');
    }
}
