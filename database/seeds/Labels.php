<?php

use Illuminate\Database\Seeder;

class Labels extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locales = ['ru', 'ua'];
        $labels = [
            ['name' => 'Новика', 'color' => '#2ae82a'],
            ['name' => 'Акция', 'color' => '#2ae8d4'],
            ['name' => 'Распродажа', 'color' => '#822ae8'],
            ['name' => 'Скидка', 'color' => '#f51919'],
        ];

        foreach ($labels as $label) {
            $item = \App\Models\Label::create([
                'color' => $label['color'],
                'name' => $label['name']
            ]);
            foreach ($locales as $locale) {
                \App\Models\LabelLocalization::create([
                    'label_id' => $item->id,
                    'locale' => $locale,
                    'description' => ''
                ]);
            }
        }
    }
}
