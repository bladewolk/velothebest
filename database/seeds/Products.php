<?php

use Illuminate\Database\Seeder;

class Products extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i = 0; $i < 70; $i++) {
            $code = rand(1000, 9999);
            $name = '№' . $i . ' Product-' . $code;
            $item = \App\Models\Product::create([
                'slug' => $name,
                'cat_id' => 2,
                'status' => 1,
                'code' => $code,
                'price' => rand(1, 100),
                'price_discount' => 0,
                'in_stock' => rand(5, 15),
            ]);

            \App\Models\ProductsLocalizations::create([
                'name' => $name,
                'product_id' => $item->id,
                'locale' => 'ru'
            ]);
            \App\Models\ProductsLocalizations::create([
                'name' => $name,
                'product_id' => $item->id,
                'locale' => 'ua'
            ]);
        }
    }
}
