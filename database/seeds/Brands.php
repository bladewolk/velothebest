<?php

use Illuminate\Database\Seeder;

class Brands extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locales = ['ru', 'ua'];
        for ($i = 0; $i < 20; $i++) {
            $name = 'Brand-' . str_random(10);
            $brand = \App\Models\Brand::create([
                'slug' => $name
            ]);
            foreach ($locales as $locale) {
                \App\Models\BrandsLocalization::create([
                    'brand_id' => $brand->id,
                    'locale' => $locale,
                    'name' => $name . '-' . $locale,
                    'body' => str_random(500) . ' THAT BRAND' . $name,
                    'meta_title' => $name . ' brand meta_title',
                    'meta_keywords' => $name . ' brand meta_keywords',
                    'meta_description' => $name . ' brand meta_description',
                ]);
            }
        }
    }
}
