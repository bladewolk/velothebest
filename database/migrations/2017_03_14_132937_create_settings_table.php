<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->char('site_name', 255)->default('');
            $table->char('phones', 50)->nullable();
            $table->char('info', 255)->nullable();
            $table->integer('slider_speed')->default(500);
            $table->char('facebook', 255)->nullable();
            $table->char('instagram', 255)->nullable();
            $table->char('youtube', 255)->nullable();
            $table->float('currency_uah')->nullable();
            $table->float('currency_rub')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
