<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->char('slug', 255)->nullable();
            $table->integer('status')->nullable();
            $table->integer('brand_id')->nullable();
            $table->integer('cat_id')->nullable();
            $table->integer('main_feature')->nullable();
            $table->char('main_feature_text', 255)->nullable();
            $table->integer('color_id')->nullable();
            $table->char('code', 100)->nullable();
            $table->boolean('visible')->nullable();
            $table->integer('label_id')->nullable();
            $table->float('price')->nullable();
            $table->float('price_discount')->nullable();
            $table->integer('rating')->nullable();
            $table->integer('relgroup')->nullable();
            $table->integer('in_stock')->default(0);
            $table->char('video', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
