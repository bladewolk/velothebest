<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageLocalizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_localizations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_id');
            $table->char('name', 255)->nullable();
            $table->text('body')->nullable();
            $table->char('locale', 5)->nullable();
            $table->char('meta_title', 255)->nullable();
            $table->char('meta_keywords', 255)->nullable();
            $table->char('meta_description', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_localizations');
    }
}
