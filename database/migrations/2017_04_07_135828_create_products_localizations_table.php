<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsLocalizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_localizations', function (Blueprint $table) {
            $table->increments('id');
            $table->char('name', 255)->nullable();
            $table->integer('product_id')->nullable();
            $table->integer('label_id')->nullable();
            $table->char('label_text', 50)->nullable();
            $table->char('locale', 10)->nullable();
            $table->text('description')->nullable();
            $table->char('meta_title', 255)->nullable();
            $table->char('meta_keywords', 255)->nullable();
            $table->char('meta_description', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_localizations');
    }
}
