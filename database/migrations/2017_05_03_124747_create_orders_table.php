<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status')->default(0);
            $table->char('name', 255)->nullable();
            $table->char('phone', 255)->nullable();
            $table->char('email', 100)->nullable();
            $table->float('total_price')->default(0);
            $table->char('region', 255)->nullable();
            $table->char('city', 255)->nullable();
            $table->text('comment')->nullable();
            $table->integer('delivery_id')->nullable();
            $table->integer('payment_id')->nullable();
            $table->float('currency');
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
