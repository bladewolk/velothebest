$(document).ready(function () {
    var clicked = 0;
    var messages = ['Плохой', 'Нормальный', 'Нормальный', 'Нормальный', 'Хороший'];

    $(document).on('mouseenter', '.rating-stars > i', function (event) {
        $(this).removeClass('fa-star-o');
        $(this).addClass('fa-star');
        current = $(this).data('id');
        $('.rating-stars > i').each(function (index, el) {
            if ($(el).data('id') < current) {
                $(el).removeClass('fa-star-o fa-star');
                $(el).addClass('fa-star');
            }
            if ($(el).data('id') > current) {
                $(el).removeClass('fa-star-o fa-star');
                $(el).addClass('fa-star-o');
            }
        });
    });
    $(document).on('mouseleave', '.rating-stars > i', function (event) {
        if (clicked == 0) {
            $('.rating-stars > i').removeClass('fa-star fa-star-o');
            $('.rating-stars > i').addClass('fa-star-o');
        } else {
            $('.rating-stars > i').each(function (index, el) {
                if ($(el).data('id') <= clicked) {
                    $(el).removeClass('fa-star-o fa-star');
                    $(el).addClass('fa-star');
                }
                if ($(el).data('id') > clicked) {
                    $(el).removeClass('fa-star-o fa-star');
                    $(el).addClass('fa-star-o');
                }
            });
        }
    });

    $(document).on('click', '.rating-stars > i', function (event) {
        clicked = $(this).data('id');
        $('#rating-input').val(clicked);
        $('.rating-text').html(messages[clicked - 1]);
    });

    $(document).on('submit', '.make-comment-form', function (event) {
        event.preventDefault();

        $.post('/makeComment',{
            _token: $('meta[name="csrf-token"]').attr('content'),
            id: $('input[name="product_id"]').val(),
            name: $('input[name="name"]').val(),
            email: $('input[name="email"]').val(),
            rating: $('input[name="rating"]').val(),
            comment: $('textarea[name="comment"]').val(),
        })
            .done(function (res) {
                $('.modal-wrapper').load("/loadAjax/success", function () {
                    $(this).find('p').html(res);
                    $(this).toggleClass('show-modal-on');
                    setTimeout(function () {
                        $('.modal-wrapper').toggleClass('show-modal-on');
                    }, 1500);
                });
            })
            .fail(function (res) {
                console.error(res);
            });
    });

});