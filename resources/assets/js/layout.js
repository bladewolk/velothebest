$(document).ready(function () {
    //Mask for phone input
    $(".phone-modal").mask("+38(999) 999 9999");
    //On select change
    $('.localization').change(function () {
        location.href = $(this).find('option:selected').data('link');
    });
    //Burger Show/Hide
    $(".burger i").click(function () {
        $('.show-burger').toggleClass('show-this-burger');
    });
    $(".close-burger").click(function () {
        $('.show-burger').toggleClass('show-this-burger');
    });
//    Main navigation sublist height fix
    $(document).on('mouseenter', '.sub-name', function () {
        height = $(this).find('.sub-descr').css('height');
        curheight = $('.show-sub').css('height');
        if (parseInt(curheight) < parseInt(height)) {
            $('.show-sub').css({height: height});
        }
    });
    $(document).on('mouseleave', '.sub-name', function () {
        $('.sub-descr').css({height: 'auto'});
        $('.show-sub').css({height: 'auto'});
    })

//    Show modal window on click Register/Callback/Login/Answer
    $('.register, .sign-in, .callback-phone, .answer, .edit-personal, #edit-password').click(function () {
        lang = document.documentElement.lang;
        $('.modal-wrapper').load('/'+lang+"/loadAjax/" + $(this).data("view"), function () {
            $(".phone-modal").mask("+38(999) 999 9999");
            $(this).toggleClass('show-modal-on');
        });
    });
    //Close modal windows FA-TIMES
    $(document).on('click', ".close-form", function () {
        $('.modal-wrapper').toggleClass('show-modal-on');
    });

//    Show Modal message after make order or answered item
    $(document).on('submit', '.modal-callback-phone', function (event) {
        event.preventDefault();
        $.post('/feedback', {
            _token: $('meta[name="csrf-token"]').attr('content'),
            phone: $(this).find('input').val(),
        }, function (res) {
            $('.modal-wrapper').toggleClass('show-modal-on');
            $('.modal-wrapper').load("/loadAjax/success", function () {
                $(this).find('p').html(res);
                $(this).toggleClass('show-modal-on');
                setTimeout(function () {
                    $('.modal-wrapper').toggleClass('show-modal-on');
                }, 1500);
            });
        });
    });
//    Show Phones
    $('.show-phones').on('click', function () {
        $('.header-phones').toggleClass('hidden-sm');
    });


//    On click buy-item
    $(document).on('click', '.buy-this-item', function (event) {
        event.preventDefault();
        button = $(this);
        $.post('/addToCart', {
            _token: $('meta[name="csrf-token"]').attr('content'),
            product_id: $(this).data("productid")
        }, function (response) {
            $('.modal-wrapper').load("/loadAjax/incart", function () {
                $(this).toggleClass('show-modal-on');
            });
            $('.cart-wrapper').load('/loadCart/cart-top');
            $('.footer-cart').load('/loadCart/footer-cart');
            button.toggleClass('button-in-cart buy-this-item');
            button.html(response);
        });
    });

//    On change currency
    $('.currency').change(function () {
//                newLink = location.href;
//                if (newLink.match(/\?/)) {
//                    if (newLink.match(/currency=/)) {
//                        location.href = newLink.replace(/currency=[a-z]{3}/, 'currency=' + $('.currency option:selected').data('value'));
//                    } else
//                        location.href = newLink + '&currency=' + $('.currency option:selected').data('value');
//                }
//                else {
//                    location.href = newLink + '?currency=' + $('.currency option:selected').data('value');
//                }
            location.href = location.pathname + '?currency=' + $("option:selected", this).data('value');
        }
    )
//    Input Search
    $('.search').keyup(function (event) {
        if (event.which == 13 && this.value.length >= 4) {
            location.href = '/search?query=' + this.value;
        }
    });

    //AJAX Валидация регистрации
    $(document).on('submit', '.modal-callback-register', function (event) {
        event.preventDefault();
        $(this).find('.not-valid-message').remove();
        $(this).find('input').removeClass('not-valid');
        name = $(this).find('input[name="name"]').val();
        password = $(this).find('input[name="password"]').val();
        password_confirmation = $(this).find('input[name="password_confirmation"]').val();
        email = $(this).find('input[name="email"]').val();
        phone = $(this).find('input[name="phone"]').val();
        region = $(this).find('input[name="region"]').val();
        address = $(this).find('input[name="address"]').val();
        city = $(this).find('input[name="city"]').val();

        $.post('/register', {
            _token: $('meta[name="csrf-token"]').attr('content'),
            name: name,
            password: password,
            password_confirmation: password_confirmation,
            email: email,
            phone: phone,
            region: region,
            address: address,
            city: city,
        })
            .done(function (response) {
                location.reload();
            })
            .fail(function (fail) {
                for (key in fail.responseJSON) {
                    $('.modal-callback-register input[name="' + key + '"]').toggleClass('not-valid');
                    $('.modal-callback-register input[name="' + key + '"]').before('<p class="not-valid-message">' + fail.responseJSON[key] + '</p>')
                }
            });
    });
    //Login form callback
    $(document).on('submit', '.modal-callback-signin', function (event) {
        event.preventDefault();
        $(this).find('.not-valid-message').remove();
        $(this).find('input').removeClass('not-valid');

        $.post('/login', {
            _token: $('meta[name="csrf-token"]').attr('content'),
            email: $(this).find('input[name="email"]').val(),
            password: $(this).find('input[name="password"]').val()
        })
            .done(function (res) {
                if (res.status == 'fail') {
                    $('.modal-callback-signin > button').before('<p class="not-valid-message">' + res.messages + '</p>');
                } else {
                    $('.modal-callback-signin > button').before('<p class="not-valid-message">' + res.messages + '</p>');
                    location.reload();
                }
            })
            .fail(function (fail) {
                for (key in fail.responseJSON) {
                    $('.modal-callback-signin input[name="' + key + '"]').toggleClass('not-valid');
                    $('.modal-callback-signin input[name="' + key + '"]').before('<p class="not-valid-message">' + fail.responseJSON[key] + '</p>')
                }
            });
    });

//    header for xs sizes
    $('.nav-xs-block > p').click(function (event) {
        $(this).find('i').toggleClass('fa-sort-desc fa-sort-asc');
        $(this).closest('.nav-xs-block').find('div:first').stop().slideToggle(500);
    });

    $('.nav-xs-main-subnav-wrap > p').click(function () {
        $(this).closest('.nav-xs-main-subnav-wrap').find('div').stop().slideToggle(500);
    });

    // Show seacrh input on xs and sm
    $('.fa-search').on('click', function(){
        $('.search-sm-xs').toggleClass('close');
    });

    $('.fa-sign-out').click(function(){
        console.log('logout');
        lang = document.documentElement.lang;
        location.href = '/'+lang+'/logout';
    });

});