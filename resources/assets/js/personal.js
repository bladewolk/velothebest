$(document).ready(function () {

    $(document).on('submit', '.modal-update-user-info', function (event) {
        event.preventDefault();
        $(this).find('.not-valid-message').remove();
        $(this).find('input').removeClass('not-valid');
        user = $(this).find('input[name="user_data"]').val();

        $.post('/personal/update', {
            _token: $('meta[name="csrf-token"]').attr('content'),
            row: 'info',
            name: $(this).find('input[name="name"]').val(),
            email: $(this).find('input[name="email"]').val(),
            phone: $(this).find('input[name="phone"]').val(),
            region: $(this).find('input[name="region"]').val(),
            address: $(this).find('input[name="address"]').val(),
            city: $(this).find('input[name="city"]').val(),
        })
            .done(function (res) {
                location.reload();
            })
            .fail(function (fail) {
                for (key in fail.responseJSON) {
                    $('.modal-update-user-info input[name="' + key + '"]').toggleClass('not-valid');
                    $('.modal-update-user-info input[name="' + key + '"]').before('<p class="not-valid-message">' + fail.responseJSON[key] + '</p>')
                }
            });
    });

    $(document).on('click', '.change-pass', function (event) {
        $('.modal-update-user-info').find('.not-valid-message').remove();
        $('.modal-update-user-info').find('input').removeClass('not-valid');
        $.post('/personal/update', {
            _token: $('meta[name="csrf-token"]').attr('content'),
            password:  $('.modal-update-user-info').find('input[name="password"]').val(),
            password_confirmation:  $('.modal-update-user-info').find('input[name="password_confirmation"]').val(),
            row: 'password',
        })
            .done(function (res) {
                location.reload();
            })
            .fail(function (fail) {
                for (key in fail.responseJSON) {
                    $('.modal-update-user-info input[name="' + key + '"]').toggleClass('not-valid');
                    $('.modal-update-user-info input[name="' + key + '"]').before('<p class="not-valid-message">' + fail.responseJSON[key] + '</p>')
                }
            })
    });

});