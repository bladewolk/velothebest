$(document).ready(function(){

    //Button show more products
    $(document).on('click', '.show-all-products', function (event) {
        event.preventDefault();
        $('.items-container-load').toggleClass('load-items');
        //Generating new Link
        if (location.href.match(/\?/)){
            if (temp = location.href.match(/page=(\d)/)){
                newLink = location.href.replace(/page=\d/, 'page='+ (parseInt(temp[1]) + 1));
            } else{
                newLink = location.href + '&page=2';
            }
        } else{
            newLink = location.href + '?page=2';
        }
        window.history.pushState("", "", newLink);

        $.get(newLink)
            .done(function (res) {
                res = JSON.parse(res);
                $('.items-container-load').append(res.products);
                $('.items-bottom-load').html(res.bottom);
                $('.items-container-load').toggleClass('load-items');
            })
            .fail(function(fail){
                console.log('fail - '+fail);
            });
    })
});