@extends('site.header')
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/show_new.css') }}">
@endsection
@section('content')
    <div class="container">
        <div class="new_title">
            <p>{{ $current_page->name }}</p>
        </div>
        <div class="new_body">
            {!! $current_page->body !!}
        </div>
    </div>
@endsection
@section('scripts')
@endsection