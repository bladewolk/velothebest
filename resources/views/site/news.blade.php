@extends('site.header')
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/news.css') }}">
@endsection
@section('content')
    <div class="container content">
        @forelse($items as $item)
            <div class="col-md-12 col-sm-12 col-xs-12 item-wrapper">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12 item-picture">
                        @if ($item->images()->first())
                            <img src="{{ '/img/'.$item->images->first()->name.'?w=390&h=236&fit=crop' }}"
                                 alt="new_image">
                        @else
                            <img src="{{ asset('images/new-image.png') }}" alt="new-Picture">
                        @endif
                    </div>

                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="row new-text">
                            <p class="new-title">{{$item->name}}</p>
                            {!! str_limit($item->body, 150) !!}
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 read-full">
                            <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), 'news/'.$item->slug) }}">
                                <p>{{trans('messages.system_read_fully')}}</p>
                            </a>
                        </div>

                        <div class="com-md-6 col-sm-6 col-xs-6 date">
                            <p>{{ $item->created_at->format('d/m/Y') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <p>{{trans('messages.system_not_news')}}</p>
        @endforelse
            @include('site.pagination', ['paginator' => $items->appends(Request::except('page'))])
    </div>
@endsection
@section('scripts')
@endsection