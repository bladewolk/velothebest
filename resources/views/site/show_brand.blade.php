@extends('site.header')
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/show_new.css') }}">
@endsection
@section('content')
    <div class="container content">
        @if ($current_page->images->first())
            <div class="new_image">
                <img src="{{ '/img/'.$current_page->images->first()->name }}" alt="{{ $current_page->name }}">
            </div>
        @endif
        <div class="new_title">
            <p>{{ $current_page->name }}</p>
        </div>
        <div class="new_body">
            {!! $current_page->body !!}
        </div>
    </div>
@endsection
@section('scripts')
@endsection