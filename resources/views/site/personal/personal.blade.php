@extends('site.header')
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/personal.css') }}">
@endsection
@section('content')
    <div class="container personal">
        {{--TODO add 1 route personal baggage--}}
        {{--TODO reqeust segment--}}
        {{-- left --}}
        <div class="col-md-3 navigation col-xs-12">
            <a href="/personal"><p
                        class="{{ collect(request()->segments())->last() == 'personal' ? 'active' : '' }}">{{ trans('messages.personal_data') }}</p>
            </a>
            <a href="/personal/history"><p
                        class="{{ collect(request()->segments())->last() == 'history' ? 'active' : '' }}">{{ trans('messages.shop_list') }}</p>
            </a>
            <p id="edit-password" data-view="change_user_password">{{ trans('messages.change_password') }}</p>
            <a href="{{ url('logout') }}"><span>{{ trans('messages.logout') }}</span></a>
        </div>
        {{-- right --}}
        <div class="col-md-9 col-sm-12 col-xs-12">

            <div class="one-row row">
                <div class="col-md-4 col-sm-4 col-xs-12 row-name">
                    <div class="row">
                        <p>{{ trans('messages.fullname') }}</p>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 row-descr">
                    <div class="row">
                        <p>{{ $user->name }}</p>
                    </div>
                </div>
            </div>

            <div class="one-row row">
                <div class="col-md-4 col-sm-4 col-xs-12 row-name">
                    <div class="row">
                        <p>{{ trans('messages.email') }}</p>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 row-descr">
                    <div class="row">
                        <p>{{ $user->email }}</p>
                    </div>
                </div>
            </div>

            <div class="one-row row">
                <div class="col-md-4 col-sm-4 col-xs-12 row-name">
                    <div class="row">
                        <p>{{ trans('messages.phone') }}</p>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 row-descr">
                    <div class="row">
                        <p>{{ $user->phone }}</p>
                    </div>
                </div>
            </div>

            <div class="one-row row">
                <div class="col-md-4 col-sm-4 col-xs-12 row-name">
                    <div class="row">
                        <p>{{ trans('messages.region') }}</p>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 row-descr">
                    <div class="row">
                        <p>{{ $user->region }}</p>
                    </div>
                </div>
            </div>

            <div class="one-row row">
                <div class="col-md-4 col-sm-4 col-xs-12 row-name">
                    <div class="row">
                        <p>{{ trans('messages.city') }}</p>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 row-descr">
                    <div class="row">
                        <p>{{ $user->city }}</p>
                    </div>
                </div>
            </div>

            <div class="one-row row">
                <div class="col-md-4 col-sm-4 col-xs-12 row-name">
                    <div class="row">
                        <p>{{ trans('messages.address') }}</p>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 row-descr">
                    <div class="row">
                        <p>{{ $user->address }}</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <p class="edit-personal" data-view="change_user_info">{{trans('messages.systen_edit_info')}}</p>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/personal.min.js') }}"></script>
@endsection