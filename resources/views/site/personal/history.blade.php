@extends('site.header')
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/personal.css') }}">
@endsection
@section('content')
    <div class="container personal">
        {{--TODO add 1 route personal baggage--}}
        {{--TODO reqeust segment--}}
        {{-- left --}}
        <div class="col-md-3 navigation col-xs-12">
            <a href="/personal"><p
                        class="{{ collect(request()->segments())->last() == 'personal' ? 'active' : '' }}">{{ trans('messages.personal_data') }}</p>
            </a>
            <a href="/personal/history"><p
                        class="{{ collect(request()->segments())->last() == 'history' ? 'active' : '' }}">{{ trans('messages.shop_list') }}</p>
            </a>
            <p id="edit-password" data-view="change_user_password">{{ trans('messages.change_password') }}</p>
            <a href="{{ url('logout') }}"><span>{{ trans('messages.logout') }}</span></a>
        </div>
        {{-- right --}}
        <div class="col-md-9 col-sm-12 col-xs-12">

            @foreach($user->orders as $item)
                <div class="row order-product">
                    <div class="col-md-6 order-product-name">
                        <a href="/catalog/{{ $item->slug }}" target="_blank">
                            <p>{{ $item->name }}</p>
                        </a>
                        @if (request()->has('currency') && request()->get('currency') == 'usd')
                            <p>{{trans('messages.price')}}: <span>{{ $item->price }} $</span></p>
                        @else
                            <p>{{trans('messages.price')}}: <span>{{ number_format($item->price * $settings->currency_uah, 0, "", " ") }}
                                    грн</span></p>
                        @endif
                        <p>{{trans('messages.system_count')}}: <span>{{ $item->count }}</span></p>
                        @if (request()->has('currency') && request()->get('currency') == 'usd')
                            <p>{{trans('messages.system_summa')}}: <span>{{ $item->count * $item->price }}</span></p>
                        @else
                            <p>{{trans('messages.system_summa')}}: <span>{{ number_format($item->price * $settings->currency_uah * $item->count, 0, "", " ") }}
                                    грн</span></p>
                        @endif
                    </div>
                    <div class="col-md-6 order-product-date">
                        <p>{{ $item->created_at->format('d/m/y') }}</p>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/personal.min.js') }}"></script>
@endsection