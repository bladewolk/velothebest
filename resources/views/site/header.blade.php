<!doctype html>
<html lang="{{ App::getlocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset("images/favicon/apple-icon-57x57.png") }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset("images/favicon/apple-icon-60x60.png") }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset("images/favicon/apple-icon-72x72.png") }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset("images/favicon/apple-icon-76x76.png") }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset("images/favicon/apple-icon-114x114.png") }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset("images/favicon/apple-icon-120x120.png") }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset("images/favicon/apple-icon-144x144.png") }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset("images/favicon/apple-icon-152x152.png") }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset("images/favicon/apple-icon-180x180.png") }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset("images/favicon/android-icon-192x192.png") }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset("images/favicon/favicon-32x32.png") }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset("images/favicon/favicon-96x96.png") }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset("images/favicon/favicon-16x16.png") }}">
    <link rel="manifest" href="{{ asset("images/favicon/manifest.json") }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('images/favicon/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    @if (preg_match('/catalog/', request()->path()) && isset($items) && $items->total())
        @if($items->nextPageUrl())
            <link rel="next" href="{{$items->nextPageUrl()}}">
        @endif
        @if($items->previousPageUrl())
            <link rel="prev" href="{{$items->previousPageUrl()}}">
        @endif
    @endif 
    <meta name="description"
          content="{{ isset($current_page->meta_description) ? $current_page->meta_description : ''}}"/>
    <meta name="keywords" content="{{ isset($current_page->meta_keywords) ? $current_page->meta_keywords : ''}}"/>
    <meta name="title" content="{{ isset($current_page->meta_title) ? $current_page->meta_title : ''}}" />

    <title>
        @if (isset($current_page->meta_title))
            {{ $current_page->meta_title }}
        @else
            {{ isset($settings->site_name) ? $settings->site_name : "VELOTHEBEST"}}
        @endif
    </title>
    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('css/bootstrap-grid.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('css/header.css') }}">
    @yield('styles')
    @stack('styles-stack')
</head>
<body>
<div class="search-sm-xs close">
    <input class="search" type="text" name="search" placeholder="{{trans('messages.enter_search')}}">
</div>
<div class="show-burger">
    <span class="visible-xs visible-sm fa fa-times close-burger"></span>
    <nav>
        @foreach($topLinks as $link)
            <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), $link->slug) }}">
                <p>{{ $link->localization->first()->name }}</p>
            </a>
        @endforeach
    </nav>

    <div class="burger-phones-wrapper visible-xs">
        <i class="fa fa-phone" aria-hidden="true"></i>

        <div class="burger-phones">
            @foreach($settings->phones as $phone)
                <a href="tel::{{ $phone['tel'] }}">
                    <p>{!! $phone['phone'] !!}</p>
                </a>
            @endforeach
            <button class="callback-phone" data-view="callback">{{ trans('messages.answer_call') }}</button>
        </div>
    </div>

    <select class="visible-xs-inline localization">
        @foreach(LaravelLocalization::getSupportedLocales() as $locale => $descr)
            <option @if (LaravelLocalization::getCurrentLocale() == $locale) selected @endif
            data-link="{{ LaravelLocalization::getLocalizedURL($locale,Request::path() . '?' . http_build_query(Request::all())) }}"
            >
                {{ strtoupper($locale) }}
            </option>
        @endforeach
    </select>
    <select class="currency visible-xs-inline">
        <option data-value="uah">{{ trans('messages.uah') }}</option>
        <option data-value="usd"
                @if (Request::get('currency') == 'usd') selected @endif>{{ trans('messages.usd') }}</option>
    </select>
</div>

<header>
    <div class="container">
        <div class="row">
            <div class="col-sm-1 visible-sm burger visible-xs col-xs-1">
                <i class="fa fa-align-justify" aria-hidden="true"></i>
            </div>
            <div class="col-md-2 header-logotype col-sm-1 col-xs-3">
                <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), '/') }}">
                    @if ($settings->images()->first() != null)
                        <img src="{{ '/img/' . $settings->images->first()->name . '?w=180&h=110&fit=crop' }}" alt="Header-logotype">
                    @else
                        <img src="{{ asset('images/logo.png') }}" alt="logotype">
                    @endif
                </a>
            </div>

            <div class="col-md-2 header-phones-wrapper col-sm-4 hidden-xs">
                <i class="fa fa-phone" aria-hidden="true"></i>
                <p class="visible-sm-inline">{!! $settings->phones[0]['phone'] !!}</p>
                <i class="fa fa-angle-down visible-sm-inline show-phones" aria-hidden="true"></i>
                <div class="header-phones hidden-sm">
                    @foreach($settings->phones as $phone)
                        <a href="tel::{{ $phone['tel'] }}">
                            <p>{!! $phone['phone'] !!}</p>
                        </a>
                    @endforeach
                    <button class="callback-phone" data-view="callback">{{trans('messages.answer_call')}}</button>
                </div>
            </div>
            <div class="col-md-6 header-center col-sm-4 col-xs-3">
                <nav class="hidden-sm hidden-xs">
                    @foreach($topLinks as $link)
                        <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), $link->slug) }}">
                            <p>{{ $link->localization->first()->name }}</p>
                        </a>
                        <span>&bull;</span>
                    @endforeach
                </nav>
                <div class="header-middle-center col-md-12">
                    @if(Auth::guest())
                        <div class="col-md-8 header-account col-sm-4 col-xs-12">
                            <i class="fa fa-sign-in sign-in" aria-hidden="true" data-view="signin"></i>
                            <i class="fa fa-search visible-sm-inline visible-xs-inline" aria-hidden="true"></i>
                            <button class="hidden-sm hidden-xs sign-in"
                                    data-view="signin">{{ trans('messages.login') }}</button>
                            <span class="hidden-sm hidden-xs">/</span>
                            <button class="hidden-sm hidden-xs register"
                                    data-view="register">{{ trans('messages.register') }}</button>
                        </div>
                    @else
                        <div class="col-md-8 header-account col-sm-4 col-xs-12">
                            <p class="hidden-sm hidden-xs">{{ Auth::user()->name }}</p>
                            <i class="fa fa-sign-out " aria-hidden="true" data-view="signin"></i>
                            <i class="fa fa-search visible-sm-inline visible-xs-inline" aria-hidden="true"></i>
                            <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), 'logout') }}">
                                <span class="hidden-sm hidden-xs">{{ trans('messages.logout') }}</span>
                            </a>
                            <span class="hidden-sm hidden-xs">/</span>
                            <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), 'personal') }}"
                               class="hidden-sm hidden-xs">
                                <span>{{ trans('messages.personal') }}</span>
                            </a>
                        </div>
                    @endif
                    <div class="col-md-4 col-sm-8 header-lang-value hidden-xs">
                        <select class="localization">
                            @foreach(LaravelLocalization::getSupportedLocales() as $locale => $descr)
                                <option @if (LaravelLocalization::getCurrentLocale() == $locale) selected @endif
                                data-link="{{ LaravelLocalization::getLocalizedURL($locale,Request::path() . '?' . http_build_query(Request::all())) }}"
                                >
                                    {{ strtoupper($locale) }}
                                </option>
                            @endforeach
                        </select>
                        <select class="currency">
                            <option data-value="uah">{{ trans('messages.uah') }}</option>
                            <option data-value="usd"
                                    @if (Request::get('currency') == 'usd') selected @endif>{{ trans('messages.usd') }}</option>
                        </select>
                    </div>
                    <div class="col-md-12 header-search hidden-sm hidden-xs">
                        <input type="text" class="search" placeholder="{{ trans('messages.enter_search') }}">
                    </div>
                </div>
            </div>
            <div class="col-md-2 cart col-sm-2 col-xs-4">
                <div class="cart-wrapper">
                    <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), 'cart') }}">
                        <img src="{{ asset('images/cart.svg') }}" alt="cart SVG image">
                        @if (Session::has('cart'))
                            <p class="visible-md visible-lg">
                                {{ trans('messages.in_cart') }}
                                <span>{{ count(Session::get('cart')) }}</span>
                                {{ Lang::choice('messages.products', count(Session::get('cart')), [], App::getLocale()) }}
                                {{trans('messages.na_summu')}}
                                @if (Request::has('currency') && Request::get('currency') == 'usd')
                                    <span>{{ number_format(Session::get('totalPrice')) }}</span>$
                                @else
                                    <span>{{ number_format(Session::get('totalPrice') * $settings->currency_uah, 0, "", " ") }}</span>
                                    грн
                                @endif
                            </p>
                            <p class="visible-sm-inline">
                                <span>{{count(Session::get('cart'))}}</span> {{ Lang::choice('messages.products', count(Session::get('cart')), [], App::getLocale()) }}
                            </p>
                            <p class="visible-xs-inline"><span>{{count(Session::get('cart'))}}</span></p>
                        @else
                            <p class="hidden-sm hidden-xs"><span>{{ trans('messages.empty_cart') }}</span></p>
                        @endif
                        <span class="header-confirm-order hidden-sm hidden-xs">{{ trans('messages.make_order') }}</span>
                    </a>
                </div>
            </div>
            <div class="col-md-12 header-bottom col-sm-12 col-xs-12">
                <div class="row">
                    <div class="header-info col-md-10 col-sm-10 col-xs-12">
                        <p>{{ $settings->info }}</p>
                    </div>
                    <div class="header-socs col-md-2 col-sm-2 hidden-xs">
                        <a href="//{{ $settings->facebook }}" target="_blank" rel="nofollow"><i class="fa fa-facebook"
                                                                                                aria-hidden="true"></i></a>
                        <a href="//{{ $settings->instagram }}" target="_blank" rel="nofollow"><i class="fa fa-instagram"
                                                                                                 aria-hidden="true"></i></a>
                        <a href="//{{ $settings->youtube }}" target="_blank" rel="nofollow"><i
                                    class="fa fa-youtube-play"
                                    aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="header-main-nav">
        <div class="container hidden-xs">
            <div class="row">
                <ul>
                    <!-- Catalog HOVER -->
                    @foreach($mainLinks as $link)
                        <li style="{{ strlen($link->slug) > 12 ? '-webkit-flex: 4;-ms-flex: 4;flex: 4' : '-webkit-flex: 3;-ms-flex: 3;flex: 3' }};">
                            <a href="@if($link->slug == 'catalog' || $link->slug == 'brands') javascript:void(0) @else{{ LaravelLocalization::getLocalizedURL(App::getLocale(), $link->slug) }} @endif">
                                <p>{{ $link->name }}
                                    @if($link->slug == 'katalog' || $link->slug == 'brendy')
                                        <span class="fa fa-caret-down visible-sm visible-xs"></span>
                                    @endif
                                </p>
                            </a>
                            @if($link->slug == 'catalog')
                                <div class="show-sub">
                                    <div class="col-md-3 col-sm-3 sub-list">
                                        @foreach($categories as $category)
                                            <div class="sub-name">
                                                <p>
                                                    <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), 'catalog/'.$category->slug) }}">
                                                        {{ $category->name }}
                                                    </a><span
                                                            class="visible-xs-inline fa fa-caret-down"></span>
                                                </p>

                                                <div class="sub-descr">
                                                    @foreach($category->childrens as $subcategory)
                                                        <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), 'catalog/'.$subcategory->slug) }}">
                                                            <div class="col-md-6 col-sm-6 col-xs-12 sub-descr-item">
                                                                @if ($subcategory->images->first())
                                                                    <img src="{{ '/img/'.$subcategory->images->first()->name.'?w=80&h=80&fit=crop' }}"
                                                                         alt="{{ $subcategory->name }}">
                                                                @else
                                                                    <img src="{{ '/img/static/no-image.jpg?w=80&h=80&fit=crop' }}"
                                                                         alt="image">
                                                                @endif
                                                                <p>{{ $subcategory->name }}</p>
                                                            </div>
                                                        </a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                            @if ($link->slug == 'brands')
                                <div class="show-brands">
                                    @foreach($brands as $brand)
                                        <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(),'brand/'.$brand->slug) }}">
                                            <div class="col-md-2 col-sm-3 col-xs-6 image-wrapper">
                                                @if ($brand->images->first())
                                                    <img src="{{ '/img/'.$brand->images->first()->name.'?w=180&h=55' }}"
                                                         alt="{{ $brand->name }}">
                                                @else
                                                    <img src="{{ '/img/static/no-image.jpg?h=55&fit=crop' }}"
                                                         alt="{{ $brand->name }}">
                                                @endif
                                            </div>
                                        </a>
                                    @endforeach
                                </div>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="visible-xs">
            <div class="nav-xs-main-nav">
                @foreach($mainLinks as $link)
                    @if ($link['slug'] == 'catalog')
                        <div class="nav-xs-block">
                            <p>{{ $link->name }} <i class="fa fa-sort-desc"></i></p>
                            <div class="nav-xs-main-subnav">
                                @foreach($categories as $item)
                                    <div class="nav-xs-main-subnav-wrap">
                                        <p>
                                            {{ $item->name }}
                                            @if($item->childrens->count())
                                                <i class="fa fa-sort-desc"></i>
                                            @endif
                                        </p>
                                        <div class="nav-xs-main-subnav-wrap-child">
                                            @foreach($item->childrens as $child)
                                                <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(),'/catalog/'.$child->slug) }}">
                                                    <p>{{ $child->name }}</p>
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @elseif($link['slug'] == 'brands')
                        <div class="nav-xs-block">
                            <p>{{ $link->name }} <i class="fa fa-sort-desc"></i></p>
                            <div class="nav-xs-main-subnav">
                                @foreach($brands as $brand)
                                    <a href="#" class="nav-xs-main-subnav-brand">
                                        @if ($brand->images->first())
                                            <img src="{{ '/img/'.$brand->images->first()->name.'?w=55&h=55' }}"
                                                 alt="{{ $brand->name }}">
                                        @else
                                            <img src="{{ '/img/static/no-image.jpg?w=55&h=55&fit=crop' }}"
                                                 alt="{{ $brand->name }}">
                                        @endif
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    @else
                        <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), $link['slug']) }}">
                            <p>{{ $link->name }}</p>
                        </a>
                    @endif
                @endforeach
            </div>
        </div>
    </nav>

</header>
{{-- BreadCrumbs --}}
@if (isset($breadcrumbs))
    @include('site.breadcrumbs')
@endif


@yield('content')


{{-- CONTENT --}}


{{-- END CONTENT --}}

{{-- FOOTER --}}
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-3 col-xs-6 footer-logo">
                <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), '/') }}">
                    @if ($settings->images()->first() != null)
                        <img src="{{ '/img/'.$settings->images()->first()->name }}" alt="Header-logotype">
                    @else
                        <img src="{{ asset('images/logo.png') }}" alt="logotype">
                    @endif
                </a>
                <p>2015 (c)velothebest.com</p>
                <a rel="nofollow" href="//https://dvacom.net/">
                    <img class="dvacom-logo" src="{{ asset('images/dvacom.png') }}" alt="DvaCom-logo">
                </a>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6 footer-menu">
                <ul>
                    @foreach($mainLinks as $link)
                        <li>
                            @if ($link->slug == 'catalog')
                                <a href="#">
                                    <p>{{$link->name}}</p>
                                </a>
                            @else
                                 <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), $link->slug) }}">
                                    <p>{{$link->name}}</p>
                                </a>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 additional-menu">
                <ul>
                    @foreach($topLinks as $link)
                        <li>
                            <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), $link->slug) }}">
                                <p>{{$link->localization->first()->name}}</p>
                            </a>
                        </li>
                    @endforeach
                </ul>
                <div class="socs">
                    <a href="//{{ $settings->facebook }}" target="_blank" rel="nofollow"><i class="fa fa-facebook"
                                                                                            aria-hidden="true"></i></a>
                    <a href="//{{ $settings->instagram }}" target="_blank" rel="nofollow"><i class="fa fa-instagram"
                                                                                             aria-hidden="true"></i></a>
                    <a href="//{{ $settings->youtube }}" target="_blank" rel="nofollow"><i class="fa fa-youtube-play"
                                                                                           aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="col-md-5 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-7 col-sm-5 col-xs-12 footer-phones">
                        <div class="footer-phones-wrapper">
                            <p><img src="{{ asset('images/life.png') }}" alt="">{!! $settings->phones[0]['phone'] !!}
                            </p>
                            <p><img src="{{ asset('images/kiev.png') }}" alt="">{!! $settings->phones[1]['phone'] !!}
                            </p>
                            <p><img src="{{ asset('images/mts.png') }}" alt="">{!! $settings->phones[2]['phone'] !!}</p>

                            <div class="feedback-button callback-phone" data-view="callback">
                                <p><span class="fa fa-phone"></span>&nbsp;{{ trans('messages.answer_call') }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-3 col-xs-12">
                        <div class="footer-cart">
                            <img src="{{ asset('images/footer-cart.svg') }}" alt="caret">
                            @if(Session::has('cart'))
                                @if(Request::has('currency') && Request::get('currency') == 'usd')
                                    <p>
                                        {{trans('messages.in_cart')}}
                                        <span>{{count(Session::get('cart'))}}</span>
                                        {{ Lang::choice('messages.products', count(Session::get('cart')), [], App::getLocale()) }}
                                        {{trans('messages.na_summu')}}
                                        <span>{{ number_format(Session::get('totalPrice'), 0, "", " ") }}</span> $</p>
                                @else
                                    <p>{{trans('messages.in_cart')}}
                                        <span>{{count(Session::get('cart'))}}</span>
                                        {{ Lang::choice('messages.products', count(Session::get('cart')), [], App::getLocale()) }}
                                        {{trans('messages.na_summu')}}
                                        <span>{{ number_format(Session::get('totalPrice') * $settings->currency_uah, 0, "", " ") }}</span>
                                        грн</p>
                                @endif
                            @else
                                <p>{{trans('messages.header_cart')}} <span>{{trans('messages.header_cart_empty')}}</span></p>
                            @endif
                            <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), 'cart') }}">
                                <span>{{ trans('messages.make_order') }}</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-4 col-xs-12 footer-info">
                        <p>{{$settings->info}}</p>
                    </div>
                    <div class="col-md-5 col-sm-4 col-xs-12 footer-info-add">
                        <p>{{trans('messages.can_payed')}}</p>
                        <img src="{{ asset("images/visa.png") }}" alt="Visa">
                    </div>

                </div>
            </div>
        </div>
    </div>
</footer>

{{-- END FOOTER --}}

{{--loads modal forms--}}
<div class="modal-wrapper">
    {{--TODO REMOVE THIS--}}

    {{--TODO REMOVE THIS--}}
</div>

@yield('styles-footer')
@yield('scripts')
<script src="{{ asset('js/input_mask.min.js') }}"></script>
<script src="{{ asset('js/layout.min.js') }}"></script>
@stack('script-stack')
</body>