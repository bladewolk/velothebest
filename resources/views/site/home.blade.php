@extends('site.header')
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
@endsection
@section('content')
    {{--SLIDER--}}
    @if (count($slides) > 0)
        <div class="slider-wrapper">
            <div class="slick-slider">
                @foreach($slides as $slide)
                    <div class="slide-wrapp" style="background: url({{'/img/'. $slide->image_background . '?w=1200&h=500&q=75' }});
                    background-size: cover; background-size: cover; background-position: center;">
                        @if($slide->image)
                            <div class="container">
                                <div class="row">
                                    <img src="{{ '/img/'.$slide->image .'?w=1170&h=500&fit=crop&q=75' }}" alt="Slider-Image">
                                </div>
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
            <span class="slick-next fa fa-angle-right visible-lg"></span>
            <span class="slick-prev fa fa-angle-left visible-lg"></span>
            <div class="slick-dotss"></div>
        </div>
    @endif
    {{--END SLIDER--}}
    {{--START BANNER--}}
    @if (count($banner))
        <div class="banner-wrapper">
            <div class="container">
                <div class="row">
                    <a rel="nofollow" href="@if (isset($banner[1]) && $banner[1]['url']) //{{$banner[1]['url']}} @else # @endif">
                        <div class="col-md-4 col-sm-4 col-sm-12 large"
                             @if (isset($banner[1]) && $banner[1]['image'])
                             style="background: url('/uploads/blocks/{{ $banner[1]['image'] }}');
                                     background-size: cover;
                                     background-position: center;"
                             @else
                                style="background: url('/img/static/no-image.jpg?w=255&h=255');
                                background-position: center;
                                background-color: white;
                                background-repeat: no-repeat;"
                             @endif >
                        </div>
                    </a>
                    <div class="col-md-8 col-sm-8 col-sm-12">
                        <div class="row">
                            <div class="col-md-8 col-sm-8 col-sm-12">
                                <div class="row">
                                    @for($i = 2; $i < 6; $i++)
                                        <a rel="nofollow" href="@if (isset($banner[$i]) && $banner[$i]['url']) //{{$banner[$i]['url']}} @else # @endif">
                                            <div class="col-md-6 col-sm-6 col-sm-12 small"
                                                @if (isset($banner[$i]))

                                                 style="background: url('/uploads/blocks/{{ $banner[$i]['image'] }}'); background-size: cover;
                                                         background-position: center;"
                                                @else
                                                 style="background: url('/img/static/no-image.jpg?w=255&h=255');
                                                    background-position: center;
                                                    background-color: white;
                                                    background-repeat: no-repeat;"
                                                @endif >
                                            </div>
                                        </a>
                                    @endfor
                                </div>
                            </div>
                            <a href="@if (isset($banner[6]) && $banner[6]['url']) //{{$banner[6]['url']}} @else # @endif">
                                <div class="col-md-4 col-sm-4 col-sm-12 large"
                                    @if (isset($banner[6]))
                                    style="background: url('/uploads/blocks/{{ $banner[6]['image'] }}');
                                             background-size: cover; background-position: center;"
                                    @else
                                    style="background: url('/img/static/no-image.jpg?w=255&h=255');
                                    background-position: center;
                                    background-color: white;
                                    background-repeat: no-repeat;"
                                    @endif >
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    {{--END BANNER--}}
    {{--LATEST NEW ITEMS--}}
    <div class="new-items-wrapper">
        <div class="container">
            <div class="row">
                <p class="page-title">
                    {{ trans('messages.new_items')}}
                </p>
                {{--ITEMS--}}
                <div class="container items-slider">
                    @foreach($products as $chunk)
                        <div class="item-slide">
                            @foreach($chunk as $item)
                                <div class="col-md-3 col-sm-6">
                                    <div class="row">
                                        <div class="item-wrapper">
                                            @if ($item->label)
                                                <span class="item-discount" style="background-color: {{$item->label->color}}; min-width: 70%; max-width: 80%">
                                                    {{ $item->label->localization->where('locale', App::getLocale())->first()->description }}
                                                    <b>{{ $item->localization->where('locale', App::getLocale())->first()->label_text }}</b>
                                                </span>
                                            @endif

                                            @if ($item->images->isEmpty() == false)
                                                <div class="item-image" style="">
                                                    <img src="{{ '/img/'.$item->images->first()->name .'?w=261&h=220&q=75' }}"
                                                         alt="{{ $item->localization->first()->name }}"
                                                         title="{{ $item->localization->first()->name }}">
                                                </div>
                                            @else
                                                <div class="item-image">
                                                    <img src="{{ '/img/static/no-image.jpg?w=261&h=220&q=75' }}"
                                                         alt="{{ $item->localization->first()->name }}"
                                                         title="{{ $item->localization->first()->name }}">
                                                </div>
                                            @endif

                                            <div class="item-name">
                                                <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), '/catalog/'. $item->slug) }}">
                                                    <p title="{{ $item->localization->first()->name }}">{{ str_limit($item->localization->first()->name, 110, '...') }}</p>
                                                </a>
                                            </div>

                                            <div class="item-code">
                                                <p>Код: <span>{{ $item->code }}</span></p>
                                            </div>
                                            <div class="item-custom-order">
                                                @if ($item->status == 2)
                                                    <p><i class="fa fa-truck" aria-hidden="true"></i>
                                                        {{ trans('messages.item_by_order') }}</p>
                                                @elseif ($item->status == 0 || $item->in_stock < 1)
                                                    <p style="color: #7e7e7e;"><i class="fa fa-ban"
                                                                                  aria-hidden="true"></i>
                                                        {{ trans('messages.item_not_in_stock') }}</p>
                                                @endif
                                            </div>

                                            <div class="price">
                                                <p>{{trans('messages.price')}}</p>
                                                @if (Request::has('currency') && Request::get('currency') == 'usd')
                                                    @if($item->price_discount != 0)
                                                        <p class="old-price">{{ number_format($item->price) }}
                                                            $</p>
                                                        <p class="current-price">
                                                            <span>{{ number_format($item->price_discount) }}</span>
                                                            $</p>
                                                    @else
                                                        <p class="current-price">
                                                            <span>{{ number_format($item->price) }}</span>
                                                            $
                                                        </p>
                                                    @endif
                                                @else
                                                    @if($item->price_discount != 0)
                                                        <p class="old-price">{{ number_format($item->price * $settings->currency_uah, 0, "", " ") }}
                                                            грн</p>
                                                        <p class="current-price">
                                                            <span>{{ number_format($item->price_discount * $settings->currency_uah, 0, "", " ") }}</span>
                                                            грн</p>
                                                    @else
                                                        <p class="current-price">
                                                            <span>{{ number_format($item->price * $settings->currency_uah, 0, "", " ") }}</span>
                                                            грн
                                                        </p>
                                                    @endif
                                                @endif
                                            </div>

                                            <div class="by-now">
                                                @if (Session::has('cart') && in_array($item->id, Session::get('cart')))
                                                    <button class="disabled">{{ trans('messages.in_cart') }}</button>
                                                @elseif ($item->status == 0 || $item->in_stock < 1)
                                                    <button class="disabled"
                                                            data-productID="{{ $item->id }}">{{ trans('messages.item_not_in_stock') }}</button>
                                                @elseif ($item->status == 2)
                                                    <button class="buy-this-item"
                                                            data-productID="{{ $item->id }}">{{ trans('messages.to_order') }}</button>
                                                @else
                                                    <button class="buy-this-item"
                                                            data-productID="{{ $item->id }}">{{ trans('messages.button_buy') }}</button>
                                                @endif
                                            </div>


                                            @if ($item->rating)
                                                <div class="item-feedback">
                                                    <div class="item-stars">
                                                        @for($i = 1; $i < 6; ++$i)
                                                            @if ($item->rating >= $i)
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                            @else
                                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                                            @endif
                                                        @endfor
                                                    </div>
                                                    <div class="item-comments">
                                                        <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), '/catalog/'. $item->slug) }}"
                                                           rel="nofollow">
                                                            <p>{{ $item->comments->count() }} {{ trans_choice('messages.product_have_comments', $item->comments->count()) }}</p>
                                                        </a>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
                <div class="container items-slider-nav visible-md visible-lg">
                    <span class="items-slick-prev fa fa-angle-left"></span>
                    <span class="items-slick-next fa fa-angle-right"></span>
                </div>
            </div>
            {{--END ITEMS--}}
        </div>
    </div>
    {{--END LATEST NEW--}}
    {{--PAGE TEXT BODY HTML--}}
    <div class="page-body-wrapper">
        <div class="container">
            {!! $current_page->body !!}
        </div>
    </div>
    {{--END PAGE TEXT BODY HTML--}}

    @if(count($news))
        {{--NEWS--}}
        <div class="news-wrapper">
            <div class="container news">
                <div class="row">
                    <p class="page-title">
                        {{trans('messages.news')}}
                    </p>
                </div>
                @foreach($news as $new)
                    <div class="col-md-3 col-sm-6">
                        <div class="new-item">
                            <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), 'news/'.$new->slug) }}">
                                <div class="new-image">
                                    @if (count($new->images))
                                        <img src="{{ '/img/'.$new->images->first()->name .'?w=260&h=110&fit=crop&q=75' }}"
                                             alt="newPicture">
                                    @else
                                        <img src="{{ asset("images/news.png") }}" alt="newPicture">
                                    @endif
                                </div>
                                <div class="new-name">
                                    <p>{{ $new->name }}</p>
                                </div>
                                <div class="new-date">
                                    <p>{{ $new->created_at->format('m/d/y') }}</p>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
                @if (count($news) == 4)
                    <div class="col-md-12">
                        <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), 'news') }}">
                            <p class="show-all-new">
                                {{ trans('messages.show_all_new') }}
                            </p>
                        </a>
                    </div>
                @endif

            </div>
        </div>
        {{--END NEWS--}}
    @endif

@endsection
@section('styles-footer')
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
@endsection
@section('scripts')
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <script>
        $(function () {
            $('.slick-slider').on('init', function (event, slick, direction) {
                // check to see if there are one or less slides
                if ($('.slick-slide').length < 2) {
                    // remove arrows
                    $('.slick-next').remove();
                    $('.slick-prev').remove();
                }
            });

            $('.slick-slider').slick({
                autoplay: true,
                autoplaySpeed: {{ $settings->slider_speed }},
                arrows: true,
                fade: true,
                dots: true,
                speed: 1000,
                nextArrow: $('.slick-next'),
                prevArrow: $('.slick-prev'),
                appendDots: $('.slick-dotss'),
            });

            $('.items-slider').slick({
                autoplay: true,
                autoplaySpeed: 3000,
                speed: 1000,
                arrows: true,
                adaptiveHeight: true,
                nextArrow: $('.items-slick-next'),
                prevArrow: $('.items-slick-prev'),
            });
        });
    </script>
@endsection