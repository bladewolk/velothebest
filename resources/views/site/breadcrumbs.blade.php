<div class="breadcrumbs ">
	<div class="container">
		@foreach($breadcrumbs as $name => $url)
			@if($name == 'home')
				<a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), '/') }}" class="fa fa-home">
				</a>
			@else
				<a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), $url) }}" class="fa">
					<span>{{ $name }}</span>
				</a>
			@endif
		@endforeach
	</div>
</div>