<form action="#" class="modal-callback-phone">
    <span class="fa fa-times close-form"></span>
    <p>{{trans('messages.system_order_callback')}}</p>
    <input type="tel" class="phone-modal" placeholder="+38(___)___ __ __" name="phone" required autocomplete="off">
    <button class="sendfeedback">{{trans('messages.to_order')}}</button>
</form>
