<div class="modal-in-cart col-md-6 col-sm-8 col-xs-12">
    <div class="row top">
        <p>{{trans('messages.system_product_add_to_cart')}}</p>
        <span class="fa fa-times close-form hidden-xs"></span>
    </div>
    @foreach($items as $item)
        <div class="row item-in-wrapper">
            <div class="col-md-3 col-sm-3">
                <div class="row item-img">
                    @if ($item->images->first())
                        <img src="{{ '/img/'.$item->images->first()->name.'?w=230&h=230' }}" alt="{{ $item->localization->first()->name }}">
                    @else
                        <img src="{{ '/img/static/no-image.jpg?w=230&h=230?fit=crop' }}" alt="{{ $item->localization->first()->name }}">
                    @endif
                </div>
            </div>
            <div class="col-md-9 col-sm-9">
                <div class="row name">
                    <p>{{ $item->localization->first()->name }}</p>
                </div>
                <div class="row price">
                    @if (Request::has('currency') && Request::get('currency') == 'usd')
                        <p>{{ number_format($item->price, 0, '', ' ') }} <span>$</span></p>
                    @else
                        <p>{{ number_format($item->price * $settings->currency_uah, 0, "", " ") }} <span>грн</span></p>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
    <div class="row bottom">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <p class="close-form">{{trans('messages.system_continue_shopping')}}</p>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), '/cart') }}">
                <span>{{trans('messages.system_go_to_order')}}</span>
            </a>
        </div>
    </div>
</div>