<form action="#" class="modal-product-answer">
    <span class="fa fa-times close-form"></span>
    <p>{{trans('messages.system_make_your_question')}}</p>
    <input type="email" placeholder="E-Mail" required name="email" autocomplete="off" value="test@mail">
    <textarea name="answer" id="" placeholder="{{trans('messages.enter_question')}}" required name="answer">Тестовый вопрос к продукту</textarea>
    <button class="sendAnswer">{{trans('messages.system_make_question')}}</button>
</form>
