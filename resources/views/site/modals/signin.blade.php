<form action="{{ url('login') }}" class="modal-callback-signin" method="POST">
 	{{ csrf_field() }}
    <span class="fa fa-times close-form"></span>
    <p>{{trans('messages.system_enter')}}</p>
    <input type="email" name="email" placeholder="{{trans('messages.enter_your_email')}}" autocomplete="off" name="email"  autocomplete="off">
    <input type="password" placeholder="{{trans('messages.enter_password')}}" autocomplete="off" name="password"  autocomplete="off">
    <button>{{trans('messages.system_enter_go')}}</button>
</form>