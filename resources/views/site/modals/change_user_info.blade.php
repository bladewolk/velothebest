<form action="#" class="modal-update-user-info col-md-3 col-sm-4 col-xs-11" method="POST" >
    {{ csrf_field() }}
    {{ Form::hidden('user_data', $user->id) }}
    <span class="fa fa-times close-form" data-form="register"></span>
    <p>{{ trans('messages.user_info_edit') }}</p>
    <input type="text" placeholder="{{trans('messages.enter_your_name')}}" name="name" value="{{ $user->name }}" autocomplete="off" max="100">
    <input type="email" placeholder="{{trans('messages.enter_your_email')}} почта" name="email" value="{{ $user->email }}" autocomplete="off" max="100">
    <input type="text" class="phone-modal" placeholder="+38(___)___ __ __" name="phone" value="{{ $user->phone }}" autocomplete="off" >
    <input type="text" placeholder="{{trans('messages.enter_yout_region')}}" name="region" value="{{ $user->region }}" autocomplete="off" max="100">
    <input type="text" placeholder="{{trans('messages.enter_yout_city')}}" name="city" value="{{ $user->city }}" autocomplete="off" max="100">
    <input type="text" placeholder="{{trans('messages.address_delivery')}}" name="address" value="{{ $user->address }}" autocomplete="off" max="100">
    <button>{{trans('messages.save')}}</button>
</form>
