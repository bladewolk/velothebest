<div class="modal-update-user-info col-md-3 col-sm-4 col-xs-11">
    {{ csrf_field() }}
    <span class="fa fa-times close-form" data-form="register"></span>
    <p>{{ trans('messages.user_password_edit') }}</p>
    {{ Form::password('password', ['autocomplete' => 'off', 'placeholder' => trans('messages.enter_password')]) }}
    {{ Form::password('password_confirmation', ['autocomplete' => 'off', 'placeholder' => trans('messages.confirm_password')]) }}
    <button class="change-pass">{{trans('messages.save')}}</button>
</div>
