<form action="/register" class="modal-callback-register col-md-3 col-sm-4 col-xs-11" method="POST" >
    {{ csrf_field() }}
    <span class="fa fa-times close-form" data-form="register"></span>
    <p>{{trans('messages.system_register')}}</p>
    <input type="text" placeholder="{{trans('messages.enter_your_name')}}" name="name" value="John Due" autocomplete="off" max="100">
    <input type="password" placeholder="{{trans('messages.enter_password')}}" name="password" value="testuser" autocomplete="off" max="100">
    <input type="password" placeholder="{{trans('messages.confirm_password')}}" name="password_confirmation" value="testuser" autocomplete="off" max="100">
    <input type="email" placeholder="{{trans('messages.enter_your_email')}}" name="email" value="testemail@gmail.com" autocomplete="off" max="100">
    <input type="text" class="phone-modal" placeholder="+38(___)___ __ __" name="phone" value="0507482196" autocomplete="off" >
    <input type="text" placeholder="{{trans('messages.enter_yout_region')}}" name="region" value="Kirovohrad" autocomplete="off" max="100">
    <input type="text" placeholder="{{trans('messages.enter_yout_city')}}" name="city" value="Kirovohrad" autocomplete="off" max="100">
    <input type="text" placeholder="{{trans('messages.address_delivery')}}" name="address" value="Kropivnitskiy" autocomplete="off" max="100">
    <button>{{trans('messages.register')}}</button>
</form>
