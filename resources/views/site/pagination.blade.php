<?php
// config
$link_limit = 7; // maximum number of links (a little bit inaccurate, but will be ok for now)
$counter = 0;
?>

@if ($paginator->lastPage() > 1)
    <div class="pagination">
        @if ($paginator->currentPage() == 2)
            <a href="{{ preg_replace('/&page=1/', '', $paginator->previousPageUrl()) }}"
               class="angle ajax-link">
            <span>
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </span>
            </a>
        @else
            <a href="{{ $paginator->previousPageUrl() ? $paginator->previousPageUrl() : 'javascript:void(0)' }}"
               class="angle {{ $paginator->previousPageUrl() ? 'ajax-link' : 'disabled' }}">
            <span>
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </span>
            </a>
        @endif

        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <?php
            $half_total_links = floor($link_limit / 2);
            $from = $paginator->currentPage() - $half_total_links;
            $to = $paginator->currentPage() + $half_total_links;
            if ($paginator->currentPage() < $half_total_links) {
                $to += $half_total_links - $paginator->currentPage();
            }
            if ($paginator->lastPage() - $paginator->currentPage() < $half_total_links) {
                $from -= $half_total_links - ($paginator->lastPage() - $paginator->currentPage()) - 1;
            }
            ?>
            @if ($from < $i && $i < $to)
                @if ($paginator->currentPage() == $i)
                    <a href="javascript:void(0)"
                       class="active" rel="nofollow">
                        <span>{{ $i }}</span>
                    </a>
                @elseif ($i == 1)
                    <a href="{{ preg_replace('/&page=\d/', '', $paginator->previousPageUrl()) }}"
                       class="ajax-link">
                        <span>{{ $i }}</span>
                    </a>
                @else
                    <a href="{{ $paginator->url($i) }}" class="ajax-link">
                        <span>{{ $i }}</span>
                    </a>
                @endif

                {{--@if ($paginator->currentPage() == $i)--}}
                {{--<a href="javascript:void(0)"--}}
                {{--class="active">--}}
                {{--<span>{{ $i }}</span>--}}
                {{--</a>--}}
                {{--@elseif ($i == 1)--}}
                {{--<a href="{{ preg_replace('/&page=1/', '', $paginator->previousPageUrl()) }}"--}}
                {{--class="ajax-link">--}}
                {{--<span>--}}
                {{--{{ $i }}--}}
                {{--</span>--}}
                {{--</a>--}}
                {{--@else--}}
                {{--<a href="{{ ($to != 1) ? $paginator->url($i) : '/'.Request::path() }}" class="ajax-link">--}}
                {{--<span>{{ $i }}</span>--}}
                {{--</a>--}}
                {{--@endif--}}

                @if ($counter == 2 && $paginator->lastPage() > 7)
                    <span class="dots">...</span>
                @endif
                <?php $counter++ ?>
            @endif
        @endfor
        @if($paginator->nextPageUrl())
            <a href="{{ $paginator->nextPageUrl() }}" class="angle ajax-link">
            <span>
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </span>
            </a>
        @else
            <a href="javascript:void(0)" class="angle disabled">
            <span>
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </span>
            </a>
        @endif

    </div>
@endif