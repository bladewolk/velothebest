<img src="{{ asset('images/footer-cart.svg') }}" alt="caret">
@if(Session::has('cart'))
    @if(Request::has('currency') && Request::get('currency') == 'usd')
        <p>
            {{trans('messages.in_cart')}}
            <span>{{count(Session::get('cart'))}}</span>
            {{ Lang::choice('messages.products', count(Session::get('cart')), [], App::getLocale()) }}
            {{trans('messages.na_summu')}}
            <span>{{ number_format(Session::get('totalPrice'), 0, "", " ") }}</span> $</p>
    @else
        <p>{{trans('messages.in_cart')}}
            <span>{{count(Session::get('cart'))}}</span>
            {{ Lang::choice('messages.products', count(Session::get('cart')), [], App::getLocale()) }}
            {{trans('messages.na_summu')}}
            <span>{{ number_format(Session::get('totalPrice') * $settings->currency_uah, 0, "", " ") }}</span>
            грн</p>
    @endif
@else
    <p>{{trans('messages.header_cart')}} <span>{{trans('messages.header_cart_empty')}}</span></p>
@endif
<a href="/cart">
    <span>{{trans('messages.make_order')}}</span>
</a>