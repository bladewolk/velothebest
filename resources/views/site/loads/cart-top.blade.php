<img src="{{ asset('images/cart.svg') }}" alt="cart SVG image">
@if (Session::has('cart'))
    <p class="visible-md visible-lg">
        {{ trans('messages.in_cart') }}
        <span>{{ count(Session::get('cart')) }}</span>
        {{ Lang::choice('messages.products', count(Session::get('cart')), [], App::getLocale()) }}
        {{trans('messages.na_summu')}}
        @if (Request::has('currency') && Request::get('currency') == 'usd')
            <span>{{ number_format(Session::get('totalPrice')) }}</span>$
        @else
            <span>{{ number_format(Session::get('totalPrice') * $settings->currency_uah, 0, "", " ") }}</span>
            грн
        @endif
    </p>
    <p class="visible-sm-inline">
        <span>{{count(Session::get('cart'))}}</span> {{ Lang::choice('messages.products', count(Session::get('cart')), [], App::getLocale()) }}
    </p>
    <p class="visible-xs-inline"><span>{{count(Session::get('cart'))}}</span></p>
@else
    <p class="hidden-sm hidden-xs"><span>{{trans('messages.empty_cart')}}</span></p>
@endif
<a href="/cart" class="header-confirm-order hidden-sm hidden-xs">{{trans('messages.make_order')}}</a>