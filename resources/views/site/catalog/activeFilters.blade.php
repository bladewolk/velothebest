@if (Request::has('features') && is_array(Request::get('features')))
    @foreach($filters as $filter)
        @foreach($filter->variants as $variant)
            @if (in_array($variant->id, Request::get('features')))
                <div class="search-filter-item">
                    <i class="fa fa-times drop-filter" aria-hidden="true"
                       data-id="{{ $variant->id }}"></i>
                    <p>{{ $filter->localization->where('locale', App::getLocale())->first()->name }}:&nbsp;
                    <span>{{ $variant->localization->where('locale', App::getLocale())->first()->name }}</span></p>
                </div>

            @endif
        @endforeach
    @endforeach
    <div class="reset-search-filters">
        <p>{{trans('messages.drop_filter')}}</p>
    </div>
@endif