@extends('site.header')
@push('styles-stack')
<link rel="stylesheet" type="text/css" href="{{ asset('css/subcategory.css') }}">
@endpush
@section('content')

    <div class="container">
        <div class="row">
            @foreach($subcategories as $item)
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <a href="{{ $item->slug }}">
                        <div class="col-md-12 subcategorie-wrapper">
                            @if ($item->images->first())
                                <img src="{{ '/img/'.$item->images->first()->name.'?w=90&h=90&fit=crop' }}"
                                     alt="{{ $item->name }}">
                            @else
                                <img src="{{ '/img/static/no-image.jpg?w=90&h=90&fit=crop' }}" alt="{{ $item->name }}">
                            @endif
                            <p>{{ $item->name }}</p>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>

@endsection
