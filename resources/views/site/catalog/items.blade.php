    {{-- START ITEMS --}}
    @foreach($items as $item)
        <div class="col-md-4 col-sm-6">
            <div class="row">
                <div class="item-wrapper">
                     @if ($item->label)
                        <span class="item-discount" style="background-color: {{$item->label->color}}; min-width: 70%; max-width: 80%">
                            {{ $item->label->localization->where('locale', App::getLocale())->first()->description }}
                            <b>{{ $item->localization->where('locale', App::getLocale())->first()->label_text }}</b>
                        </span>
                    @endif

                    @if ($item->images->isEmpty() == false)
                        <div class="item-image" style="">
                            <img src="{{ '/img/'.$item->images->first()->name .'?w=261&h=220' }}"
                                 alt="{{ $item->localization->first()->name }}"
                                 title="{{ $item->localization->first()->name }}">
                        </div>
                    @else
                            <div class="item-image">
                                <img src="{{ '/img/static/no-image.jpg?w=261&h=220' }}"
                                     alt="{{ $item->localization->first()->name }}"
                                     title="{{ $item->localization->first()->name }}">
                            </div>
                    @endif

                    <div class="item-name">
                        <a href="/catalog/{{ $item->slug }}">
                            <p title="{{ $item->localization->first()->name }}">{{ str_limit($item->localization->first()->name, 110, '...') }}</p>
                        </a>
                    </div>

                    <div class="item-code">
                        <p>Код: <span>{{ $item->code }}</span></p>
                    </div>
                    <div class="item-custom-order">
                        @if ($item->status == 2)
                            <p><i class="fa fa-truck" aria-hidden="true"></i>
                                {{trans('messages.item_by_order')}}</p>
                        @elseif ($item->status == 0 || $item->in_stock < 1)
                            <p style="color: #7e7e7e;"><i class="fa fa-ban"
                                                          aria-hidden="true"></i>
                                {{trans('messages.item_not_in_stock')}}</p>
                        @endif
                    </div>

                    <div class="price">
                        <p>{{trans('messages.price')}}</p>
                        @if (Request::has('currency') && Request::get('currency') == 'usd')
                            @if($item->price_discount != 0)
                                <p class="old-price">{{ number_format($item->price) }}
                                    $</p>
                                <p class="current-price">
                                    <span>{{ number_format($item->price_discount) }}</span>
                                    $</p>
                            @else
                                <p class="current-price">
                                    <span>{{ number_format($item->price) }}</span>
                                    $
                                </p>
                            @endif
                        @else
                            @if($item->price_discount != 0)
                                <p class="old-price">{{ number_format($item->price * $settings->currency_uah, 0, "", " ") }}
                                    грн</p>
                                <p class="current-price">
                                    <span>{{ number_format($item->price_discount * $settings->currency_uah, 0, "", " ") }}</span>
                                    грн</p>
                            @else
                                <p class="current-price">
                                    <span>{{ number_format($item->price * $settings->currency_uah, 0, "", " ") }}</span>
                                    грн
                                </p>
                            @endif
                        @endif
                    </div>

                    <div class="by-now">
                        @if (Session::has('cart') && in_array($item->id, Session::get('cart')))
                            <button class="disabled">{{trans('messages.in_cart')}}</button>
                        @elseif ($item->status == 0 || $item->in_stock < 1)
                            <button class="disabled"
                                    data-productID="{{ $item->id }}">{{ trans('messages.item_not_in_stock') }}</button>
                        @elseif ($item->status == 2)
                            <button class="buy-this-item"
                                    data-productID="{{ $item->id }}">{{ trans('messages.to_order') }}</button>
                        @else
                            <button class="buy-this-item"
                                    data-productID="{{ $item->id }}">{{ trans('messages.button_buy') }}</button>
                        @endif
                    </div>


                    @if ($item->rating)
                        <div class="item-feedback">
                            <div class="item-stars">
                                @for($i = 1; $i < 6; ++$i)
                                    @if ($item->rating >= $i)
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    @else
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    @endif
                                @endfor
                            </div>
                            <div class="item-comments">
                                <a href="/catalog/{{ $item->slug }}" rel="nofollow">
                                    <p>{{ $item->comments->count() }} {{ trans_choice('messages.product_have_comments', $item->comments->count()) }}</p>
                                </a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
    {{--END ITEMS--}}
