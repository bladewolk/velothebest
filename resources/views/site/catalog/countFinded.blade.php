@if ($items->total() > 0)
    <p>{{trans('messages.system_finded')}}:
        <span>{{ $items->total() }} {{ Lang::choice('messages.countItems', $items->total(), [], App::getLocale()) }}</span>
    </p>
@else
    <p>{{trans('messages.system_finded_null')}}</p>
@endif
