@extends('site.header')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/catalog.css')}}">
    <link rel="stylesheet" href="{{ asset('css/nouislider.css') }}">
    <script src="{{ asset('js/nouislider.min.js') }}"></script>
@endsection
@section('content')
    <div class="container content">
        <form action="/" method="GET">
            <div class="row">
                {{--LEFT-SIDE--}}
                <div class="left-side col-md-3 col-sm-4 col-xs-12">
                    <p class="show-filters hidden-md hidden-lg">
                        {{trans('messages.system_show_filters')}}
                        <span class="fa fa-angle-right"></span>
                    </p>
                    <div class="filters-wrapper hidden-sm hidden-xs">
                        <div class="col-md-12 col-sm-12 count-finded-items">
                            @include ('site.catalog.countFinded')
                        </div>
                        <div class="col-md-12 col-sm-12 search-filters">
                            @include('site.catalog.activeFilters')
                        </div>

                        <div class="col-md-12 col-sm-12 filter-wrapper ">
                            <p class="filter-name">
                                {{trans('messages.price')}}
                                <span class="fa fa-angle-right rotate-angle"></span>
                            </p>
                            <div class="price-slider-wrapper">
                                <span>{{trans('messages.system_catalog_from')}}</span><input type="number" min="1"
                                                      value="{{ $currentMinPrice ? $currentMinPrice : $minPrice }}"
                                                      name="minPrice">
                                <span>{{trans('messages.system_catalog_to')}}</span><input type="number" min="1"
                                                      value="{{ $currentMaxPrice ? $currentMaxPrice : $maxPrice }}"
                                                      name="maxPrice">
                                <div id="price-slider">

                                </div>
                            </div>
                        </div>
                        @foreach($filters as $filter)
                            <div class="col-md-12 col-sm-12 filter-wrapper ">
                                <p class="filter-name">
                                    {{ $filter->localization->where('locale', App::getLocale())->first()->name }}
                                    <span class="fa fa-angle-right rotate-angle"></span>
                                </p>
                                <ol>
                                    @foreach($filter->variants as $variant)
                                        <li>
                                            <input type="checkbox" id="checkbox{{ $variant->id }}" name="features[]"
                                                   value="{{ $variant->id }}" }}
                                                   @if (Request::has('features') && in_array($variant->id, Request::get('features')))
                                                   checked="true"
                                                    @endif
                                            >
                                            <label for="checkbox{{ $variant->id }}">{{ $variant->localization->where('locale', App::getLocale())->first()->name }}</label>
                                        </li>
                                    @endforeach
                                </ol>
                            </div>
                        @endforeach
                    </div>

                </div>

                {{--END LEFT SIDE--}}
                {{--RIGHT-SIDE--}}
                <div class="right-side col-md-9 col-sm-8 col-xs-12">
                    <div class="row">
                        {{-- TOP BAR --}}
                        <div class="col-md-12">
                            <div class="row right-side-top-nav">

                                <div class="col-md-6 col-sm-6 col-xs-12 sorted">
                                    <span class="fa fa-caret-right visible-xs-inline-block"></span>
                                    <p>{{trans('messages.catalog_sort')}}</p>
                                    {{ Form::select('sort',
                                    ['id|desc' => trans('messages.catalog_sort_starndart'),'price|asc' => trans('messages.catalog_price_sort_asc'), 'price|desc' => trans('messages.catalog_price_sort_desc')],
                                    null,
                                    ['class' => 'col-xs-12 hidden-xs', 'id' => 'sort']) }}
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 item-on-page">
                                    <span class="fa fa-caret-right visible-xs-inline-block"></span>
                                    <p>{{trans('messages.catalog_item_on_page')}}</p>
                                    {{ Form::select('paginate', [24 => 24,48 => 48,72 => 72],
                                    Request::has('paginate') ? array_search(Request::get('paginate'), [10 => 10,15 => 15,25 => 25,50 => 50]) : 0
                                    , ['class' => 'col-xs-12 hidden-xs', 'id' => 'item-on-page']) }}
                                </div>
                            </div>
                        </div>
                        {{-- END TOP BAR --}}

                    </div>
                </div>
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div class="row items-container-load">
                        @include('site.catalog.items')
                    </div>
                    <div class="row items-bottom-load">
                        @include('site.catalog.items-bottom')
                    </div>
                </div>
            </div>
        </form>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/catalog.min.js') }}"></script>
    <script>
        $(function () {
            $(document).on("click", '.by-now > .row > button', function (event) {
                event.preventDefault();
                console.log('click');
            });

            $('.show-filters').click(function () {
                $(this).find('span').toggleClass('rotate-angle');
                $('.filters-wrapper').toggleClass('hidden-sm hidden-xs');
            });

            $('.filter-name').click(function () {
                $(this).closest('.filter-wrapper').find('ol, div').stop().slideToggle(400);
                $(this).closest('.filter-wrapper').find('span').toggleClass('rotate-angle');
            });

//          Подгрузка продуктов
            function loadItems() {
                newLink = $('form').serialize();
                if (location.href.match(/currency/))
                    newLink += '&currency=' + location.href.match(/currency=([a-z]{3})/)[1];

                $('.items-container-load').toggleClass('load-items');
                $.get(location.pathname + '?' + newLink, function (response) {
                    result = JSON.parse(response);
                    $('.items-container-load').html(result.products);
                    $('.search-filters').html(result.filters);
                    $('.count-finded-items').html(result.finded);
                    $('.items-bottom-load').html(result.bottom);
                    $('.items-container-load').toggleClass('load-items');
                    window.history.pushState("", "", location.pathname + '?' + newLink);
                });
            }

            $(document).on("change", ":checkbox, #item-on-page, #sort", function () {
                loadItems();
            });

            $(document).on("click", '.ajax-link', function () {
                event.preventDefault();
                newLink = $(this).prop("href");
                $('.items-container-load').toggleClass('load-items');
                $.get(newLink, function (response) {
                    result = JSON.parse(response);
                    $('.items-container-load').html(result.products);
                    $('.items-bottom-load').html(result.bottom);
                    $('.items-container-load').toggleClass('load-items');
                    window.history.pushState("", "", newLink);
                });
            })

//            Сбросить фильтр
            $(document).on("click", '.drop-filter', function () {
                $("#checkbox" + $(this).data('id')).prop("checked", false);
                loadItems();
            });
//          Сбросить все фильтры
            $(document).on("click", '.reset-search-filters', function () {
                $(":checkbox").prop("checked", false);
                $('input[name="minPrice"]').val({{ $minPrice }});
                $('input[name="maxPrice"]').val({{ $maxPrice }});
//            Слайдер при сетте срабатывает отправка формы
                handlesSlider.noUiSlider.set([{{ $minPrice }}, {{ $maxPrice }}]);
            });

//          Ценовой слайдер
            var handlesSlider = document.getElementById('price-slider');

            noUiSlider.create(handlesSlider, {
                start: [ {{ $currentMinPrice ? $currentMinPrice : $minPrice }}, {{ $currentMaxPrice ? $currentMaxPrice : $maxPrice }}],
                connect: [false, true, false],
                step: 1,
                range: {
                    'min': [  {{ ($minPrice)}} ],
                    'max': [ {{ ($maxPrice) }} ]
                }
            });

            handlesSlider.noUiSlider.on('set', function (eve) {
                loadItems();
            });

            handlesSlider.noUiSlider.on('slide', function (eve) {
                $('input[name="minPrice"]').val(parseInt(eve[0]));
                $('input[name="maxPrice"]').val(parseInt(eve[1]));
            });

            $('input[name="minPrice"], input[name="maxPrice"]').on("change", function () {
                loadItems();
            });

        });
    </script>
@endsection