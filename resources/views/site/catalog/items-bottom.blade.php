<div class="col-md-6">
    @include('site.pagination', ['paginator' => $items->appends(Request::except('page'))])
</div>
@if( $items->currentPage() != $items->lastPage() && $items->total() > 0)
    <div class="col-md-6" style="text-align: right">
        <button class="show-all-products">
            {{trans('messages.system_show_all_products')}}
        </button>
    </div>
@endif