@extends('site.header')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/product.css') }}">
    <link rel="stylesheet" href="{{ asset('css/xzoom.css') }}">
@endsection
@section('content')
    <div class="container">
        <div class="product-name">
            <p>{{ $item->localization->where('locale', App::getLocale())->first()->name }}</p>
        </div>
        <div class="col-md-7 col-sm-12 product-images">
             @if ($item->label)
                <span class="item-label" style="background-color: {{$item->label->color}};">
                    {{ $item->label->localization->where('locale', App::getLocale())->first()->description }}
                    <b>{{ $item->localization->where('locale', App::getLocale())->first()->label_text }}</b>
                </span>
            @endif
            @if ($item->images->isEmpty() == false)
                <img class="product-image xzoom"
                     src="{{ '/img/'.$item->images->first()->name .'?w=665&h=500&q=75' }}" alt="image"
                     xoriginal="{{ '/img/'.$item->images->first()->name }}">
            @else
                <img class="product-image" src="{{ '/img/static/no-image.jpg' }}" alt="image">
            @endif
            <div class="additional-images">
                @foreach($item->images as $image)
                    <a href="{{ '/img/'.$image->name }}">
                        <div class="xzoom-gallery image-container"
                             xpreview="{{ '/img/'.$image->name. '?w=665&h=500&q=75' }}">
                            <img src="{{ '/img/'.$image->name. '?w=80&h=80&q=50' }}" alt="">
                        </div>
                    </a>
                @endforeach
            </div>
        </div>

        <div class="col-md-5 col-sm-12 col-xs-12 product-rightSide">
            <div class="row">
                @if($item->color_id)
                    <div class="col-md-12 col-sm-6 col-xs-12 product-colors">
                        <div class="row">
                            <p>{{trans('messages.system_color')}}</p>
                            @if ($item->group)
                               @foreach($item->group->products->sortBy('code') as $groupItem)
                                    @if ($item->main_feature && $groupItem->main_feature_text == $item->main_feature_text && $groupItem->color)
                                        <a href="{{ $groupItem->slug }}" class="color {{ $groupItem->id == $item->id ? 'active' : '' }}">
                                            <span style="background-color: {{ $groupItem->color->value }}; border-color: {{ $groupItem->color->value }};"></span>
                                        </a>
                                    @elseif (!$item->main_feature && $groupItem->color)
                                        <a href="{{ $groupItem->slug }}" class="color {{ $groupItem->id == $item->id ? 'active' : '' }}">
                                            <span style="background-color: {{ $groupItem->color->value }}; border-color: {{ $groupItem->color->value }};"></span>
                                        </a>
                                    @endif
                                @endforeach
                            @else
                                <a href="{{ $item->slug }}" class="color active">
                                    <span style="background-color: {{ $item->color->value }}; border-color: {{ $item->color->value }};"></span>
                                </a>
                            @endif
                        </div>
                    </div>
                @endif

                @if ($item->group)
                    <div class="col-md-12 col-sm-6 col-xs-12 product-sizes">
                        <div class="row">
                            @if ($item->mainFeature)
                                <p>{{ $item->mainFeature->localization->where('locale', App::getLocale())->first()->name }}</p>
                                @foreach($item->group->products->sortBy('code')->unique('main_feature_text') as $value)
                                @if($value->main_feature > 0)
                                    <a href="/catalog/{{ $value->slug }}"
                                        class="product-size {{ $value->main_feature_text == $item->main_feature_text ? 'active' : '' }}">
                                        <span>{{ preg_replace('/\s.+/', '', $value->main_feature_text) }}</span>
                                        {{-- <span>{{ intval($value->main_feature_text) }}</span> --}}
                                    </a>
                                @endif
                                @endforeach
                                @if (preg_replace('/.+\s/', '', $value->main_feature_text) != preg_replace('/\s.+/', '', $value->main_feature_text))
                                    <span>{{ preg_replace('/.+\s/', '', $item->main_feature_text) }}</span>
                                @endif
                            @endif
                        </div>
                    </div>
                @endif


                <div class="product-nav col-md-12 col-sm-8 col-xs-12">
                    <div class="top">
                        @if ($item->comments->count())
                            <p class="stars">
                                @for($i = 1; $i < 6; $i++)
                                    @if ($i <= $item->rating)
                                        <i class="fa fa-star"></i>
                                    @else
                                        <i class="fa fa-star-o"></i>
                                    @endif
                                @endfor
                                <span>{{ $item->comments->count() }}
                                    {{ trans_choice('messages.product_have_comments', $item->comments->count()) }}
                                </span>
                            </p>
                        @endif
                        <p class="code">Код:<span> {{ $item->code }}</span></p>
                        @if ($item->status == 2)
                            <p class="status" style="color: #0c6eb5;"><i style="color: #0c6eb5;" class="fa fa-truck"
                                                                         aria-hidden="true"></i>{{ trans('messages.item_by_order') }}
                            </p>
                        @elseif ($item->status == 0 || $item->in_stock < 1)
                            <p style="color: #7e7e7e;"><i class="fa fa-ban"
                                                          aria-hidden="true"></i>{{ trans('messages.item_not_in_stock') }}
                            </p>
                        @else
                            <p class="status"><i class="fa fa-check-circle-o"
                                                 aria-hidden="true"></i>{{ trans('messages.item_in_stock') }}</p>
                        @endif
                    </div>

                    <div class="bottom">
                        <div class="left col-md-6 col-sm-6 col-xs-6">
                            <span>{{trans('messages.price')}}:</span>
                            @if (Request::has('currency') && Request::get('currency') == 'usd')
                                <p>
                                    @if ($item->price_discount != 0)
                                        <span class="old-price">
                                        {{ $item->price_discount }}$
                                    </span>
                                    @endif
                                    {{ $item->price }}<span>$</span>
                                </p>
                            @else
                                <p>
                                    @if ($item->price_discount != 0)
                                        <span class="old-price">
                                        {{ number_format($item->price_discount * $settings->currency_uah, 0, "", ' ') }}
                                            грн
                                    </span>
                                    @endif
                                    {{ number_format($item->price * $settings->currency_uah, 0, ".", ' ') }}
                                    <span> грн</span>
                                </p>
                            @endif
                        </div>

                        <div class="right col-md-6 col-sm-6 col-xs-6">
                            @if ($item->status == 0 || $item->in_stock < 1)
                                <button class="disabled"
                                        data-productID="{{ $item->id }}">{{ trans('messages.item_not_in_stock') }}</button>
                            @elseif(Session::has('cart') && in_array($item->id, Session::get('cart')))
                                <button class="button-in-cart"
                                        data-productID="{{ $item->id }}">{{ trans('messages.in_cart') }}</button>
                            @elseif ($item->status == 2)
                                <button class="buy-this-item"
                                        data-productID="{{ $item->id }}">{{ trans('messages.item_by_order') }}</button>
                            @else
                                <button class="buy-this-item"
                                        data-productID="{{ $item->id }}">{{ trans('messages.button_buy') }}</button>
                            @endif
                        </div>
                    </div>
                </div>

                <p class="col-md-12 col-sm-4 col-xs-12 answer" data-view="product_answer" data-id="{{ $item->id }}">
                    <i class="fa fa-question" aria-hidden="true"></i>
                    {{ trans('messages.make_question_this_item') }}
                </p>
            </div>

        </div>

        <div class="col-md-12 col-sm-12 col-xs-12 tab-panel">
            <div class="header">
                <ul>
                    <li data-tab="descr" class="active ">
                        <p>{{trans('messages.system_description')}}</p>
                    </li>
                    <li data-tab="char" class="">
                        <p>{{trans('messages.system_features')}}</p>
                    </li>
                    <li data-tab="del" class="">
                        <p>{{trans('messages.system_delivery_and_payment')}}</p>
                    </li>
                    <li data-tab="quant" class="">
                        <p>{{trans('messages.system_quantities')}}</p>
                    </li>
                    <li data-tab="vid" class="">
                        <p>{{trans('messages.system_video')}}</p>
                    </li>
                    <li data-tab="feed" class="">
                        <p>{{trans('messages.system_feedbacks')}} (<span>{{ $item->comments->count() }}</span>)</p>
                    </li>
                </ul>
            </div>

            <div class="tab-content">
                <div id="descr" class="tab col-xs-12">
                    @if ($item->localization->first()->description)
                        {!! $item->localization->first()->description !!} @else
                    @endif
                </div>

                <div id="char" class="tab col-xs-12">
                    @foreach($item->characteristicks->where('locale', App::getLocale()) as $ch)
                        <div class="row chars">
                            <div class="col-md-4 charname">
                                <p>{{ $ch->option->localization->where('locale', App::getLocale())->first()->name }}:</p>
                            </div>
                            <div class="col-md-8 chardescr">
                                <p>{{ $ch->value }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div id="del" class="tab col-xs-12">
                    {!! $delivery !!}
                </div>

                <div id="quant" class="tab col-xs-12">
                    {!! $quant !!}
                </div>
                @if ($item->video)
                    <div id="vid" class="tab col-xs-12">
                        {!! $item->video !!}
                    </div>
                @endif

                <div id="feed" class="tab col-xs-12">
                    @foreach($item->comments as $comment)
                        <div class="comment-wrapper row">
                            <i class="comment-user fa fa-user"></i>
                            <p class="comment-user-name">
                                {{ $comment->name }}
                            </p>
                            <div class="comment-stars">
                                @for($i = 1; $i < 6; $i++)
                                    @if ($i <= $comment->rating)
                                        <i class="fa fa-star"></i>
                                    @else
                                        <i class="fa fa-star-o"></i>
                                    @endif
                                @endfor
                            </div>
                            <div class="comment-date">
                                <p>{{ $comment->created_at->format('d/m/y') }}</p>
                            </div>
                            <div class="comment-text">
                                <p>
                                    {{ $comment->comment }}
                                </p>
                            </div>
                        </div>
                    @endforeach
                    <div class="make-comment col-md-12 col-xs-12">
                        <form action="/" method="POST" class="make-comment-form">
                            <p class="make-comment-title">
                                {{trans('messages.system_make_your_feedback')}}
                            </p>
                            <div class="col-md-6 col-xs-12">
                                <input type="text" maxlength="255" value="testname" autocomplete="off" name="name"
                                       required
                                       placeholder="{{ trans('messages.enter_your_name') }}">
                                <input type="email" value="test@test.test" maxlength="100" autocomplete="off"
                                       name="email" required
                                       placeholder="{{ trans('messages.enter_your_email') }}">
                                <input id="rating-input" type="hidden" name="rating" value="0">
                                <input type="hidden" name="product_id" value="{{ $item->id }}">
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <p class="rating">{{trans('messages.system_stars')}}:
                                    <span class="rating-stars">
                                        <i class="fa fa-star-o" data-id="1"></i>
                                        <i class="fa fa-star-o" data-id="2"></i>
                                        <i class="fa fa-star-o" data-id="3"></i>
                                        <i class="fa fa-star-o" data-id="4"></i>
                                        <i class="fa fa-star-o" data-id="5"></i>
                                    </span>
                                </p>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <textarea name="comment" id="" maxlength="1500" cols="30" rows="10" required
                                          autocomplete="off"
                                          placeholder="{{ trans('messages.enter_your_comment') }}">test comment</textarea>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <button>{{trans('messages.system_make_feedback')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        {{-- end Container --}}
    </div>

    {{-- Сопутствующие товары --}}
    @if($item->associated->count())
        @include('site.product.carousel')
    @endif
    {{-- END сопутствуюзие товары --}}

@endsection
@section('scripts')
    <!-- XZOOM JQUERY PLUGIN  -->
    <script src="{{ asset('js/xzoom.js') }}"></script>
    <script src="{{ asset('js/product.min.js') }}"></script>
    <script>
        $(function () {
            $(".xzoom, .xzoom-gallery").xzoom({
                position: 'lens',
                sourceClass: 'xzoom-hidden',
                tint: "#0c6eb5",
                tintOpacity: 0.25,
                // zoomWidth: 400,
                Xoffset: 15,
                fadeIn: true,
            });

            $('.header > ul > li').click(function () {
                $('.header > ul > li').each(function (index, value) {
                    $(this).removeClass('active');
                });
                $(this).toggleClass('active');

                $('.tab-content .tab').each(function () {
                    $(this).removeClass('active');
                });

                $("#" + $(this).data('tab') + "").toggleClass('active');
            });


            // Задать вопрос о продукте
            $(document).on('submit', '.modal-product-answer', function (e) {
                e.preventDefault();
                $.post('/question', {
                    _token: "{{ csrf_token() }}",
                    email: $(this).find('input').val(),
                    comment: $(this).find('textarea').val(),
                    product_id: $('.answer').data('id')
                }, function (res) {
                    $('.modal-wrapper').toggleClass('show-modal-on');

                    $('.modal-wrapper').load("/loadAjax/success", function () {
                        $(this).find('p').html(res);
                        $(this).toggleClass('show-modal-on');
                        setTimeout(function () {
                            $('.modal-wrapper').toggleClass('show-modal-on');
                        }, 1500);
                    });
                });

            });

        });

    </script>
@endsection