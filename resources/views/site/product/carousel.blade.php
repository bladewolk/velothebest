<div class="soput-wrapper">
    <div class="container">
        <div class="soput-title">
            <p>{{trans('messages.system_carousel_item')}}</p>
        </div>
        <div class="slider-wrapper">

            @foreach($item->associated->chunk(4) as $items)
                {{-- Item WRAPPER --}}
                <div class="slider-slide">
                    @foreach($items as $carouselItem)

                        <div class="col-md-3 col-sm-6">
                            <div class="row">
                                <div class="item-wrapper">

                                    @if ($carouselItem->product->label)
                                        <span class="item-discount" style="background-color: {{$carouselItem->product->label->color}}; min-width: 70%; max-width: 80%">
                                            {{ $carouselItem->product->label->localization->where('locale', App::getLocale())->first()->description }}
                                            <b>{{ $carouselItem->product->localization->where('locale', App::getLocale())->first()->label_text }}</b>
                                        </span>
                                    @endif
                                
                                    @if ($carouselItem->product->images->isEmpty() == false)
                                        <div class="item-image" style="">
                                            <img src="{{ '/img/'.$carouselItem->product->images->first()->name .'?w=261&h=220&q=75' }}"
                                                 alt="{{ $carouselItem->product->localization->first()->name }}"
                                                 title="{{ $carouselItem->product->localization->first()->name }}">
                                        </div>
                                    @else
                                        <div class="item-image">
                                            <img src="{{ '/img/static/no-image.jpg?w=261&h=220&q=75' }}"
                                                 alt="{{ $carouselItem->product->localization->first()->name }}"
                                                 title="{{ $carouselItem->product->localization->first()->name }}">
                                        </div>
                                    @endif

                                    <div class="item-name">
                                        <a href="/catalog/{{ $carouselItem->product->slug }}">
                                            <p title="{{ $carouselItem->product->localization->first()->name }}">{{ str_limit($carouselItem->product->localization->first()->name, 110, '...') }}</p>
                                        </a>
                                    </div>

                                    <div class="item-code">
                                        <p>Код: <span>{{ $carouselItem->product->code }}</span></p>
                                    </div>
                                    <div class="item-custom-order">
                                        @if ($carouselItem->product->status == 2)
                                            <p><i class="fa fa-truck" aria-hidden="true"></i>
                                                {{trans('messages.item_by_order')}}</p>
                                        @elseif ($carouselItem->product->status == 0 || $item->in_stock < 1)
                                            <p style="color: #7e7e7e;"><i class="fa fa-ban"
                                                                          aria-hidden="true"></i>
                                                {{trans('messages.item_not_in_stock')}}</p>
                                        @endif
                                    </div>

                                    <div class="price">
                                        <p>{{trans('messages.price')}}</p>
                                        @if (Request::has('currency') && Request::get('currency') == 'usd')
                                            @if($carouselItem->product->price_discount != 0)
                                                <p class="old-price">{{ number_format($carouselItem->product->price) }}
                                                    $</p>
                                                <p class="current-price">
                                                    <span>{{ number_format($carouselItem->product->price_discount) }}</span>
                                                    $</p>
                                            @else
                                                <p class="current-price">
                                                    <span>{{ number_format($carouselItem->product->price) }}</span>
                                                    $
                                                </p>
                                            @endif
                                        @else
                                            @if($carouselItem->product->price_discount != 0)
                                                <p class="old-price">{{ number_format($carouselItem->product->price * $settings->currency_uah, 0, "", " ") }}
                                                    грн</p>
                                                <p class="current-price">
                                                    <span>{{ number_format($carouselItem->product->price_discount * $settings->currency_uah, 0, "", " ") }}</span>
                                                    грн</p>
                                            @else
                                                <p class="current-price">
                                                    <span>{{ number_format($carouselItem->product->price * $settings->currency_uah, 0, "", " ") }}</span>
                                                    грн
                                                </p>
                                            @endif
                                        @endif
                                    </div>

                                    <div class="by-now">
                                        @if (Session::has('cart') && in_array($carouselItem->product->id, Session::get('cart')))
                                            <button class="disabled">{{trans('messages.in_cart')}}</button>
                                        @elseif ($carouselItem->product->status == 0 || $item->in_stock < 1)
                                            <button class="disabled"
                                                    data-productID="{{ $item->id }}">{{ trans('messages.item_not_in_stock') }}</button>
                                        @elseif ($carouselItem->product->status == 2)
                                            <button class="buy-this-item"
                                                    data-productID="{{ $carouselItem->product->id }}">{{ trans('messages.to_order') }}</button>
                                        @else
                                            <button class="buy-this-item"
                                                    data-productID="{{ $carouselItem->product->id }}">{{ trans('messages.button_buy') }}</button>
                                        @endif
                                    </div>


                                    @if ($carouselItem->product->rating)
                                        <div class="item-feedback">
                                            <div class="item-stars">
                                                @for($i = 1; $i < 6; ++$i)
                                                    @if ($carouselItem->product->rating >= $i)
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    @else
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    @endif
                                                @endfor
                                            </div>
                                            <div class="item-comments">
                                                <a href="/catalog/{{ $carouselItem->product->slug }}" rel="nofollow">
                                                    <p>{{ $carouselItem->product->comments->count() }} {{ trans_choice('messages.product_have_comments', $carouselItem->product->comments->count()) }}</p>
                                                </a>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        {{-- END ITEM WRAPPER --}}
                    @endforeach
                </div>
            @endforeach
        </div>
        <span class="slick-prev fa fa-angle-left"></span>
        <span class="slick-next fa fa-angle-right"></span>
    </div>
</div>

@push('styles-stack')
<link rel="stylesheet" href="{{ asset('css/slick.css') }}">
@endpush
@push('script-stack')
<script src="{{ asset('js/slick.min.js') }}"></script>
<script>
    $('.slider-wrapper').slick({
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 1000,
        arrows: true,
        nextArrow: $('.slick-next'),
        prevArrow: $('.slick-prev'),
    });
</script>
@endpush