<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>VeloTheBest</title>

    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('css/bootstrap-grid.css') }}">
    <script src="https://use.fontawesome.com/5c9f604857.js"></script>

    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('css/layout.css') }}">
    @yield('styles')
</head>
<body>
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-5 logotype">
                <div class="row">
                    <div class="burger col-sm-1 col-xs-2 hidden-lg hidden-md">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </div>
                    <div class="col-md-12 col-sm-9 col-xs-9">
                        <img src="{{ asset('images/logo.png') }}" alt="LogoType">
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 hidden-xs">
                <div class="row contacts">
                    <div class="col-md-1 col-sm-1">
                        <div class="row phone-icon">
                            <span class="fa fa-phone"></span>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-10 phones">
                        <div class="row">
                            <p><span>(050)</span> 451-60-08 <i class="fa fa-angle-down visible-sm-inline-block"
                                                               aria-hidden="true"></i></p>
                            <p class="hidden-sm"><span>(050)</span> 451-60-08</p>
                            <p class="hidden-sm"><span>(093)</span> 451-60-08</p>
                            <p class="call hidden-sm">{{ trans('messages.answer_call') }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-5 col-xs-4 header-center">
                <div class="header-center-navigation hidden-sm hidden-xs">
                    <a href="#">Оплата и доставка</a>&bull;
                    <a href="#">Помощь</a>&bull;
                    <a href="#">Гарантии</a>&bull;
                    <a href="#">О нас</a>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 header-center-middle">
                    <div class="row">
                        <div class="col-md-6 col-sm-5 col-xs-12">
                            <div class="row">
                                <div class="header-login hidden-sm hidden-xs">
                                    <span class="fa fa-sign-in"></span>&nbsp;
                                    <p>Войти</p><span>&nbsp;/</span>
                                    <p>Зарегестрироватся</p>
                                </div>
                                <a href="login" class="visible-sm-inline-block visible-xs-inline-block">
                                    <i class="fa fa-sign-in" aria-hidden="true"></i>
                                </a>
                                <i class="fa fa-search hidden-lg hidden-md" aria-hidden="true"></i>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-7 hidden-xs header-lang-choise">
                            <div class="row">
                                <ul>
                                    <li><span>RU</span><i class="fa fa-angle-down" aria-hidden="true"></i>
                                        <ul>
                                            <li>UA</li>
                                        </ul>
                                    </li>
                                </ul>
                                &nbsp;
                                <ul>
                                    <li><span>Гривны</span><i class="fa fa-angle-down" aria-hidden="true"></i>
                                        <ul>
                                            <li>Доляры</li>
                                            <li>Рубаси</li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="header-search hidden-sm hidden-xs">
                    <input type="text" placeholder="{{ trans('messages.enter_search') }}">
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 cart">
                        <img src="{{ asset('images/cart.svg') }}" alt="">
                        <p class="hidden-sm hidden-xs">В корзине</p> <span class="">10</span>
                        <p class="hidden-xs">товаров</p>
                        <p class="hidden-sm hidden-xs">на сумму</p><span class="hidden-sm hidden-xs">15234</span>
                        <p class="hidden-sm hidden-xs">грн</p>
                    </div>
                    <div class="col-md-12 make-order hidden-sm hidden-xs">
                        <a href="#"><p>{{ trans('messages.make_order') }}</p></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row header-bottom">
            <div class="col-md-10 col-sm-10">
                <div class="row header-bottom-info">
                    <p>Консультации и заказ по телефонам: <span>ПН-ПТ с 9:00 до 17:30</span>. Через корзину —
                        круглосуточно
                    </p>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 hidden-xs">
                <div class="row social">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                    <i class="fa fa-instagram" aria-hidden="true"></i>
                    <i class="fa fa-youtube-play" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>
</header>
<nav>
    <div class="container">
        <div class="row navigation">
            <a href="#">КАТАЛОГ <i class="fa fa-sort-desc visible-sm visible-xs" aria-hidden="true"></i></a>
            <a href="#">БРЕНДЫ <i class="fa fa-sort-desc visible-sm visible-xs" aria-hidden="true"></i></a>
            <a href="#">ПОДБОР ВЕЛОСИПЕДОВ</a>
            <a href="#">КОНТАКТЫ</a>
            <a href="#">АКЦИИ И СКИДКИ</a>
            <a href="#">РАСПРОДАЖА</a>
        </div>
    </div>
</nav>

{{--<div class="modal-call-feedback hidden">--}}
    {{--<div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4">--}}
        {{--<div class="row form">--}}
            {{--<i class="fa fa-times" aria-hidden="true"></i>--}}
            {{--<form action="">--}}
                {{--<p>ЗАКАЖИТЕ ОБРАТНЫЙ ЗВОНОК</p>--}}
                {{--<input type="tel" placeholder="+38(___)___ __ __">--}}
                {{--<button type="submit">ЗАКАЗАТЬ</button>--}}
            {{--</form>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

@yield('content')




<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-6 footer-logo">
                <img src="{{ asset('images/logo.png') }}" alt="Logotype">
                <p>2015 © velothebest.com</p>
                <a href=""><img class="dvacom-logo" src="{{ asset('images/dvacom.png') }}" alt="DvaCom"></a>
            </div>

            <div class="col-md-2 col-sm-5 col-xs-6 footer-nav">
                <a href="#">КАТАЛОГ</a>
                <a href="#">БРЕНДЫ</a>
                <a href="#">ПОДБОР ВЕЛОСИПЕДОВ</a>
                <a href="#">КОНТАКТЫ</a>
                <a href="#">АКЦИИ И СКИДКИ</a>
                <a href="#">РАСПРОДАЖА</a>
            </div>

            <div class="col-md-3 col-sm-5 col-xs-12 footer-subnav">
                <ol>
                    <li><a href="#">О нас</a></li>
                    <li><a href="#">Инструкция использования сайта</a></li>
                    <li><a href="#">Оплата и доставка</a></li>
                    <li><a href="#">Гарантия</a></li>
                    <li><a href="#">Помощь сайту</a></li>
                </ol>

                <div class="footer-soc">
                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                </div>
            </div>

            <div class="col-md-5 col-sm-12 col-xs-12 footer-phones">
                <div class="row">
                    <div class="col-md-7 col-sm-5">
                        <div class="footer-phones-wrap">
                            <div class="footer-phones-item">
                                <img src="{{ asset('images/life.png') }}" alt="">
                                <p>+38 <span>(050)</span> 74 82 196</p>
                            </div>
                            <div class="footer-phones-item">
                                <img src="{{ asset('images/kiev.png') }}" alt="">
                                <p>+38 <span>(050)</span> 74 82 196</p>
                            </div>
                            <div class="footer-phones-item">
                                <img src="{{ asset('images/mts.png') }}" alt="">
                                <p>+38 <span>(050)</span> 74 82 196</p>
                            </div>
                            <button class="feedback-button"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;Заказать
                                звонок
                            </button>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-4 col-xs-12">
                        <div class="row">
                            <div class="footer-basket">
                                <img src="{{ asset('images/footer-cart.svg') }}" alt="caret">
                                <p>В корзине <span>10</span> товаров
                                    на сумму <span>15447</span> грн</p>
                                <button class="footer-make-order">Оформить заказ</button>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-7 col-sm-3">
                        <p>Консультации и заказ по телефонам: ПН-ПТ с 9:00 до 17:30. Через корзину — круглосуточно</p>
                    </div>
                    <div class="col-md-5 col-sm-3">
                        <p style="text-align: center;">Принимаем к оплате</p>
                        <img src="{{ asset("images/visa.png") }}" alt="Visa">
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
@yield('scripts')
</body>
</html>