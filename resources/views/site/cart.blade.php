@extends('site.header')
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/cart.css') }}">
@endsection
@section('content')
    {{ Form::open(['url' => '/makeOrder']) }}
    <div class="container content">
        @if(Session::has('cart'))
            <div class="row">
                <div class="col-md-7 col-sm-4 col-xs-12">
                    <p class="visible-xs show-items-in-basket">{{trans('messages.system_show_items_in_basket')}} <span
                                class="fa fa-angle-right"></span></p>

                    @foreach($items as $item)
                        <div class="row item-wrapper hidden-xs">
                            {{ Form::hidden('items[]', $item->id) }}
                            <i class="fa fa-times drop-from-cart" data-productid="{{ $item->id }}"
                               aria-hidden="true"></i>
                            <div class="col-md-4 col-sm-12 col-xs-12 item-picture">
                                @if ($item->images->first())
                                    <img src="{{ '/img/'.$item->images->first()->name.'?w=225&h=225' }}"
                                         alt="{{ $item->localization->first()->name }}">
                                @else
                                    <img src="{{ '/img/static/no-image.jpg?w=225&h=225' }}" alt="Picture Not Found">
                                @endif
                            </div>
                            <div class="col-md-8 col-sm-12 col-xs-12 item-name">
                                <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), 'catalog/'.$item->slug) }}">
                                    <p>{{ $item->localization->first()->name }}</p>
                                </a>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-5 item-count">
                                <i class="fa fa-caret-left" aria-hidden="true"></i>
                                <input type="text" value="1" name="counts[]">
                                <i class="fa fa-caret-right" aria-hidden="true"></i>
                            </div>
                            <div class="col-md-4 col-sm-7 col-xs-7 item-price">
                                @if (Request::get('currency') == "usd")
                                    @if ($item->price_discount)
                                        <p data-price="{{ $item->price_discount }}">{{ number_format($item->price_discount, 0, "", " ") }}
                                            <span>$</span></p>
                                    @else
                                        <p data-price="{{ $item->price }}">{{ number_format($item->price, 0, "", " ") }}
                                            <span>$</span></p>
                                    @endif
                                @else
                                    @if($item->price_discount)
                                        <p data-price="{{ $item->price_discount * $settings->currency_uah }}">{{ number_format($item->price_discount * $settings->currency_uah, 0, "", " ") }}
                                            <span>грн</span></p>
                                    @else
                                        <p data-price="{{ $item->price * $settings->currency_uah }}">{{ number_format($item->price * $settings->currency_uah, 0, "", " ") }}
                                            <span>грн</span></p>
                                    @endif
                                @endif
                            </div>

                        </div>
                    @endforeach

                    <div class="row total-cost">
                        <p>{{trans('messages.system_so')}}
                            <span>
							@if (Request::get('currency') == 'usd')
                                    {{ number_format(Session::get('totalPrice'), 0, "", " ") }} $
                                @else
                                    {{ number_format(Session::get('totalPrice') * $settings->currency_uah, 0, "", " ") }}
                                    грн
                                @endif
						</span>
                        </p>
                    </div>
                    <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), '/') }}">
                        <p class="back-to-shop">
                            {{trans('messages.system_back_to_shop')}}
                        </p>
                    </a>
                </div>
                {{-- right-part --}}
                <div class="col-md-5 col-sm-8 col-xs-12">
                    @if (Auth::guest())
                        <div class="head">
                            <div class="col-md-6 active">
                                <p>{{trans('messages.system_i_new_shopper')}}</p>
                            </div>
                            <div class="col-md-6">
                                <p class="sign-in" data-view="signin">{{trans('messages.system_i_aldeady_user')}}</p>
                            </div>
                        </div>
                        <div class="tab-body col-md-12">
                            <div class="row">
                                <p class="title">{{trans('messages.system_enter_your_data')}}</p>
                                <input class="required" type="text" placeholder="{{trans('messages.system_your_FIO')}}" name="name" required
                                       autocomplete="off">
                                <input type="email" placeholder="{{trans('messages.enter_your_email')}}" name="email"
                                       autocomplete="off">
                                <input class="required phone-modal" type="tel" name="phone"
                                       placeholder="+38(___)___ __ __" required autocomplete="off">
                                <p class="title">{{trans('messages.address_delivery')}}</p>
                                <input type="text" placeholder="{{trans('messages.enter_yout_region')}}" name="region" autocomplete="off">
                                <input type="text" placeholder="{{trans('messages.enter_yout_city')}}" name="city" autocomplete="off">
                                <p class="title">{{trans('messages.system_select_delivery')}}<sup>*</sup></p>
                                @foreach($delivery as $item)
                                    <input type="radio" id="delivery{{ $item->id }}" name='delivery_id'
                                           value="{{ $item->id }}" {{$loop->first ? 'checked' : ''}}>
                                    <label for="delivery{{ $item->id }}">{{ $item->name }}</label>
                                @endforeach
                                <p class="title">{{trans('messages.system_select_payment')}}<sup>*</sup></p>
                                @foreach($payment as $item)
                                    <input type="radio" id="payment{{ $item->id }}" name='payment_id'
                                           value="{{ $item->id }}" {{$loop->first ? 'checked' : ''}}>
                                    <label for="payment{{ $item->id }}">{{ $item->name }}</label>
                                @endforeach
                                <textarea placeholder="{{trans('messages.system_show_comment')}}" name="comment"
                                          autocomplete="off"></textarea>
                                {{ Form::submit(trans('messages.make_order'), ['class' => 'make-order']) }}
                                <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), '/') }}"
                                   class="back-to-shop">
                                    <p>{{trans('messages.system_back_to_shop')}}</p>
                                </a>
                            </div>
                        </div>
                    @else
                        <div class="head">
                            <div class="col-md-12">
                                <p>{{trans('messages.system_ordering')}}</p>
                            </div>
                        </div>
                        <div class="tab-body col-md-12">
                            <div class="row">
                                <p class="title">{{trans('messages.system_select_delivery')}}</p>
                                @foreach($delivery as $item)
                                    <input type="radio" id="delivery{{ $item->id }}" name='delivery_id'
                                           value="{{ $item->id }}" {{$loop->first ? 'checked' : ''}}>
                                    <label for="delivery{{ $item->id }}">{{ $item->name }}</label>
                                @endforeach
                                <p class="title">{{trans('messages.system_select_payment')}}</p>
                                @foreach($payment as $item)
                                    <input type="radio" id="payment{{ $item->id }}" name='payment_id'
                                           value="{{ $item->id }}" {{$loop->first ? 'checked' : ''}}>
                                    <label for="payment{{ $item->id }}">{{ $item->name }}</label>
                                @endforeach
                                <textarea placeholder="{{trans('messages.system_show_comment')}}" name="comment"></textarea>
                                {{ Form::submit(trans('messages.make_order'), ['class' => 'make-order']) }}
                                <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), '/') }}"
                                   class="back-to-shop">
                                    <p>{{trans('messages.system_back_to_shop')}}</p>
                                </a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        @else
            <div class="empty-cart">
                <p>{{ trans('messages.empty_cart') }}</p>
            </div>
        @endif
    </div>
    {{ Form::close() }}
@endsection
@section('scripts')
    <script>
        $(function () {
            $('.show-items-in-basket').click(function () {
                $(this).find('span').toggleClass('rotate-angle');
                $('.item-wrapper').toggleClass('item-wrapper-show');
            });

            $(".drop-from-cart").on('click', function () {
                wrapper = $(this).closest('.item-wrapper');
                $.post('/dropFromCart', {
                    _token: "{{ csrf_token() }}",
                    product_id: $(this).data('productid')
                }, function (response) {
                    if (response == false)
                        location.reload();
                    else {
                        wrapper.remove();
                        recalcTotalPrice();
                    }
                });
            });

            Number.prototype.format = function (n, x, s, c) {
                var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
                    num = this.toFixed(Math.max(0, ~~n));

                return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
            };

            // up/down Price
            $('.fa-caret-right, .fa-caret-left').on('click', function () {
                cur_price = $(this).closest('.item-wrapper').find('.item-price > p');
                inputVal = $(this).closest('.item-count').find('input');

                if ($(this).hasClass('fa-caret-right')) {
                    if (parseInt(inputVal.val()) < 99)
                        inputVal.val(parseInt(inputVal.val()) + 1);
                    newPrice = cur_price.html().replace(/[0-9\s?]+/, (inputVal.val() * cur_price.data('price')).format(0, 3, ' '));
                    cur_price.html(newPrice);
                } else {
                    if (parseInt(inputVal.val()) > 1) {
                        inputVal.val(parseInt(inputVal.val()) - 1);
                        newPrice = cur_price.html().replace(/[0-9\s?]+/, (inputVal.val() * cur_price.data('price')).format(0, 3, ' '));
                        cur_price.html(newPrice);
                    }
                }
                recalcTotalPrice();

                return;
            });

            //
            $('.item-count > input').on('keydown keyup', function (e) {
                if (!e.key.match(/[\d]/) && e.keyCode != 8) {
                    this.value = this.value.replace(/[^\d]/, '');
                    if (this.value == '')
                        this.value = 1;
                    return false;
                }

                if (this.value == '')
                    this.value = 1;

                if (this.value > 100)
                    this.value = 99;

                cur_price = $(this).closest('.item-wrapper').find('.item-price > p');
                inputVal = $(this).closest('.item-count').find('input');
                newPrice = cur_price.html().replace(/[0-9\s?]+/, (inputVal.val() * cur_price.data('price')).format(0, 3, ' '));
                cur_price.html(newPrice);
                recalcTotalPrice();
            });

            // recalc TotalPrice
            function recalcTotalPrice() {
                total = 0;
                $('.item-price > p').each(function (i) {
                    val = parseInt($(this).html().replace(/\s/, ''));
                    total += val;
                });
                newTotalPrice = $('.total-cost > p > span').html().replace(/[0-9\s?]+/, total.format(0, 3, ' ') + ' ');
                $('.total-cost > p > span').html(newTotalPrice);
            }

        });
    </script>
@endsection