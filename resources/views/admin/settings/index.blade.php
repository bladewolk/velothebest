@extends('admin.layout')
@section('main')
    <h1>{{ $title }}
        <a href="{{ route('master') }}" class="btn btn-primary" title="Назад" data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>
        <a href="" id="send-form" class="btn btn-success" title="Сохранить" data-toggle="tooltip"><span
                    class="fa fa-floppy-o"></span></a>



        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>
    {!! Form::model($settings, ['route' => ['settings.update', $settings->id], 'class'=>'form-horizontal', 'files' => true]) !!}
    {{ method_field('PATCH') }}

    <div class="form-group">
        {!! Form::label('logotype', 'Логотип', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-9">
            {!! Form::file('logo', array('class' => 'filestyle', 'data-value'=> $settings->logo,
            'data-buttonText' => 'Выбрать файл', 'data-buttonName' => 'btn-primary', 'data-icon' => 'false' ) ) !!}
        </div>
        @if($settings->images()->first())
            {!! Form::label('logotype', 'Текущий логотип', ['class' => 'control-label col-sm-2']) !!}
            <div class="col-sm-9">
                <img src="{{ '/img/'.$settings->images->first()->name . '?w=180&h=110' }}" alt="logotype">
            </div>
        @endif
    </div>

    <div class="form-group">
        {!! Form::label('site_name', 'Название сайта', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-9">
            {!! Form::text('site_name', $settings->site_name, ['class'=>'form-control', 'required']) !!}
        </div>
    </div>
    @for($i = 0; $i < 3; $i++)
        <div class="form-group">
            {!! Form::label('phones[]', 'Телефон', ['class'=>'control-label col-sm-2']) !!}
            <div class="col-sm-9">
                {!! Form::text('phones[]', isset($settings->phones[$i]['tel']) ? $settings->phones[$i]['tel'] : "", ['class'=>'form-control ', 'required']) !!}
            </div>
        </div>
    @endfor
    <div class="form-group">
        {!! Form::label('info', 'Информация в шапке', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-9">
            {!! Form::text('info', $settings->info, ['class'=>'form-control', 'required', 'maxlength' => 255]) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('slider_speed', 'Скорость слайдера (в мс.)', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-9">
            {!! Form::number('slider_speed', $settings->slider_speed, ['class'=>'form-control', 'required', 'min' => 100, 'max' => 60000, 'step' => 10]) !!}
        </div>
    </div>


    <div class="form-group">
        {!! Form::label('phone2', 'Facebook', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-9">
        {!! Form::text('facebook', $settings->facebook, ['class'=>'form-control ', 'required']) !!}
        </div>
    </div>

     <div class="form-group">
        {!! Form::label('phone2', 'Instagram', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-9">
        {!! Form::text('instagram', $settings->instagram, ['class'=>'form-control ', 'required']) !!}
        </div>
    </div>

     <div class="form-group">
        {!! Form::label('phone2', 'Youtube', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-9">
        {!! Form::text('youtube', $settings->youtube, ['class'=>'form-control ', 'required']) !!}
        </div>
    </div>


    {!! Form::close() !!}

    <script>
        $("#send-form").click(function (event) {
            event.preventDefault();
            $('form').submit();
        });
    </script>
@stop

