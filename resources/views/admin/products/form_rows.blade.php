<div class="form-group">
    {{ Form::label('name' . $key, 'Название продукта', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-10">
        {{ Form::text("name[$key]", isset($data->name) ? $data->name : "", ['class'=>'form-control'] ) }}
    </div>
</div>


<div class="form-group">
    {{ Form::label('label_text', 'Текст лейбы', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-4">
        {{ Form::text("label_text[$key]", isset($data->label_text) ? $data->label_text : "", ['class'=>'form-control'] ) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('description' . $key, 'Описание', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-10">
        {{ Form::textarea("description[$key]", isset($data->description) ? $data->description : "", ['class'=>'form-control editor'] ) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('meta_title' . $key, 'Meta Title', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-10">
        {{ Form::text("meta_title[$key]", isset($data->meta_title) ? $data->meta_title : "", ['class'=>'form-control', 'id' => 'inputMetaTitle'] ) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('meta_keywords' . $key, 'Meta Keys', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-10">
        {{ Form::text("meta_keywords[$key]", isset($data->meta_keywords) ? $data->meta_keywords : "", ['class'=>'form-control', 'id' => 'inputMetaTitle'] ) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('meta_description' . $key, 'Meta Description', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-10">
        {{ Form::text("meta_description[$key]", isset($data->meta_description) ? $data->meta_description : "", ['class'=>'form-control'] ) }}
    </div>
</div>
