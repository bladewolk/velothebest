@extends('admin.layout')
@section('styles')
    <link rel="stylesheet" href="{{ asset('admin/css/sol.css') }}">
    <script src="{{ asset('admin/js/sol.js') }}"></script>
@stop
@section('main')

    <h1>{{ $title }}
        <a href="{{ route('products.index') }}" class="btn btn-primary" title="Назад"
           data-toggle="tooltip"><span
                    class="fa fa-reply"></span></a>

        <button id="send-form" class="btn btn-success" title="Сохранить" data-toggle="tooltip"><i class="fa fa-floppy-o"
                                                                                                  aria-hidden="true"></i>
        </button>

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>

    {{ Form::model($item, ['route'=> ['products.update', $item->id], 'class' => 'form-horizontal', 'role' => 'form', 'files' => true] ) }}
    {{ method_field('PATCH') }}
    @include('admin.products.form')
    {{ Form::close() }}


    <script src="/admin/js/multiselect.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('#pre-selected-options').multiselect();
        });
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('#search').multiselect({
                search: {
                    left: '<input type="text" class="form-control" placeholder="Search..." />',
                    right: '<input type="text" class="form-control" placeholder="Search..." />',
                },
                fireSearch: function(value) {
                    return value.length > 3;
                }
            });
        });
    </script>

    <script>
        var skip = 1;
        var flag = true;

        $('#category_search').on('change', function(event){
            skip = 0;
            flag = false;
            $('#search').find('option').remove();
            
                $.get('/master/products_management/products/{{ $item->id }}/edit?skip='+skip+'&id={{ $item->id }}&cat_id='+$(this).val(), function (response) {
                    if (response != 0){
                        html = '';
                        for (key in response) {
                            html += '<option value="' + response[key].id + '">' + response[key].localization[0].name + '</option>';
                        }
                        $('#search').append(html);
                        ++skip;
                        flag = true;
                    }
                });
        });

        $('#search').scroll(function () {
            if (this.scrollHeight - this.scrollTop < 900 && flag == true) {
                flag = false;
                $.get('/master/products_management/products/{{ $item->id }}/edit?skip='+skip+'&id={{ $item->id }}&cat_id='+$("#category_search").val(), function (response) {
                    html = '';
                    for (key in response) {
                        html += '<option value="' + response[key].id + '">' + response[key].localization[0].name + '</option>';
                    }
                    $('#search').append(html);
                    ++skip;
                    flag = true;
                });
            }

        });


        $('#my-select').searchableOptionList();
        $('#my-select2').searchableOptionList({
            multiple: false,
        });

        $('#send-form').click(function () {
            $('.form-horizontal').submit();
        });

        $(".add-option").click(function () {
            cloned = $(this).closest('.tab-pane').find('.options:first').clone();
            cloned.find('input').val("");
            cloned.css({
                display: "block"
            });
            console.error(cloned);
            cloned.insertAfter($(this).closest('.tab-pane').find('.form-group:last'));
        });

        meta_title_touched = false;
        url_touched = false;

        $('input[name="slug"]').change(function () {
            url_touched = true;
        });

        $('input[name="name[ru]"]').keyup(function () {
            if (!url_touched)
                $('input[name="slug"]').val(generate_url());

            if (!meta_title_touched)
                $('input[name="meta_title[ru]"]').val($('input[name="name"]').val());
        });

        $('input[name="meta_title[ru]"]').change(function () {
            meta_title_touched = true;
        });

        function generate_url() {
            url = $('input[name="name[ru]"]').val();
            url = url.replace(/[\s]+/gi, '-');
            url = translit(url);
            url = url.replace(/[^0-9a-z_\-]+/gi, '').toLowerCase();
            return url;
        }

        function translit(str) {
            var ru = ("А-а-Б-б-В-в-Ґ-ґ-Г-г-Д-д-Е-е-Ё-ё-Є-є-Ж-ж-З-з-И-и-І-і-Ї-ї-Й-й-К-к-Л-л-М-м-Н-н-О-о-П-п-Р-р-С-с-Т-т-У-у-Ф-ф-Х-х-Ц-ц-Ч-ч-Ш-ш-Щ-щ-Ъ-ъ-Ы-ы-Ь-ь-Э-э-Ю-ю-Я-я").split("-")
            var en = ("A-a-B-b-V-v-G-g-G-g-D-d-E-e-E-e-E-e-ZH-zh-Z-z-I-i-I-i-I-i-J-j-K-k-L-l-M-m-N-n-O-o-P-p-R-r-S-s-T-t-U-u-F-f-H-h-TS-ts-CH-ch-SH-sh-SCH-sch-'-'-Y-y-'-'-E-e-YU-yu-YA-ya").split("-")
            var res = '';
            for (var i = 0, l = str.length; i < l; i++) {
                var s = str.charAt(i), n = ru.indexOf(s);
                if (n >= 0) {
                    res += en[n];
                }
                else {
                    res += s;
                }
            }
            return res;
        }
    </script>
@stop