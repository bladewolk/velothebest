@extends('admin.layout')
@section('main')
    <h1>
        <span class="fa fa-newspaper-o"></span>
        {{ $title }}
        <a href="{{ route('product_managment') }}" class="btn btn-primary" title="Назад" data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>
        <a href="{{ route('products.create') }}" id="send-form" class="btn btn-success"
           title="Cоздать запись"
           data-toggle="tooltip"><span
                    class="fa fa-plus"></span></a>

        <img class="leopard" src="http://www.elnit.org/images/irbis.gif" alt="" style="position: absolute;
    top: 10%;
    z-index: -1;
    left: -10%;">

        @if($items->total() > 0)
            <span class="btn btn-warning" title="Выбрать все" data-toggle="tooltip"
                  id="check-all"><i class="glyphicon glyphicon-ok"
                                    aria-hidden="true"></i></span>
            <button class="delete-selected btn btn-danger"
                    title="Удалить выбранные" data-toggle="tooltip">
                <span class="glyphicon glyphicon-trash"></span></button>

            <span class="label label-primary" style="font-size: 14px;">
			{{ $items->total() }}
                {{ Lang::choice('продукт|продукта|продуктов', $items->total(), [], 'ru') }}
        </span>
        @endif

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>

    <div class="col-sm-3">
        {{ Form::select('categories', $categories, request()->get('cat_id'), ['class' => 'form-control categorie']) }}
    </div>

    @if ($items->total() > 0)

        {!! Form::open(['route' => ['products.destroy', 'destroy']]) !!}
        {{ method_field('DELETE') }}
        <table class="table">
            <tr>
                <th style="width: 25%;">Изображение
                </th>
                <th>Код товара</th>
                <th>Статус</th>
                <th>
                    Название
                </th>
                <th>Категория
                </th>
                <th>Количество
                </th>
                <th>Цена
                </th>
                <th>Скидка
                </th>
                <th>Видимость</th>
                <th>Группа</th>
                <th style="text-align: right;">Управление</th>
            </tr>
            @foreach ($items as $item)
                <tr>
                    <td style="vertical-align: middle">
                        {{ Form::checkbox('check[]', $item->id, false, ['id' => 'check'.$item->id, 'class' => 'custom-check']) }}
                        {{ Form::label('check'.$item->id, '1', ['class' => 'custom-label-for-check']) }}
                        @if ($item->images->first())
                            <img src="{{ '/img/'.$item->images->first()->name .'?w=261&h=220' }}">
                        @endif
                        {{--          <a class="black_link" title="Редактировать" data-toggle="tooltip"
                                    href="{{ route('products.edit', $item->id) }}">
                                     {{ $item->name }}</a> --}}
                    </td>
                    <td>
                        <span class="label label-primary">
                            {{ $item->code }}
                        </span>
                    </td>
                    <td>
                        @if ($item->in_stock < 1 || $item->status == 0)
                            <label class="label label-danger">Нет в наличии</label>
                        @elseif($item->status == 1)
                            <label class="label label-success">В наличии</label>
                        @elseif($item->status == 2)
                            <label class="label label-primary">Под заказ</label>
                        @endif
                    </td>
                    <td>
                        {{ $item->name }}
                    </td>
                    <td>
                        {{ $item->cat_name }}
                    </td>
                    <td>
                        {{ $item->in_stock }}
                    </td>
                    <td>
                        {{ $item->price }}
                    </td>
                    <td>
                        @if ($item->price_discount)
                            <span class="label label-primary">{{ $item->price - $item->price_discount }}
                                $</span>
                        @endif
                    </td>
                    <td>
                        <button class="btn {{ $item->visible ? 'btn-success' : 'btn-danger'}} fa fa-eye"
                                title="Виден/Скрыт" data-toggle="tooltip" data-visible="{{ $item->visible }}"
                                data-eye={{ $item->id }}></button>
                    </td>
                    <td>
                        @if (isset($item->group))
                            <a href="{{ route('groups.edit', $item->group['id']) }}">
                                <span class="label label-primary">{{ $item->group['name'] }}</span>
                            </a>
                        @endif
                    </td>
                    <td style="text-align: right;">
                        <div class="group">
                            <a href="{{ route('products.edit', ['id' => $item->id]) }}" class="btn btn-default"
                               title="Редактировать" data-toggle="tooltip">
                                <span class="fa fa-pencil-square-o"></span>
                            </a>
                            <a href="{{ route('products.show', $item->id) }}" class="btn btn-default"
                               title="Создать копию" data-toggle="tooltip">
                                <span class="fa fa-clone"></span>
                            </a>
                            <button class="btn btn-default delete-this-id" title="Удалить запись" data-toggle="tooltip">
                                <span class="fa fa-times"></span>
                            </button>
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ Form::close() }}

    @endif
    {{ $items->appends(Request::except('page'))->links() }}

    <script>
        $(function () {
            $('.categorie').on('change', function () {
                location.href = location.pathname + '?cat_id=' + $('.categorie option:selected').val();
            });


            var flag = false;
            $(".delete-selected, .unconfirm-delete, .delete-this-id").click(function (event) {
                event.preventDefault();
                flag = !flag;
                if (flag)
                    move = "50%";
                else move = "-50%";

                $('.confirm-message').stop().animate({
                    top: move
                }, 300);
            });
            $(".confirm-delete").click(function () {
                $('form').submit();
            });
            $(".unconfirm-delete").click(function () {
                $('input[type=checkbox]:checked').prop('checked', false);
            });
            $(".delete-this-id").click(function () {
                $(this).closest('tr').find('input:checkbox').prop('checked', true);
            });
            $("#check-all").on('click', function () {
                $('input[type="checkbox"][name*="check"]').prop('checked', $('input[type="checkbox"][name*="check"]:not(:checked)').length > 0);
            });

            $('.fa-eye').on('click', function (event) {
                event.preventDefault();
                button = $(this);
                $.post('{{ route('products.update', ['id' => 1]) }}', {
                    _method: "PATCH",
                    _token: "{{ csrf_token() }}",
                    visible: $(this).data('visible'),
                    id: $(this).data("eye")
                }, function (response) {
                    console.error(response);
                    $('[data-eye="' + response + '"]').toggleClass('btn-success btn-danger');
                    // $('.fa-eye').data("id", response).toggleClass('btn-success btn-danger');
                    // button.toggleClass('btn-success btn-danger');
                });
            });

        })
    </script>
@endsection