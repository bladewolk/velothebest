<div class="form-group">
    {!! Form::label('image', 'Изображение продукта', array('class'=>'col-sm-2 control-label') ) !!}
    <div class="col-sm-10">
        {!! Form::file('image',array('class' => 'filestyle', 'data-value'=> '',
        'data-buttonText' => 'Выберите файлы', 'data-buttonName' => 'btn-primary',
        'data-icon' => 'true' , 'multiple' => 'true') ) !!}
        <span>Если изображение не соответствует размеру 255x96, оно будет автоматически обрезано до данной величины</span>
    </div>
    @if ($item->images()->first())
        <div class="col-sm-10 col-sm-offset-2">
            <img src="{{ '/img/'.$item->images->first()->name . '?w=200&h=200' }}" alt="{{ $item->name }}">
        </div>
    @endif
</div>

<div class="form-group options">
    {{ Form::label('categories', 'Категория', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-4">
        {{ Form::select("cat_id", $categories, $item->cat_id, ['class' => 'form-control']) }}
    </div>

    {{ Form::label('status', 'Статус товара', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-4">
        {{ Form::select("status", $status, $item->status, ['class' => 'form-control form-control-succes']) }}
    </div>
</div>

<div class="form-group options">
    {{ Form::label('color_id', 'Цвет', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-4">
        {{ Form::select("color_id", $colors,$item->color_id, ['class' => 'form-control']) }}
    </div>

    {{ Form::label('brand_id', 'Бренд', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-4">
        {{ Form::select("brand_id", $brands, $item->brand_id, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group options">
    {{ Form::label('main_feature', 'Основная характеристика', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-4">
        {{ Form::select("main_feature", $options, $item->main_feature, ['class' => 'form-control']) }}
    </div>

    {{ Form::label('main_feature_text', 'Значение основной характеристики', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-4">
        {{ Form::text("main_feature_text", $item->main_feature_text, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('in_stock', 'Количество на складе', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-4">
        {{ Form::number("in_stock", $item->in_stock, ['class'=>'form-control','min' => 0, 'step' => 1] ) }}
    </div>
    {{ Form::label('code', 'Код продукта', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-4">
        {{ Form::text("code", $item->code, ['class'=>'form-control'] ) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('price', 'Цена (USD)', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-4">
        {{ Form::text("price", $item->price, ['class'=>'form-control'] ) }}
    </div>
    {{ Form::label('price_discount', 'Цена со скидкой (USD)', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-4">
        {{ Form::text("price_discount", $item->price_discount, ['class'=>'form-control'] ) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('video', 'Видео', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-4">
        {{ Form::text("video", $item->video, ['class'=>'form-control'] ) }}
    </div>

    {{ Form::label('label', 'Лейба', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-4">
        {{ Form::select("label_id", $labels, null, ['class' => 'form-control']) }}
    </div>
</div>
<hr>

<?php $count = 0; ?>
<ul class="nav nav-tabs" id="myTab">
    @foreach(LaravelLocalization::getSupportedLocales() as $key => $value)
        <li @if($count == 0) class="active" @endif data-toggle="tooltip" title="{{ $value['native']}}">
            <a href="#tab_{{ $key }}" data-toggle="tab">&nbsp;&nbsp;&nbsp;&nbsp;<span
                        class="flag flag-{{ $key }}"></span>
            </a>
        </li>
        <?php $count++ ?>
    @endforeach
    <li data-toggle="tooltip" title="Дополнительные картинки товара">
        <a href="#tab_pictures" data-toggle="tab">
            Изображения товара
        </a>
    </li>

    <li data-toggle="tooltip" title="Параметры фильтрации">
        <a href="#tab_filters" data-toggle="tab">
            Параметры фильтрации
        </a>
    </li>

    <li data-toggle="tooltip" title="Сопутствующие товары">
        <a href="#tab_related" data-toggle="tab">
            Сопутствующие товары
        </a>
    </li>

    <li data-toggle="tooltip" title="Характеристики товара">
        <a href="#tab_characteristics" data-toggle="tab">
            Характеристики товара
        </a>
    </li>

    <li>
        <div class="col-sm-12">
            {{ Form::text('slug', old('slug') ? old('slug') : $item->slug, [
            'class'=>'form-control col-sm-11',
            'data-toggle' => 'tooltip',
            'title' => 'Адрес страницы',
            ]) }}
        </div>
    </li>
</ul>
<?php $count = 0; ?>


<div class="tab-content">
    @foreach(LaravelLocalization::getSupportedLocales() as $key => $value)
        @forelse($item->localization as $data)
            @if ($data['locale'] == $key)
                <div class="tab-pane @if ($count == 0) active @endif" id="tab_{{ $key }}">
                    <div class="tab-content">
                        @include('admin.products.form_rows')
                    </div>
                </div>
            @endif
        @empty
            <div class="tab-pane @if ($count == 0) active @endif" id="tab_{{ $key }}">
                <div class="tab-content">
                    @include('admin.products.form_rows')
                </div>
            </div>
        @endforelse
        <?php $count++ ?>
    @endforeach

    <div class="tab-pane" id="tab_pictures">
        <hr>
        <div class="tab-content">

            <div class="form-group">
                {!! Form::label('images', 'Дополнительные изображения продукта', array('class'=>'col-sm-2 control-label') ) !!}
                <div class="col-sm-10">
                    {!! Form::file('images[]',array('class' => 'filestyle', 'data-value'=> '',
                    'data-buttonText' => 'Выберите файлы', 'data-buttonName' => 'btn-primary',
                    'data-icon' => 'true' , 'multiple' => 'true') ) !!}
                    <span>Если изображение не соответствует размеру 255x96, оно будет автоматически обрезано до данной величины</span>
                </div>
            </div>

            @if ($item->images->isEmpty() == false)
                @foreach($item->images->slice(1) as $image)
                    <div class="col-sm-6 col-md-2">
                        <div class="thumbnail">
                            <img style="width: 100%; margin: 0 auto; display: block"
                                 alt="photo"
                                 src="{{ isset($image->name) ? '/img/'.$image->name . '?w=240&h=240' : asset('images/noimg.jpg')}}">
                            <div class="caption">
                                <div class="btn-group">
                                    {{ Form::checkbox('deleteImages[]', $image->id) }}
                                    {{ Form::label('', 'Удалить', ['class' => 'label label-danger'])}}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>

    </div>

    <div class="tab-pane" id="tab_filters">
        <hr>
        <div class="tab-content">

            <div class="form-group">
                {{ Form::label('variant_name', 'Параметры фильтрации', ['class'=>'col-sm-2 control-label'] ) }}
                <div class="col-sm-10">
                    <select id="my-select" name="product_features[]" multiple="multiple">
                        @foreach($filters as $filter)
                            <optgroup label="{{ $filter->name }}">
                                @foreach($filter->variants as $variant)
                                    <option value="{{ $variant->id }}" {{ in_array($variant->id,$product_filters) ? 'selected' : '' }}>
                                        {{ $variant->name }}
                                    </option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    </select>
                </div>
            </div>

        </div>
    </div>
    <div class="tab-pane" id="tab_related">
        <hr>
        <div class="tab-content">
            <div class="row">
                <div class="col-xs-5">
                    {{ Form::select("category_search", $categories, $item->cat_id, ['class' => 'form-control', 'id' => 'category_search']) }}
                     <select name="soput[]" id="search" class="form-control" size="8" multiple="multiple">
                        @foreach($soput as $product)
                            <option value="{{$product->id}}">{{$product->localization->first()->name}}</option>
                        @endforeach
                    </select>
                </div>
                 <div class="col-xs-2">
                    <button type="button" id="search_rightAll" class="btn btn-block btn-warning"><i class="glyphicon glyphicon-forward"></i></button>
                    <button type="button" id="search_rightSelected" class="btn btn-block btn-primary"><i class="glyphicon glyphicon-chevron-right"></i></button>
                    <button type="button" id="search_leftSelected" class="btn btn-block btn-primary"><i class="glyphicon glyphicon-chevron-left"></i></button>
                    <button type="button" id="search_leftAll" class="btn btn-block btn-warning"><i class="glyphicon glyphicon-backward"></i></button>
                </div>
                <div class="col-xs-5">
                 <select name="soputSelected[]" id="search_to" class="form-control" size="8" multiple="multiple">
                    @foreach($soputSelected as $product)
                        <option value="{{$product->id}}">{{$product->localization->first()->name}}</option>
                    @endforeach
                 </select>
                </div>
            </div>

        </div>
    </div>

    <div class="tab-pane" id="tab_characteristics">
        <hr>
        <div class="tab-content">
        <table class="table">
            <tr>
                <th>Название</th>
                <th><span class="label label-primary">RU</span></th>
                <th><span class="label label-primary">UA</span></th>
            </tr>
            @foreach($options as $id => $name)
                <tr>
                    <td>
                        {{$name}}
                    </td>
                    @if (isset($item))
                        <td>
                            <input type="text" class="form-control" placeholder="..." name="option[{{$id}}][ru]" 
                            @if ($item->characteristicks->where('option_id', $id)->where('locale', 'ru')->first())
                            value="{{ $item->characteristicks->where('option_id', $id)->where('locale', 'ru')->first()->value }}"
                            @endif
                            >
                        </td>
                        <td>
                            <input type="text" class="form-control" placeholder="..." name="option[{{$id}}][ua]" 
                            @if ($item->characteristicks->where('option_id', $id)->where('locale', 'ua')->first())
                            value="{{ $item->characteristicks->where('option_id', $id)->where('locale', 'ua')->first()->value }}" @endif >
                        </td>
                    @else
                        <td>
                            <input type="text" class="form-control" placeholder="Значение..." name="option[{{$id}}][ru]" value="">
                        </td>
                        <td>
                            <input type="text" class="form-control" placeholder="Значение..." name="option[{{$id}}][ua]" value="">
                        </td>
                    @endif
                 
                </tr>
            @endforeach
        </table>
        </div>
    </div>

</div>

