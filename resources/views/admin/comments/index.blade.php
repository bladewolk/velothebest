@extends('admin.layout')
@section('main')
    <h1>
        <span class="fa fa-newspaper-o"></span>
        {{ $title }}
        <a href="{{ route('master') }}" class="btn btn-primary" title="Назад" data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>
       
        @if($items->total() > 0)
            <span class="btn btn-warning" title="Выбрать все" data-toggle="tooltip"
                  id="check-all"><i class="glyphicon glyphicon-ok"
                                    aria-hidden="true"></i></span>
            <button class="delete-selected btn btn-danger"
                    title="Удалить выбранные" data-toggle="tooltip">
                <span class="glyphicon glyphicon-trash"></span></button>

            <span class="label label-primary" style="font-size: 14px;">
			     {{ $items->total() }}
                {{ Lang::choice('Комментарий|Комментария|Комментариев', $items->total(), [], 'ru') }}
            </span>
        @endif
        
        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>

    @if ($items->total() > 0)

        {!! Form::open(['route' => ['comments.destroy', 'destroy']]) !!}
        {{ method_field('DELETE') }}
        <table class="table">
            <tr>
                <th>№</th>
                <th>Комментарий к продукту</th>
                <th>От</th>
                <th style="width: 50%;">Комментарий</th>
                <th>Рейтинг</th>
                <th style="text-align: right;">Управление</th>
            </tr>
            @foreach ($items as $item)
                <tr>
                    <td>
                        <span style="font-weight: bolder; font-size: 16px;">{{ $item->id }}</span> 
                         {{ Form::checkbox('check[]', $item->id, false, ['id' => 'check'.$item->id, 'class' => 'custom-check']) }}
                        {{ Form::label('check'.$item->id, '1', ['class' => 'custom-label-for-check']) }}
                    </td>
                    <td><a href="/catalog/{{ $item->product->slug }}" target="_blink">{{ $item->product->slug }}</a></td>
                    <td>{{ $item->email }}</td>
                    <td style="vertical-align: middle">
                       {{ $item->comment }}
                    </td>
                    <td>
                        <span class="label label-warning">
                            {{ $item->rating }}
                        </span>
                        <i class="fa fa-star"></i>    
                    </td>
                    <td style="text-align: right;">
                            <button class="{{ $item->status == 1 ? 'btn btn-success' : 'btn btn-primary'}} fa fa-check resolved" title="Отметить как завершенный" data-toggle="tooltip" data-id="{{ $item->id }}"></button>
                            
                            <span class="delete-this-id btn btn-default" title="Удалить"
                                  data-toggle="tooltip"><span
                                        class="glyphicon glyphicon-remove"></span>
                            </span>
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ Form::close() }}

    @endif

    {{ $items->links() }}

    <script>
        $(function () {
            var flag = false;
            $(".delete-selected, .unconfirm-delete, .delete-this-id").click(function () {
                flag = !flag;
                if (flag)
                    move = "50%";
                else move = "-50%";

                $('.confirm-message').stop().animate({
                    top: move
                }, 300);
            });
            $(".confirm-delete").click(function () {
                $('form').submit();
            });
            $(".unconfirm-delete").click(function () {
                $('input[type=checkbox]:checked').prop('checked', false);
            });
            $(".delete-this-id").click(function () {
                $(this).closest('tr').find('input:checkbox').prop('checked', true);
            });
            $("#check-all").on('click', function () {
                $('input[type="checkbox"][name*="check"]').prop('checked', $('input[type="checkbox"][name*="check"]:not(:checked)').length > 0);
            });

            $('.resolved').on('click', function(event){
                event.preventDefault();
                but = $(this);
                $.post("/master/comments/"+$(this).data('id'), {
                    _token: "{{ csrf_token() }}",
                    _method: "PATCH",
                }, function (res){
                    $("button[data-id='" + res +"']").toggleClass('btn-success btn-primary');
                })
            });
        })
    </script>
@endsection