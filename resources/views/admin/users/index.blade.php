@extends('admin.layout')
@section('main')
    <h3>
        <span class="fa fa-newspaper-o"></span>
        {{ $title }}
        <a href="{{ route('master') }}" class="btn btn-primary" title="Назад" data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>

        @if($items->count() > 0)
            {{--<span class="btn btn-warning" title="Выбрать все" data-toggle="tooltip"--}}
            {{--id="check-all"><i class="glyphicon glyphicon-ok"--}}
            {{--aria-hidden="true"></i></span>--}}
            <a href="{{ route('users.create') }}" class="btn btn-success"  title="Создать пользователя" data-toggle="tooltip">
                <span class="fa fa-user-plus"   ></span>
            </a>
            <button class="delete-selected btn btn-danger"
            title="Удалить выбранные" data-toggle="tooltip">
            <span class="glyphicon glyphicon-trash"></span></button>
            <span class="label label-primary">
            {{ $items->count() }}
                {{ Lang::choice('пользователь|пользователя|пользователей', $items->count(), [], 'ru') }}
        </span>
        @endif
    </h3><br>
    @if ($items->count() > 0)
        {!! Form::open(['route' => ['users.destroy', 'destroy']]) !!}
        {{ method_field('DELETE') }}
        <table class="table">
            <tr>
                <th>Имя</th>
                <th>Почта</th>
                <th>Телефон</th>
                <th>Область</th>
                <th>Город</th>
                <th>Адрес</th>
                <th>Права</th>
                <th>Дата регистрации</th>
                <th>Управление</th>
            </tr>
            @foreach ($items as $item)
                <tr>
                    <td>
                        {{ Form::checkbox('check[]', $item->id, false, ['id' => 'check'.$item->id, 'class' => 'custom-check']) }}
                        {{ Form::label('check'.$item->id, '1', ['class' => 'custom-label-for-check']) }}
                        {{ $item->name }}
                    </td>
                    <td>
                        {{ $item->email }}
                    </td>
                    <td>
                        {{ $item->phone }}
                    </td>
                    <td>
                        {{ $item->region }}
                    </td>
                    <td>
                        {{ $item->city }}
                    </td>
                    <td>
                        {{ $item->address }}
                    </td>
                    <td>
                        @if ($item->permission == 'root')
                            <span class="label label-success">Admin</span>
                        @else
                            <span class="label label-primary">Пользователь сайта</span>
                        @endif
                    </td>
                    <td>
                        {{ $item->created_at }}
                    </td>
                    <td style="text-align: right;">
                        <a title="Редактировать" href="{{ route('users.edit', ['id' => $item->id]) }}"
                        data-toggle="tooltip"
                        class="btn btn-primary"><span class="fa fa-pencil"></span></a>
                        <span class="delete-this-id btn btn-default" title="Удалить"
                              data-toggle="tooltip"><span
                                    class="glyphicon glyphicon-remove"></span>
                            </span>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ Form::close() }}

    @endif
    {{--    {{ $items->links() }}--}}

    <script>
        $(function () {

            var flag = false;
            $(".delete-selected, .unconfirm-delete, .delete-this-id").click(function () {
                flag = !flag;
                if (flag)
                    move = "50%";
                else move = "-50%";

                $('.confirm-message').stop().animate({
                    top: move
                }, 300);
            });
            $(".confirm-delete").click(function () {
                $('form').submit();
            });
            $(".unconfirm-delete").click(function () {
                $('input[type=checkbox]:checked').prop('checked', false);
            });
            $(".delete-this-id").click(function () {
                $(this).closest('tr').find('input:checkbox').prop('checked', true);
            });
            $("#check-all").on('click', function () {
                $('input[type="checkbox"][name*="check"]').prop('checked', $('input[type="checkbox"][name*="check"]:not(:checked)').length > 0);
            });
        })
    </script>
@endsection