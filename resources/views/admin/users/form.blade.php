<div class="form-group">
    {{ Form::label('name', 'Имя пользователя', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-10">
        {{ Form::text("name", $user->name, ['class'=>'form-control'] ) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('email', 'Email', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-10">
        {{ Form::email("email", $user->email, ['class'=>'form-control'] ) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('phone', 'Телефон', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-10">
        {{ Form::tel("phone", $user->phone, ['class'=>'form-control'] ) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('region', 'Область', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-10">
        {{ Form::text("region", $user->region, ['class'=>'form-control'] ) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('city', 'Город', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-10">
        {{ Form::text("city", $user->city, ['class'=>'form-control'] ) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('address', 'Адрес', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-10">
        {{ Form::text("address", $user->address, ['class'=>'form-control'] ) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('permission', 'Права', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-10">
        <span class="label label-success">Admin</span>
        {{ Form::radio('permission', 'root', $user->permission) }}
        <span class="label label-primary">Пользователь</span>
        {{ Form::radio('permission', 'user', $user->permission) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('password', 'Пароль', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-10">
        {{ Form::password("password", ['class'=>'form-control', 'required'] ) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('password_confirmation', 'Подтвердите пароль', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-10">
        {{ Form::password("password_confirmation", ['class'=>'form-control', 'required' => true] ) }}
    </div>
</div>

