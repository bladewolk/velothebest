@extends('admin.layout')

@section('main')

    <h1>{{ $title }}
        <a href="{{ route('users.index') }}" class="btn btn-primary" title="Назад"
           data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>

        <button id="send-form" class="btn btn-success" title="Сохранить" data-toggle="tooltip"><i class="fa fa-floppy-o"
                                                                                                  aria-hidden="true"></i>
        </button>

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>

    {{ Form::model($user, ['route'=> ['users.update', $user->id], 'class' => 'form-horizontal', 'role' => 'form', 'files' => true] ) }}
    {{ method_field('PATCH') }}
    @include('admin.users.form')
    {{ Form::close() }}


    <script>
        $('#send-form').click(function () {
            $('form').submit();
        });
    </script>
@stop