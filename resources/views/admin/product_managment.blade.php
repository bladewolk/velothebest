@extends('admin.layout')

@section('main')

    <div class="row pages">

        <div class="col-sm-12">
            <h1>Управление Товаром</h1>
        </div>

        <div class="col-md-2 col-sm-3 col-xs-4 nav-block">
            <a href="{{ route('categories.index') }}">
                <i class="fa fa-thumb-tack fa-4x"></i>
                <p>Категории</p>
            </a>
        </div>

        <div class="col-md-2 col-sm-3 col-xs-4 nav-block">
            <a href="{{ route('features.index') }}">
                <i class="fa fa-leaf fa-4x"></i>
                <p>Параметры фильтрации</p>
            </a>
        </div>

        <div class="col-md-2 col-sm-3 col-xs-4 nav-block">
            <a href="{{ route('options.index') }}">
                <i class="fa fa-bolt fa-4x"></i>
                <p>Характеристики продуктов</p>
            </a>
        </div>

        <div class="col-md-2 col-sm-3 col-xs-4 nav-block">
            <a href="{{ route('brands.index') }}">
                <i class="fa fa-gg-circle fa-4x"></i>
                <p>Бренды</p>
            </a>
        </div>

        <div class="col-md-2 col-sm-3 col-xs-4 nav-block">
            <a href="{{ route('labels.index') }}">
                <i class="fa fa-bookmark-o fa-4x"></i>
                <p>Лейбы</p>
            </a>
        </div>

        <div class="col-md-2 col-sm-3 col-xs-4 nav-block">
            <a href="{{ route('colors.index') }}">
                <i class="fa fa-lightbulb-o fa-4x"></i>
                <p>Цвета продуктов</p>
            </a>
        </div>

        <div class="col-md-2 col-sm-3 col-xs-4 nav-block">
            <a href="{{ route('products.index') }}">
                <i class="fa fa-cubes fa-4x"></i>
                <p>Продукты</p>
            </a>
        </div>

        <div class="col-md-2 col-sm-3 col-xs-4 nav-block">
            <a href="{{ route('groups.index') }}">
                <i class="fa fa-cubes fa-4x"></i>
                <p>Группы продуктов</p>
            </a>
        </div>

        <div class="col-md-2 col-sm-3 col-xs-4 nav-block">
            <a href="{{ route('delivery.index') }}">
                <i class="fa fa-truck fa-4x"></i>
                <p>Доставка</p>
            </a>
        </div>

        <div class="col-md-2 col-sm-3 col-xs-4 nav-block">
            <a href="{{ route('payment.index') }}">
                <i class="fa fa-money fa-4x"></i>
                <p>Оплата</p>
            </a>
        </div>

    </div>

@stop