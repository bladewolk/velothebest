<?php $count = 0; ?>
<ul class="nav nav-tabs" id="myTab">
    @foreach(LaravelLocalization::getSupportedLocales() as $key => $value)
        <li @if($count == 0) class="active" @endif data-toggle="tooltip" title="{{ $value['native']}}">
            <a href="#tab_{{ $key }}" data-toggle="tab">&nbsp;&nbsp;&nbsp;&nbsp;<span
                        class="flag flag-{{ $key }}"></span>
            </a>
        </li>
        <?php $count++ ?>
    @endforeach
    <li data-toggle="tooltip" title="Параметры фильтрации">
        <a href="#tab_filters" data-toggle="tab">
            Фильтры
        </a>
    </li>
    <li>
        <div class="col-sm-12">
            {{ Form::text('slug', old('slug') ? old('slug') : $item->slug, [
            'class'=>'form-control',
            'data-toggle' => 'tooltip',
            'title' => 'Адрес страницы'
            ]) }}
        </div>
    </li>
</ul>
<?php $count = 0; ?>
<div class="tab-content">
    @foreach(LaravelLocalization::getSupportedLocales() as $key => $value)
        @forelse($item->localization as $data)
            @if ($data['locale'] == $key)
                <div class="tab-pane @if ($count == 0) active @endif" id="tab_{{ $key }}">
                    <div class="tab-content">

                        <div class="form-group">
                            {{ Form::label('name' . $key, 'Название', ['class'=>'col-sm-2 control-label'] ) }}
                            <div class="col-sm-10">
                                {{ Form::text("name[$key]", $data->name, ['class'=>'form-control'] ) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('body' . $key, 'Полный текст страницы', ['class'=>'col-sm-2 control-label'] ) }}
                            <div class="col-sm-10">
                                {{ Form::textarea("body[$key]", $data->body, ['class'=>'form-control editor'] ) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('meta_title' . $key, 'Meta Title', ['class'=>'col-sm-2 control-label'] ) }}
                            <div class="col-sm-10">
                                {{ Form::text("meta_title[$key]", $data->meta_title, ['class'=>'form-control', 'id' => 'inputMetaTitle'] ) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('meta_keywords' . $key, 'Meta Keys', ['class'=>'col-sm-2 control-label'] ) }}
                            <div class="col-sm-10">
                                {{ Form::text("meta_keywords[$key]", $data->meta_keywords, ['class'=>'form-control', 'id' => 'inputMetaTitle'] ) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('meta_description' . $key, 'Meta Description', ['class'=>'col-sm-2 control-label'] ) }}
                            <div class="col-sm-10">
                                {{ Form::text("meta_description[$key]", $data->meta_description, ['class'=>'form-control'] ) }}
                            </div>
                        </div>

                    </div>
                </div>
            @endif
        @empty
            <div class="tab-pane @if ($count == 0) active @endif" id="tab_{{ $key }}">
                <div class="tab-content">

                    <div class="form-group">
                        {{ Form::label('name' . $key, 'Название', ['class'=>'col-sm-2 control-label'] ) }}
                        <div class="col-sm-10">
                            {{ Form::text("name[$key]", '', ['class'=>'form-control'] ) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('body' . $key, 'Полный текст страницы', ['class'=>'col-sm-2 control-label'] ) }}
                        <div class="col-sm-10">
                            {{ Form::textarea("body[$key]", '', ['class'=>'form-control editor'] ) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('meta_title' . $key, 'Meta Title', ['class'=>'col-sm-2 control-label'] ) }}
                        <div class="col-sm-10">
                            {{ Form::text("meta_title[$key]", '', ['class'=>'form-control', 'id' => 'inputMetaTitle'] ) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('meta_keywords' . $key, 'Meta Keys', ['class'=>'col-sm-2 control-label'] ) }}
                        <div class="col-sm-10">
                            {{ Form::text("meta_keywords[$key]", '', ['class'=>'form-control', 'id' => 'inputMetaTitle'] ) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('meta_description' . $key, 'Meta Description', ['class'=>'col-sm-2 control-label'] ) }}
                        <div class="col-sm-10">
                            {{ Form::text("meta_description[$key]", '', ['class'=>'form-control'] ) }}
                        </div>
                    </div>

                </div>
            </div>
        @endforelse
        <?php $count++ ?>
    @endforeach

    <div class="tab-pane" id="tab_filters">
        <div class="tab-content">
            @foreach($filters as $filter)
                <div class="form-group">
                    {{ Form::label('filter_name', $filter->name, ['class' => 'col-sm-2']) }}
                    @if (in_array($filter->id, $category_filter))
                        {{ Form::checkbox('filters[]', $filter->id, true) }}
                    @else
                        {{ Form::checkbox('filters[]', $filter->id) }}
                    @endif
                </div>
            @endforeach
        </div>
    </div>
</div>

