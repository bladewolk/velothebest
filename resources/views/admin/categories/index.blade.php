@extends('admin.layout')
@section('main')
    <h1>
        <span class="glyphicon glyphicon-globe"></span>
        {{ $title }}

        <a href="{{ route('product_managment') }}" class="btn btn-primary" title="Назад" data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>
        <a href="{{ route('categories.create') }}" id="send-form" class="btn btn-success"
           title="Создать страницу"
           data-toggle="tooltip"><span
                    class="fa fa-plus"></span></a>

        @if($count > 0)
            <span class="label label-primary" style="font-size: 14px;">
			{{ $count }}
                {{ Lang::choice('категория|категории|категорий', $count, [], 'ru') }}
        </span>
        @else
            <div class="btn alert-warning" role="alert">
                Категории отсутствуют
            </div>
        @endif

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>

    @if($count)
        <div ng-app="ngApp" class="nodeWrapper">
            <div ng-controller="treeCtrl">

                <script type="text/ng-template" id="nodes_renderer.html">
                    <div class="tree-node">
                        <div class="tree-node-content">
                            <span class="glyphicon glyphicon-list"  ui-tree-handle></span>
                            <a class="btn btn-success btn-xs" ng-if="node.children && node.children.length > 0" nodrag
                               ng-click="toggle(this)"><span class="glyphicon"
                                                             ng-class="{'glyphicon-chevron-right': collapsed, 'glyphicon-chevron-down': !collapsed}"></span></a>
                            <div style="display: inline-block; margin-right: 9px;">
                                <input name="check" type="checkbox" ng-model="selected[node.id]" value="[[node.id]]">
                            </div>
                            <a class="black_link" href="/master/products_management/categories/[[node.id]]/edit"
                               title="Редактировать" data-toggle="tooltip">[[node.name]]</a>

                            <div class="pull-right" style="margin-top: -6px; margin-right: -5px;">
                                <a class="btn btn-primary" href="/master/products_management/categories/[[node.id]]/edit"
                                title="Редактирова страницу" data-toggle="tooltip"><span
                                class="fa fa-pencil"></span></a>

                                <span title="Удалить" data-toggle="tooltip"
                                      class="delete btn btn-danger"
                                      ng-click="delete(node)"
                                      data-id="[[node.id]]"><span class="fa fa-times"></span></span>

                                <a title="Открыть в новом окне" target="_blank" href="/[[node.slug]]"
                                   data-toggle="tooltip"
                                   class="btn btn-default"><span class="glyphicon glyphicon-new-window"></span></a>
                            </div>
                        </div>
                    </div>
                    <ol ui-tree-nodes="" ng-model="node.children" ng-class="{hidden: collapsed}">
                        <li ng-repeat="node in node.children" ui-tree-node ng-include="'nodes_renderer.html'">
                        </li>
                    </ol>
                </script>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                        <span class="btn btn-warning" title="Выбрать все" data-toggle="tooltip"
                              id="check-all"><i class="glyphicon glyphicon-ok" ng-click="checkAll()"
                                                aria-hidden="true"></i></span>
                            <button id="delete-selected" class="btn btn-danger"
                                    title="Удалить выбранные" ng-click="deleteChecked()"
                                    data-toggle="tooltip"><span
                                        class="glyphicon glyphicon-trash"></span></button>
                            <button class="btn btn-success" ng-click="expandAll()">Развернуть все</button>
                            <button class="btn btn-primary" ng-click="collapseAll()">Свернуть все</button>

                            Поиск: <input class="form-control" style="display: inline-block; width: auto;"
                                          ng-model="query"
                                          ng-change="findNodes()" title="Только для главной категории"
                                          data-toggle="tooltip">
                        </div>
                    </div>
                </div>

                <hr>


                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div ui-tree="dataOptions" data-max-depth="2"  id="tree-root">
                            <ol ui-tree-nodes ng-model="data">
                                <li ng-repeat="node in data" ui-tree-node ng-include="'nodes_renderer.html'"
                                    ng-show="visible(node)" data-scroll-container=".nodeWrapper">></li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    @endif


    <script src="{{ asset('angular-ui-tree/angular.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('angular-ui-tree/dist/angular-ui-tree.min.css') }}">
    <script src="{{ asset('angular-ui-tree/dist/angular-ui-tree.js') }}"></script>
    <script>
        (function () {
            'use strict';

            var app = angular.module('ngApp', ['ui.tree'], function ($interpolateProvider) {
                $interpolateProvider.startSymbol('[[');
                $interpolateProvider.endSymbol(']]');
            });

            app.controller('treeCtrl', ['$scope', '$http', function ($scope, $http) {
                $scope.remove = function (scope) {
                    scope.remove();
                };

                $scope.data = {!! $items !!};
                $scope.selected = [];
                var flagChecked = true;


                $scope.visible = function (item) {
                    return !($scope.query && $scope.query.length > 0
                    && item.name.indexOf($scope.query) == -1);
                };

                $scope.checkAll = function () {
                    $scope.data.filter(function (el, index) {
                        $scope.selected[el.id] = flagChecked;
                    });
                    flagChecked = !flagChecked;
                }

                $scope.deleteChecked = function () {
                    var checked = [];
                    $scope.selected.filter(function (el, index) {
                        if (el == true)
                            checked.push(index);
                    });
                    if (checked.length < 1)
                        return;

                    $http({
                        method: 'POST',
                        url: '/master/products_management/categories/' + 1,
                        data: {
                            _token: "{{ Session::token() }}",
                            checked: checked,
                            {{--type: "{{ Request::get('type') }}",--}}
                            _method: "DELETE",
                            action: 'destroy'
                        }
                    }).then(function successCallback(response) {
                        console.log(response.data);
                        $scope.data = response.data;
                    }, function errorCallback(response) {
                        alert('Ошибка, перезагрузите страницу');
                    });
                }

                $scope.collapseAll = function () {
                    $scope.$broadcast('angular-ui-tree:collapse-all');
                };

                $scope.expandAll = function () {
                    $scope.$broadcast('angular-ui-tree:expand-all');
                };

//                Remove item
                $scope.delete = function (node) {
                    var item = this;
                    $http({
                        method: 'POST',
                        url: '/master/products_management/categories/' + node.id,
                        data: {
                            {{--_token: "{{ Session::token() }}",--}}
                            id: node.id,
                            _method: "DELETE",
                            action: 'delete_one'
                        }
                    }).then(function successCallback(response) {
                        $scope.remove(item);
                    }, function errorCallback(response) {
                        alert('Ошибка, перезагрузите страницу');
                    });
                }
//Rebuild TREE
                $scope.dataOptions = {
                    dropped: function (e) {
                        $http({
                            method: 'POST',
                            url: '{{ route('rebuildTree') }}',
                            data: {
                                {{--type: "{{ $type }}",--}}
                                _token: "{{ Session::token() }}",
                                item: e.source.nodeScope.$modelValue.id,
                                data: $scope.data,
                                model: 'Categorie'
                            }
                        }).then(function successCallback(response) {

                        }, function errorCallback(response) {
                            alert('Ошибка, перезагрузите страницу');
                        });
                    },
                }

            }]);
        }());
        //Delete item on button DELETE click
    </script>
@endsection