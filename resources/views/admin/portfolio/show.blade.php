@extends('admin.layout')
@section('main')
    <h1>
        <span class="glyphicon glyphicon-camera"></span>
        Портфолио: {{ $title }}
        @if($count > 0)
            <span class="btn btn-primary" style="font-size: 14px; font-weight: bold;">
			{{ $count }}
                {{ Lang::choice('изображение|изображения|изображений', $count, [], 'ru') }}
        </span>
        @else
            <div class="btn alert-warning" role="alert">
                Изображения отсутствуют
            </div>
        @endif
        <a href="{{ route('portfolio.index') }}" class="btn btn-primary" title="Назад" data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>
        <button id="upload-form" class="btn btn-success" title="Загрузить изображения" data-toggle="tooltip"><i
                    class="fa fa-upload"></i>
        </button>

        <span class="btn btn-warning" title="Выбрать все" data-toggle="tooltip"
              id="check-all"><i class="glyphicon glyphicon-ok"
                                aria-hidden="true"></i></span>
        <button class="delete-selected btn btn-danger"
                title="Удалить выбранные" data-toggle="tooltip">
            <span class="glyphicon glyphicon-trash"></span></button>


        @if(Session::has('success'))
            <span class="btn alert-success" role="alert">{{ Session::get('success') }}</span>
        @elseif(Session::has('error'))
            <span class="btn alert-danger" role="alert">{{ Session::get('error') }}</span>
        @endif

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>

    <div class="confirm-message alert alert-info text-center"
         style="position: fixed; top: -50%; left: 0; right: 0; z-index: 10">
        Подтвердите удаление записей из БД &nbsp;
        <span class="confirm-delete btn btn-success">Да</span>
        <span class="unconfirm-delete btn btn-danger">Нет</span>
    </div>

    {!! Form::model( $model, ['route' =>['portfolio.store', 'id'=> $model->id],'id' => 'upload-images','class' => 'form-horizontal', 'role' => 'form', 'files' => true]) !!}
    {{ Form::hidden('picture', true) }}
    <div class="col-sm-12">
        <div class="form-group">
            {!! Form::label('image', 'Изображение', array('class'=>'col-sm-2 control-label') ) !!}
            <div class="col-sm-8">
                {!! Form::file('image[]',array('class' => 'filestyle', 'data-value'=> '',
                'data-buttonText' => 'Выберите файлы', 'data-buttonName' => 'btn-primary',
                'data-icon' => 'true' , 'multiple' => 'true', 'required') ) !!}
                <span>Рекомендуемый размер изображения XxXpx.</span>
            </div>
        </div>
    </div>
    {{ Form::close() }}

    @if($count)
        {!! Form::open(['route' => ['portfolio.destroy', 'destroy', 'id' => 'delete-form']]) !!}
        {{ method_field('DELETE') }}
        <div class="row">
            @foreach ($items as $item )
                <div class="col-sm-6 col-md-2">
                    <div class="thumbnail">
                        <img style="width: 100%;" alt="{{ $item->name }}"
                             src="{{ File::exists(public_path('uploads/'.$item->name)) ? asset('uploads/'.$item->name) : asset('images/noimg.jpg')}}">
                        <div class="caption">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default" title="Выбрать слайд"><input
                                            style="margin: 0;" name="check[]"
                                            value="{{ $item->id }}"
                                            type="checkbox"></button>

                                <button title="Удалить" data-toggle="tooltip" type="submit"
                                        class="delete-this-id btn btn-danger"><span
                                            class="glyphicon glyphicon-remove"></span></button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="select_form">
            <label id="check_all" class="btn btn-primary">Выбрать все</label>
            <button type="submit" style="" class="btn btn-danger">Удалить выбранные</button>
        </div>
        {!! Form::close() !!}

    @endif
    <script>
        $(function () {
            $(":checkbox").click(function (event) {
                return $(this).parent().click();
            });

            $('button').click(function () {
                checkbox = $(this).children(':checkbox');
                checkbox.is(":checked") ? checkbox.prop('checked', false) : checkbox.prop('checked', true);
            });
//Delete record
//            $('.delete').click(function () {
//                $('input[type="checkbox"][name*="check"]').prop('checked', false);
//                $(this).closest(".btn-group").find('input[type="checkbox"][name*="check"]').prop('checked', true);
//                $(this).closest("form#list").submit();
//            })
//Check All
//            $("#check-all").on('click', function () {
//                $('input[type="checkbox"][name*="check"]').prop('checked', $('input[type="checkbox"][name*="check"]:not(:checked)').length > 0);
//            });

//            $('form').submit(function () {
//                if (confirm("Подтвердить действие?") === false)
//                    return false;
//                else
//                    return true;
//            });

            $("#upload-form").click(function () {
                $("#upload-images").submit();
            });

            var flag = false;
            $(".delete-selected, .unconfirm-delete, .delete-this-id").click(function () {
                flag = !flag;
                if (flag)
                    move = "50%";
                else move = "-50%";

                $('.confirm-message').stop().animate({
                    top: move
                }, 300);
            });
            $(".confirm-delete").click(function () {
                $('#delete-form').submit();
            });
            $(".unconfirm-delete").click(function () {
                $('input[type=checkbox]:checked').prop('checked', false);
            });
            $(".delete-this-id").click(function () {
                $(this).closest().find('input:checkbox').prop('checked', true);
            });
            $("#check-all").on('click', function () {
                $('input[type="checkbox"][name*="check"]').prop('checked', $('input[type="checkbox"][name*="check"]:not(:checked)').length > 0);
            });
        });
    </script>
@stop