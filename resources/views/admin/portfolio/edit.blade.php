@extends('admin.layout')

@section('main')

    <h1>{{ $title }}
        <a href="{{ route('portfolio.index') }}" class="btn btn-primary" title="Назад"
           data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>

        <button id="send-form" class="btn btn-success" title="Сохранить" data-toggle="tooltip"><i class="fa fa-floppy-o"
                                                                                                  aria-hidden="true"></i>
        </button>


        @if(Session::has('success'))
            <span class="btn alert-success" role="alert">{{ Session::get('success') }}</span>
        @elseif(Session::has('error'))
            <span class="btn alert-danger" role="alert">{{ Session::get('error') }}</span>
        @endif

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>

    {{ Form::model($item, ['route'=> ['portfolio.update', $item->id], 'class' => 'form-horizontal', 'role' => 'form'] ) }}
    {{ method_field('PATCH') }}
    @include('admin.portfolio.form')
    {{ Form::close() }}


    <script>
        $('#send-form').click(function () {
            $('form').submit();
        });

        meta_title_touched = false;
        url_touched = false;

        $('input[name="slug"]').change(function () {
            url_touched = true;
        });

        $('input[name="name"]').keyup(function () {
            if (!url_touched)
                $('input[name="slug"]').val(generate_url());

            if (!meta_title_touched)
                $('input[name="meta_title"]').val($('input[name="name"]').val());
        });

        $('input[name="meta_title"]').change(function () {
            meta_title_touched = true;
        });

        function generate_url() {
            url = $('input[name="name"]').val();
            url = url.replace(/[\s]+/gi, '-');
            url = translit(url);
            url = url.replace(/[^0-9a-z_\-]+/gi, '').toLowerCase();
            return url;
        }

        function translit(str) {
            var ru = ("А-а-Б-б-В-в-Ґ-ґ-Г-г-Д-д-Е-е-Ё-ё-Є-є-Ж-ж-З-з-И-и-І-і-Ї-ї-Й-й-К-к-Л-л-М-м-Н-н-О-о-П-п-Р-р-С-с-Т-т-У-у-Ф-ф-Х-х-Ц-ц-Ч-ч-Ш-ш-Щ-щ-Ъ-ъ-Ы-ы-Ь-ь-Э-э-Ю-ю-Я-я").split("-")
            var en = ("A-a-B-b-V-v-G-g-G-g-D-d-E-e-E-e-E-e-ZH-zh-Z-z-I-i-I-i-I-i-J-j-K-k-L-l-M-m-N-n-O-o-P-p-R-r-S-s-T-t-U-u-F-f-H-h-TS-ts-CH-ch-SH-sh-SCH-sch-'-'-Y-y-'-'-E-e-YU-yu-YA-ya").split("-")
            var res = '';
            for (var i = 0, l = str.length; i < l; i++) {
                var s = str.charAt(i), n = ru.indexOf(s);
                if (n >= 0) {
                    res += en[n];
                }
                else {
                    res += s;
                }
            }
            return res;
        }
    </script>
@stop