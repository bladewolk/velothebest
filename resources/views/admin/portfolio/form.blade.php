<ul class="nav nav-tabs" id="myTab">
    <li class="active"><a href="#tab_a" data-toggle="tab">Запись</a></li>
    <li><a href="#tab_b" data-toggle="tab">Параметры страницы</a></li>
</ul>
<div class="tab-content">

    <div class="tab-pane active" id="tab_a">
        <div class="form-group">&nbsp;</div>
        <div class="tab-content">
            <div class="form-group">
                {{ Form::label('name', 'Название', ['class'=>'col-sm-2 control-label'] ) }}
                <div class="col-sm-10">
                    {{ Form::text('name', old('name') ? old('name') : $item->name, ['class'=>'form-control', 'autocomplete' => 'off'] ) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('body', 'Текст страницы', ['class'=>'col-sm-2 control-label'] ) }}
                <div class="col-sm-10">
                    {{ Form::textarea('body', old('body') ? old('body') : $item->body, ['class'=>'form-control editor'] ) }}
                </div>
            </div>
        </div>
    </div>


    <div class="tab-pane" id="tab_b">
        <div class="form-group">&nbsp;</div>

        @if( !in_array( $item->slug, ['404']) )
            <div class="form-group">
                {{ Form::label('slug', 'Адрес', ['class'=>'col-sm-2 control-label'] ) }}
                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon">&nbsp;/&nbsp;</span>
                        {{ Form::text('slug', old('slug') ? old('slug') : $item->slug, ['class'=>'form-control', 'id' => 'inputSlug', 'autocomplete' => 'off'] ) }}
                    </div>
                </div>
            </div>
        @endif

        <div class="form-group">
            {{ Form::label('meta_title', 'Заголовок', ['class'=>'col-sm-2 control-label'] ) }}
            <div class="col-sm-10">
                {{ Form::text('meta_title', old('meta_title') ? old('meta_title') : $item->meta_title, ['class'=>'form-control', 'id' => 'inputMetaTitle', 'autocomplete' => 'off'] ) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('meta_keywords', 'Ключевые слова', ['class'=>'col-sm-2 control-label'] ) }}
            <div class="col-sm-10">
                {{ Form::text('meta_keywords', old('meta_keywords') ? old('meta_keywords') : $item->meta_keywords, ['class'=>'form-control', 'id' => 'inputMetaTitle', 'autocomplete' => 'off'] ) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('meta_description', 'Описание', ['class'=>'col-sm-2 control-label'] ) }}
            <div class="col-sm-10">
                {{ Form::text('meta_description', old('meta_description') ? : $item->meta_description, ['class'=>'form-control', 'autocomplete' => 'off'] ) }}
            </div>
        </div>

    </div>
</div>
