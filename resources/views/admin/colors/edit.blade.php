@extends('admin.layout')

@section('main')

    <h1>{{ $title }}
        <a href="{{ route('colors.index') }}" class="btn btn-primary" title="Назад"
           data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>

        <button id="send-form" class="btn btn-success" title="Сохранить" data-toggle="tooltip"><i class="fa fa-floppy-o"
                                                                                                  aria-hidden="true"></i>
        </button>

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>

    {{ Form::model($item, ['route'=> ['colors.update', $item->id], 'class' => 'form-horizontal', 'role' => 'form'] ) }}
    <div class="form-group">
        {{ method_field('PATCH') }}
        {{ Form::label('name', 'Название', ['class'=>'col-sm-1 control-label'] ) }}
        <div class="col-sm-2">
            {{ Form::text("name", $item->name, ['class'=>'form-control'] ) }}
        </div>
        {{ Form::label('value', 'Цвет', ['class'=>'col-sm-1 control-label'] ) }}
        <div class="col-sm-2">
            {{ Form::color("value", $item->value, ['class'=>'form-control'] ) }}
        </div>
    </div>
    {{ Form::close() }}

    <script>
        $('#send-form').click(function () {
            $('form').submit();
        });
    </script>
@stop