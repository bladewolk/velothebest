@extends('admin.layout')
@section('main')
    <h1>
        <span class="fa fa-newspaper-o"></span>
        {{ $title }}
        <a href="{{ route('product_managment') }}" class="btn btn-primary" title="Назад" data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>

        @if($items->total() > 0)
            <span class="btn btn-warning" title="Выбрать все" data-toggle="tooltip"
                  id="check-all"><i class="glyphicon glyphicon-ok"
                                    aria-hidden="true"></i></span>
            <button class="delete-selected btn btn-danger"
                    title="Удалить выбранные" data-toggle="tooltip">
                <span class="glyphicon glyphicon-trash"></span></button>

            <span class="label label-primary" style="font-size: 14px;">
			{{ $items->total() }}
                {{ Lang::choice('цвет|цвета|цветов', $items->total(), [], 'ru') }}
        </span>
        @endif

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>

    {!! Form::open(['route'=> ['colors.store'], 'class' => 'form-horizontal', 'role' => 'form', 'files' => true] ) !!}
    <div class="form-group">
        {{ Form::label('name', 'Название', ['class'=>'col-sm-1 control-label'] ) }}
        <div class="col-sm-2">
            {{ Form::text("name", "", ['class'=>'form-control'] ) }}
        </div>
        {{ Form::label('value', 'Цвет', ['class'=>'col-sm-1 control-label'] ) }}
        <div class="col-sm-2">
            {{ Form::color("value", "", ['class'=>'form-control'] ) }}
        </div>
        <button class="fa fa-floppy-o btn btn-success" data-toggle="tooltip" title="Создать"></button>
    </div>
    {!! Form::close() !!}

    @if ($items->total() > 0)
        <hr>
        {!! Form::open(['route' => ['colors.destroy', 'destroy']]) !!}
        {{ method_field('DELETE') }}
        <table class="table">
            <tr>
                <th style="width: 20%;">Название</th>
                <th>Цвет</th>
                <th style="text-align: right;">Управление</th>
            </tr>
            @foreach ($items as $item)
                <tr>
                    <td style="vertical-align: middle">
                        {{ Form::checkbox('check[]', $item->id, false, ['id' => 'check'.$item->id, 'class' => 'custom-check']) }}
                        {{ Form::label('check'.$item->id, '1', ['class' => 'custom-label-for-check']) }}
                        {{--<input name="check[]" value="{{ $item->id }}"type="checkbox">--}}
                        <a class="black_link" title="Редактировать новость" data-toggle="tooltip"
                           href="{{ route('colors.edit', $item->id) }}">
                            {{ $item->name }}</a>
                    </td>
                    <td>
                        <div style="background: rgba(0,0,0,.1); display: inline-block; padding: 2px 5px">
                            <span class="fa fa-circle"
                                  style="color: {{ $item->value }}; font-size: 25px"></span>
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <a title="Редактировать" href="{{ route('colors.edit', ['id' => $item->id]) }}"
                           data-toggle="tooltip"
                           class="btn btn-primary"><span class="fa fa-pencil"></span></a>
                        <span class="delete-this-id btn btn-default" title="Удалить"
                              data-toggle="tooltip"><span
                                    class="fa fa-times"></span>
                        </span>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ Form::close() }}

    @endif
    {{--    {{ $items->links() }}--}}

    <script>
        $(function () {

            var flag = false;
            $(".delete-selected, .unconfirm-delete, .delete-this-id").click(function () {
                flag = !flag;
                if (flag)
                    move = "50%";
                else move = "-50%";

                $('.confirm-message').stop().animate({
                    top: move
                }, 300);
            });
            $(".confirm-delete").click(function () {
                $('form').submit();
            });
            $(".unconfirm-delete").click(function () {
                $('input[type=checkbox]:checked').prop('checked', false);
            });
            $(".delete-this-id").click(function () {
                $(this).closest('tr').find('input:checkbox').prop('checked', true);
            });
            $("#check-all").on('click', function () {
                $('input[type="checkbox"][name*="check"]').prop('checked', $('input[type="checkbox"][name*="check"]:not(:checked)').length > 0);
            });
        })
    </script>
@endsection