<div class="col-sm-12">
    <div class="form-group">
        {!! Form::label('image', 'Основное изображение', array('class'=>'col-sm-2 control-label') ) !!}
        <div class="col-sm-9">
            {!! Form::file('image' ,array('class' => 'filestyle', 'data-value'=> '',
            'data-buttonText' => 'Выберите файлы', 'data-buttonName' => 'btn-primary',
            'data-icon' => 'true' , 'multiple' => 'false') ) !!}
            <span class="label label-primary">1200px x 500px</span>
        </div>
    </div>
    @if($item->image)
        <div class="form-group">
            {!! Form::label('image', 'Текущее изображение', array('class'=>'col-sm-2 control-label') ) !!}
            <img class="col-sm-9" src="{{ '/img/'.$item->image.'?w=1170&h=600' }}" alt="slide">
        </div>
    @endif
</div>

<div class="col-sm-12">
    <div class="form-group">
        {!! Form::label('image_background', 'Фоновое изображение', array('class'=>'col-sm-2 control-label') ) !!}
        <div class="col-sm-9">
            {!! Form::file('image_background',array('class' => 'filestyle', 'data-value'=> '',
            'data-buttonText' => 'Выберите файлы', 'data-buttonName' => 'btn-primary',
            'data-icon' => 'true' , 'multiple' => 'false') ) !!}
            <span class="label label-primary">1200px x 500px</span>
        </div>
    </div>
    @if($item->image_background)
        <div class="form-group">
            {!! Form::label('image-background', 'Текущее изображение', array('class'=>'col-sm-2 control-label') ) !!}
            <img class="col-sm-9" src="{{ '/img/'.$item->image_background.'?w=1900&h=600' }}" alt="slide">
        </div>
    @endif
</div>