@extends('admin.layout')
@section('main')

    <h1>{{ $title }}
        <a href="{{ route('slider.index') }}" class="btn btn-primary" title="Назад" data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>
        <a href="" id="send-form" class="btn btn-success" title="Сохранить" data-toggle="tooltip"><span
                    class="fa fa-floppy-o"></span></a>

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>

    {!! Form::model( $item, ['route' =>['slider.update', $item->id], 'class' => 'form-horizontal', 'role' => 'form', 'files' => true]) !!}
    {{ method_field('PATCH') }}
    @include('admin.slider.form')
    {!! Form::close() !!}

    <script>
        $("#send-form").click(function (event) {
            event.preventDefault();
            $('form').submit();
        });
    </script>
@stop