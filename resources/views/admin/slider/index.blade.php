@extends('admin.layout')
@section('main')
    <h1>
        <span class="glyphicon glyphicon-tasks"></span>
        {{ $title }}
        <a href="{{ route('master') }}" class="btn btn-primary" title="Назад" data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>

        <a href="{{ route('slider.create') }}" id="send-form" class="btn btn-success" title="Создать слайд"
           data-toggle="tooltip">
           <span class="fa fa-plus"></span>
        </a>
          @if($items->count() > 0)
            <span class="btn btn-warning" title="Выбрать все" data-toggle="tooltip"
                  id="check-all"><i class="glyphicon glyphicon-ok"
                                    aria-hidden="true"></i></span>
            <button class="delete-selected btn btn-danger"
                    title="Удалить выбранные" data-toggle="tooltip">
                <span class="glyphicon glyphicon-trash"></span></button>

            <span class="label label-primary" style="font-size: 14px;">
            {{ $items->count() }}
            {{ Lang::choice('слайд|слайда|слайдов', $items->count(), [], 'ru') }}
        </span>
        @endif

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>

    @if($count)
        {!! Form::open(['route' => ['slider.destroy', 'destroy']]) !!}
        {{ method_field('DELETE') }}
        <div class="row">
            @foreach ( $items as $item )
                <div class="col-sm-6 col-md-2">
                    <div class="thumbnail">
                        <a href="{{ route('slider.edit', ['id' => $item->id]) }}" title="Редактировать слайд"
                           data-toggle="tooltip">
                            <div class="image-wrapper"
                                 style="background: url('{{ '/img/'. $item->image_background.'?w=150' }}');
                                         background-size: cover; text-align: center;">
                                @if($item->image)
                                    <img style="display: inline-block;"
                                     alt="photo"
                                     src="{{ '/img/' . $item->image .'?w=150'}}">
                                @else
                                    <br><br><br><br>
                                @endif
                            </div>
                        </a>
                        <div class="caption">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default" title="Выбрать слайд"><input
                                            style="margin: 0;" name="check[]"
                                            value="{{ $item->id }}"
                                            type="checkbox"></button>

                                <span class="delete-this-id btn btn-danger" title="Удалить"
                                      data-toggle="tooltip"><span
                                            class="fa fa-times"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        {!! Form::close() !!}
    @endif
    <script>
       $(function () {
            var flag = false;
            $(".delete-selected, .unconfirm-delete, .delete-this-id").click(function () {
                flag = !flag;
                if (flag)
                    move = "50%";
                else move = "-50%";

                $('.confirm-message').stop().animate({
                    top: move
                }, 300);
            });
            $(".confirm-delete").click(function () {
                $('form').submit();
            });
            $(".unconfirm-delete").click(function () {
                $('input[type=checkbox]:checked').prop('checked', false);
            });
            $(".delete-this-id").click(function () {
                $(this).closest('.btn-group').find('input:checkbox').prop('checked', true);
            });
            $("#check-all").on('click', function () {
                $('input[type="checkbox"][name*="check"]').prop('checked', $('input[type="checkbox"][name*="check"]:not(:checked)').length > 0);
            });
        });
    </script>
@stop