@extends('admin.layout')

@section('main')

    <div class="row pages">

        <div class="col-sm-12">
            <h1>Управление контентом</h1>
        </div>

        <div class="col-md-2 col-sm-3 col-xs-4 nav-block">
            <a href="{{ route('page.index',['type' => 'main']) }}">
                <i class="fa fa-rocket fa-4x"></i>
                <p>Страницы основного меню</p>
            </a>
        </div>

        <div class="col-md-2 col-sm-3 col-xs-4 nav-block">
            <a href="{{ route('page.index',['type' => 'top']) }}">
                <i class="fa fa-space-shuttle fa-4x"></i>
                <p>Страницы топ меню</p>
            </a>
        </div>


        {{--<div class="col-md-2 col-sm-3 col-xs-4">--}}
            {{--<a href="{{ route('portfolio.index') }}">--}}
                {{--<i class="fa fa-camera fa-4x"></i>--}}
                {{--<span>Портфолио</span>--}}
            {{--</a>--}}
        {{--</div>--}}

        <div class="col-md-2 col-sm-3 col-xs-4 nav-block">
            <a href="{{ route('slider.index') }}">
                <i class="fa fa-sliders fa-4x"></i>
                <p>Слайдер</p>
            </a>
        </div>

        <div class="col-md-2 col-sm-3 col-xs-4 nav-block">
            <a href="{{ route('banner.index') }}">
                <i class="fa fa-sliders fa-4x"></i>
                <p>Баннер</p>
            </a>
        </div>

        <div class="col-md-2 col-sm-3 col-xs-4 nav-block">
            <a href="{{ route('currency.index') }}">
                <i class="fa fa-usd fa-4x"></i>
                <p>Валюта</p>
            </a>
        </div>

        <div class="col-md-2 col-sm-3 col-xs-4 nav-block">
            <a href="{{ route('news.index') }}">
                <i class="fa fa-newspaper-o fa-4x"></i>
                <p>Новости</p>
            </a>
        </div>

    </div>

@stop