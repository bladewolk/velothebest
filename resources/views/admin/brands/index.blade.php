@extends('admin.layout')
@section('main')
    <h1>
        <span class="fa fa-newspaper-o"></span>
        {{ $title }}
        <a href="{{ route('product_managment') }}" class="btn btn-primary" title="Назад" data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>
        <a href="{{ route('brands.create') }}" id="send-form" class="btn btn-success"
           title="Cоздать Бренд"
           data-toggle="tooltip"><span
                    class="fa fa-plus"></span></a>

        @if($items->total() > 0)
            <span class="btn btn-warning" title="Выбрать все" data-toggle="tooltip"
                  id="check-all"><i class="glyphicon glyphicon-ok"
                                    aria-hidden="true"></i></span>
            <button class="delete-selected btn btn-danger"
                    title="Удалить выбранные" data-toggle="tooltip">
                <span class="glyphicon glyphicon-trash"></span></button>

            <span class="label label-primary" style="font-size: 14px;">
			{{ $items->total() }}
                {{ Lang::choice('бренд|бренда|брендов', $items->total(), [], 'ru') }}
        </span>
        @else
            <div class="btn alert-warning" role="alert">
                Записи отсутствуют
            </div>
        @endif

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>

    @if ($items->total() > 0)

        {!! Form::open(['route' => ['brands.destroy', 'destroy']]) !!}
        {{ method_field('DELETE') }}
        <table class="table">
            <tr>
                <th style="width: 40%;">Название</th>
                <th style="text-align: right;">Управление</th>
            </tr>
            @foreach ($items as $item)
                <tr>
                    <td style="vertical-align: middle">
                        {{ Form::checkbox('check[]', $item->id, false, ['id' => 'check'.$item->id, 'class' => 'custom-check']) }}
                        {{ Form::label('check'.$item->id, '1', ['class' => 'custom-label-for-check']) }}
                        {{--<input name="check[]" value="{{ $item->id }}"type="checkbox">--}}
                        <a class="black_link" title="Редактировать" data-toggle="tooltip"
                           href="{{ route('brands.edit', $item->id) }}">
                            {{ $item->name }}</a>
                    </td>
                    <td style="text-align: right;">
                        <a title="Редактировать" href="{{ route('brands.edit', ['id' => $item->id]) }}"
                           data-toggle="tooltip"
                           class="btn btn-default"><span class="fa fa-pencil"></span></a>
                        <span class="delete-this-id btn btn-default" title="Удалить"
                              data-toggle="tooltip"><span
                                    class="fa fa-times"></span>
                            </span>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ Form::close() }}

    @endif

    {{ $items->links() }}

    <script>
        $(function () {
            var flag = false;
            $(".delete-selected, .unconfirm-delete, .delete-this-id").click(function () {
                flag = !flag;
                if (flag)
                    move = "50%";
                else move = "-50%";

                $('.confirm-message').stop().animate({
                    top: move
                }, 300);
            });
            $(".confirm-delete").click(function () {
                $('form').submit();
            });
            $(".unconfirm-delete").click(function () {
                $('input[type=checkbox]:checked').prop('checked', false);
            });
            $(".delete-this-id").click(function () {
                $(this).closest('tr').find('input:checkbox').prop('checked', true);
            });
            $("#check-all").on('click', function () {
                $('input[type="checkbox"][name*="check"]').prop('checked', $('input[type="checkbox"][name*="check"]:not(:checked)').length > 0);
            });
        })
    </script>
@endsection