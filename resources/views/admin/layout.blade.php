<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ $title }} @if( isset( $site_offline ) && $site_offline == 1 )OFFLINE @endif</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href="/dashboard/css/bootstrap-switch.min.css" rel="stylesheet">
    <link href="/dashboard/css/magnific.css" rel="stylesheet">
    <link href="/admin/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/flags.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/custom.css') }}">
    <link href="/dashboard/bower_components/angular/angular-ui-tree.min.css" rel="stylesheet">
    {{--<script src="/dashboard/js/jquery.js"></script>--}}
    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    @section('styles')
    @show

    <script src="/dashboard/js/magnific.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="/dashboard/js/bootstrap.filestyle.js"></script>
    <script src="/dashboard/js/bootstrap-switch.min.js"></script>
    <script src="/dashboard/js/bootstrap-growl.min.js"></script>
    @section('scripts')
	@show
	<!--[if lt IE 9]>
    <script src="/frontend/js/html5shiv.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            <li class="icon @if( in_array( Request::segment(2), ['settings'] )) active @endif">
                <a class="navbar-brand" href="{{ route('settings.edit', 1) }}">
                    <i class="fa fa-cog @if( in_array( Request::segment(2), ['settings'] )) fa-spin @endif fa-2x"></i>
                </a>
            </li>
            <li @if( in_array( Request::segment(2), ['','page', 'articles', 'news', 'slider', 'portfolio'] ) ) class="active"@endif>
                <a href="/master">Управление контентом<span class="sr-only">(current)</span></a>
                <ul>
                    @foreach($navigation['content'] as $key => $value)
                        <li><a href="{{ url($value) }}"><p>{{$key}}</p></a></li>
                    @endforeach
                </ul>
            </li>
            <li @if( in_array( Request::segment(2), ['products_management','categories', 'payment', 'products', 'delivery', 'features', 'option_variants'] ) ) class="active"@endif>
                <a href="/master/products_management">Управление товаром</a>
                <ul>
                    @foreach($navigation['product'] as $key => $value)
                        <li><a href="{{ url($value) }}"><p>{{$key}}</p></a></li>
                    @endforeach
                </ul>
            </li>
            <li @if( Request::segment(2) == 'order' ) class="active" @endif title="Заказы" data-toggle="tooltip"
                data-placement="bottom">
                <a href="{{ route('order.index') }}">
                    <i style="font-size: 18px;" class="fa fa-cart-plus" aria-hidden="true"></i>
                    @if ($new_orders > 0)
                        <label class="label label-primary">
                            {{ $new_orders }}
                        </label>
                    @endif
                </a>
            </li>
            <li @if( Request::segment(2) == 'questions' ) class="active" @endif title="Вопросы покупателей"
                data-toggle="tooltip" data-placement="bottom">
                <a href="{{ route('questions.index') }}">
                    <i style="font-size: 18px;" class="fa fa-weixin" aria-hidden="true"></i>
                    @if ($new_questions > 0)
                        <label class="label label-primary">
                            {{ $new_questions }}
                        </label>
                    @endif
                </a>
            </li>
            <li @if( Request::segment(2) == 'feedback' ) class="active"
                @endif title="Обратная связь (Заказы обратных звонков)"
                data-toggle="tooltip" data-placement="bottom">
                <a href="{{ route('feedback.index') }}">
                    <i style="font-size: 18px;" class="fa fa-volume-control-phone" aria-hidden="true"></i>
                    @if ($feedback > 0)
                        <label class="label label-primary">
                            {{ $feedback }}
                        </label>
                    @endif
                </a>
            </li>
            <li @if( Request::segment(2) == 'comments' ) class="active"
                @endif title="Комментарии пользователей" data-toggle="tooltip" data-placement="bottom">
                <a href="{{ route('comments.index') }}">
                    <i style="font-size: 18px;" class="fa fa-commenting" aria-hidden="true"></i>
                    @if ($comments)
                        <label class="label label-primary">
                            {{$comments}}
                        </label>
                    @endif
                </a>
            </li>
            <li @if( Request::segment(2) == 'import' ) class="active" @endif title="Импорт (Документ XML)"
                data-toggle="tooltip"
                data-placement="bottom">
                <a href="{{ route('import.index') }}">
                    <i style="font-size: 18px;" class="fa fa-cloud-upload" aria-hidden="true"></i>
                </a>
            </li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="icon gotosite"><a target="_blank" href="/">
                    <div class="goto">Перейти на сайт</div>
                    <i class="fa fa-arrow-circle-o-right fa-3x"></i></a></li>
            <li><a href="/logout" class="logout"><i class="fa fa-sign-in"></i> Выход</a></li>
            <li class="icon" title="Пользователи" data-toggle="tooltip">
                <a class="navbar-brand" href="{{ route('users.index') }}">
                    <i class="fa fa-user-circle-o fa-2x"></i>
                </a>
            </li>
        </ul>
    </div>
    <!-- /.container-fluid -->
</nav>
<div class="confirm-message">
    <p>Подтвердите удаление записей из БД?</p> &nbsp;
    <span class="confirm-delete btn btn-success">Да</span>
    <span class="unconfirm-delete btn btn-danger">Нет</span>
</div>
@if(Session::has('success'))
    <div class="message message-success">
        {{-- <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
        {{-- <span class="sr-only">Success:</span> --}}
        <i class="fa fa-check"></i>
        {{ Session::get('success') }}
    </div>
@elseif(Session::has('error'))
    <div class="message message-error">
        <span class="fa fa-times" aria-hidden="true"></span>
        <span class="sr-only">Error:</span>
        {{ Session::get('error') }}
    </div>
@elseif(Session::has('warning'))
    <div class="message message-warning">
        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
        <span class="sr-only">Warning:</span>
        {{ Session::pull('warning') }}
    </div>
@elseif(Session::has('info'))
    <div class="message message-warning">
        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
        <span class="sr-only">Info:</span>
        {{ Session::pull('info') }}
    </div>
@endif

<div class="container-fluid">
    <div id="show_page">
        @yield('main')
    </div>
</div>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
<script>
    $(function () {
        $('.leopard').animate({
            'left': "100vw"
        }, 6000, function () {
            $(this).remove();
        });


        $('[data-toggle="tooltip"]').tooltip();
        $('.editor').ckeditor();

        if ($('.message').html()) {
            $('.message').animate({
                'opacity': 1,
                'top': "20vh"
            }, 1000, function () {
                setTimeout(function () {
                    $('.message').animate({
                        'opacity': 0,
                        'top': "-10%"
                    }, 700, function () {
                        $('.message').remove();
                    });
                }, 2500);
            });
        }

        function confirmDelete() {

            return true;
        }

        $(document).ready(function () {
            $('.nav-block').addClass('animate-this');
        });

        function printS(text) {
            return console.log('%c' + text, 'background: black; font-size: 18px; color: greenyellow; font-weight: bolder');
        }
    });
</script>
@yield('scripts')
</body>
</html>