@extends('admin.layout')
@section('main')
    <h1>
        <span class="fa fa-newspaper-o"></span>
        {{ $title }}
        <a href="{{ route('product_managment') }}" class="btn btn-primary" title="Назад" data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>

        @if($items->total() > 0)
            <span class="btn btn-warning" title="Выбрать все" data-toggle="tooltip"
                  id="check-all"><i class="glyphicon glyphicon-ok"
                                    aria-hidden="true"></i></span>
            <button class="delete-selected btn btn-danger"
                    title="Удалить выбранные" data-toggle="tooltip">
                <span class="glyphicon glyphicon-trash"></span></button>

            <span class="label label-primary" style="font-size: 14px;">
			{{ $items->total() }}
                {{ Lang::choice('запись|записи|записей', $items->total(), [], 'ru') }}
        </span>
        @endif

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>

    {{ Form::open(['route' => ['features.store']]) }}
    {{ Form::hidden('filter', 'filter') }}
    {{ Form::label('name', 'Название фильтра', ['class'=>'col-sm-2 control-label text-right'] ) }}
    @foreach(LaravelLocalization::getSupportedLocales() as $key => $value)
        <div class="col-sm-2">
            {{ Form::text("name[$key]", '', ['class'=>'form-control', 'data-toggle' => 'tooltip', 'title' => $value['native'], 'required', 'autocomplete' => 'off'] ) }}
        </div>
    @endforeach
    <button class="btn btn-success fa fa-plus" data-toggle="tooltip" title="Добавить"></button>
    {{ Form::close() }}

    @if ($items->total() > 0)
<hr>
        {!! Form::open(['route' => ['features.destroy', 'destroy']]) !!}
        {{ method_field('DELETE') }}
        {{ Form::hidden('parent', true) }}
        <table class="table">
            <tr>
                <th style="width: 70%;">Название</th>
                <th>Создан</th>
                <th style="text-align: right;">Управление</th>
            </tr>
            @foreach ($items as $item)
                <tr>
                    <td style="vertical-align: middle">
                        {{ Form::checkbox('check[]', $item->id, false, ['id' => 'check'.$item->id, 'class' => 'custom-check']) }}
                        {{ Form::label('check'.$item->id, '1', ['class' => 'custom-label-for-check']) }}
                        {{ csrf_field() }}

                        @foreach(LaravelLocalization::getSupportedLocales() as $key => $value)
                            @forelse($item->localization as $itemLocalized)
                                @if ($itemLocalized->locale == $key)
                                    {{ Form::text("name[$key]", $itemLocalized->name, ['class' => 'form-control feature-name', 'style' => 'width: 40%; display:inline-block;', 'data-toggle' => 'tooltip', 'title' => $value['native']]) }}
                                @endif
                            @empty
                                {{ Form::text("name[$key]", "", ['class' => 'form-control feature-name', 'style' => 'width: 50%; display:inline-block;', 'data-toggle' => 'tooltip', 'title' => $value['native']]) }}
                            @endforelse
                        @endforeach
                        <button class="btn btn-success send-ajax" title="Обновить запись" style="visibility: hidden"
                                data-toggle="tooltip" data-id="{{$item->id}}"><span class="fa fa-cogs"></span></button>


                        {{--<a class="" title="Редактировать" data-toggle="tooltip"--}}
                        {{--href="{{ route('features.edit', $item->id) }}">--}}
                        {{--{{ $item->name }}</a>--}}
                    </td>
                    <td>
                        {{ $item->created_at }}
                    </td>
                    <td style="text-align: right;">
                        <div class="btn-group">
                            <a title="Редактировать" href="{{ route('features.edit', ['id' => $item->id]) }}"
                               data-toggle="tooltip"
                               class="btn btn-default"><span class="fa fa-filter"></span></a>
                            <span class="delete-this-id btn btn-default" title="Удалить"
                                  data-toggle="tooltip"><span
                                        class="glyphicon glyphicon-remove"></span>
                            </span>
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ Form::close() }}

    @endif

    {{ $items->links() }}

    <script>
        $(function () {
            var flag = false;
            $(".delete-selected, .unconfirm-delete, .delete-this-id").click(function () {
                flag = !flag;
                if (flag)
                    move = "50%";
                else move = "-50%";

                $('.confirm-message').stop().animate({
                    top: move
                }, 300);
            });
            $(".confirm-delete").click(function () {
                $('form').submit();
            });
            $(".unconfirm-delete").click(function () {
                $('input[type=checkbox]:checked').prop('checked', false);
            });
            $(".delete-this-id").click(function () {
                $(this).closest('tr').find('input:checkbox').prop('checked', true);
            });
            $("#check-all").on('click', function () {
                $('input[type="checkbox"][name*="check"]').prop('checked', $('input[type="checkbox"][name*="check"]:not(:checked)').length > 0);
            });

            $('.feature-name').on("keyup", function () {
                $(this).closest('td').find('button').css({
                    'visibility': "visible"
                });
            });
            $(".send-ajax").click(function () {
                button = $(this);
                event.preventDefault();
                $(this).find('span').toggleClass('fa-cogs fa-spinner fa-spin');
                token = $('input[name=_token]').val();
                $.post("/master/products_management/features/update", {
                    'id': button.data("id"),
                    '_method': 'PATCH',
                    'parent': 1,
                    '_token': token,
                    'name_ru': $(this).closest('td').find('input[name^="name[ru]"]').val(),
                    'name_uk': $(this).closest('td').find('input[name^="name[uk]"]').val(),
                })
                    .done(function (response) {
                        button.css({
                            'visibility': "hidden"
                        });
                        $(this).find('span').toggleClass('fa-cogs fa-spinner fa-spin');
                    });
            })
        })
    </script>
@endsection