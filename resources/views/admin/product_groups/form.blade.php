<div class="form-group">
    {{ Form::label('body', 'Название группы', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-10">
        {{ Form::text('name', $item->name, ['class'=>'form-control'] ) }}
    </div>
</div>

<div class="row">
    <div class="col-xs-5">
        {{ Form::select("category_search", $categories, null, ['class' => 'form-control', 'id' => 'category_search']) }}
        <select name="from[]" id="search" class="form-control" size="8" multiple="multiple">
            @foreach($allProducts as $id => $name)
                <option value="{{$id}}">{{$name}}</option>
            @endforeach
        </select>
    </div>
    
    <div class="col-xs-2">
        <button type="button" id="search_rightAll" class="btn btn-block"><i class="glyphicon glyphicon-forward"></i></button>
        <button type="button" id="search_rightSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-right"></i></button>
        <button type="button" id="search_leftSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-left"></i></button>
        <button type="button" id="search_leftAll" class="btn btn-block"><i class="glyphicon glyphicon-backward"></i></button>
    </div>
    
    <div class="col-xs-5">
        <select name="to[]" id="search_to" class="form-control" size="8" multiple="multiple">
            @foreach($inGroup as $id => $name)
                <option value="{{$id}}">{{$name}}</option>
            @endforeach
        </select>
    </div>
</div>

@if($item->products)
    <table class="table">
        <tr>
            <th>Название</th>
            <th>Код</th>
            <th>Лейба</th>
            <th>Цвет</th>
            <th>Основная характеристика</th>
        </tr>
            @foreach($item->products as $product)
            <tr>
                <td>
                    <a href="{{ route('products.edit', $product->id) }}">
                        {{ $product->localization->where('locale', 'ru')->first()->name}}   
                    </a>
                </td>
                <td>
                    <span class="label label-primary">
                        {{$product->code}}
                    </span>
                </td>
                <td>
                    @if ($product->label)
                        <span class="label label-default" style="background-color: {{ $product->label->color }}">
                            {{ $product->label->localization->where('locale', 'ru')->first()->description }}
                        </span>
                    @endif
                </td>
                <td>
                    @if($product->color)
                        <span class="label label-primary" style="background-color: {{$product->color->value}}">
                            {{$product->color->name}}
                        </span>
                    @endif
                </td>
                <td>
                    @if ($product->mainFeature)
                        <span class="label label-primary">
                            {{ $product->mainFeature->localization->where('locale', 'ru')->first()->name }}:
                        </span>
                        &nbsp;
                        <span class="label label-success">
                            {{ $product->main_feature_text}}
                        </span>
                    @endif
                </td>
            </tr>
            @endforeach
    </table>
@endif