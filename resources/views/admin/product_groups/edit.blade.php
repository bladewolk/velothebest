@extends('admin.layout')
@section('main')
    <h1>{{ $title }}
        <a href="{{ route('groups.index') }}" class="btn btn-primary" title="Назад"
           data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>
      <button id="send-form" class="btn btn-success" title="Сохранить" data-toggle="tooltip"><i class="fa fa-floppy-o"
                                                                                              aria-hidden="true"></i>
    </button>
    </h1><br>

    {{ Form::model($item, ['route'=> ['groups.update', $item->id], 'class' => 'form-horizontal', 'role' => 'form'] ) }}
    {{ method_field('PATCH') }}
    @include('admin.product_groups.form')
    {{ Form::close() }}



    <script src="/admin/js/multiselect.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('#search').multiselect({
                search: {
                    left: '<input type="text"  class="form-control" placeholder="Search..." />',
                    right: '<input type="text"  class="form-control" placeholder="Search..." />',
                },
                fireSearch: function(value) {
                    return value.length > 3;
                }
            });
        });
    </script>

    <script>
        $(function () {
            $('#send-form').click(function () {
                $('form').submit();
            });

            var skip = 1;
            var flag = true;

            $('#category_search').on('change', function(event){
                skip = 0;
                flag = false;
                $('#search').find('option').remove();
                
                    $.get('/master/products_management/groups/{{ $item->id }}/edit?skip='+skip+'&cat_id='+$(this).val(), function (response) {
                        console.log(response);
                        if (response != 0){
                            html = '';
                            for (key in response) {
                                html += '<option value="' + response[key].id + '">' + response[key].localization[0].name + '</option>';
                            }
                            $('#search').append(html);
                            ++skip;
                            flag = true;
                        }
                    });
            });

            $('#search').scroll(function () {
                if (this.scrollHeight - this.scrollTop < 900 && flag == true) {
                    flag = false;
                    $.get('/master/products_management/groups/{{ $item->id }}/edit?skip='+skip+'&cat_id='+$("#category_search").val(), function (response) {
                        console.log(response);
                        html = '';
                        for (key in response) {
                            html += '<option value="' + response[key].id + '">' + response[key].localization[0].name + '</option>';
                        }
                        $('#search').append(html);
                        ++skip;
                        flag = true;
                    });
                }

            });
        })
    </script>
@stop