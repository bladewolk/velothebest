<p>Информация о покупателе <span
            class="label label-primary">{{ $item->user_id ? 'Пользователь сайта' : 'Не зарегистрирован'}}</span></p>
<table class="table" style="width: 600px;">
    <tr>
        <td>Имя</td>
        <td>{{ $item->user ? $item->user->name : $item->name }}</td>
    </tr>
    <tr>
        <td>Телефон</td>
        <td>{{ $item->user ? $item->user->phone : $item->phone }}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>{{ $item->user ? $item->user->email : $item->email }}</td>
    </tr>
    <tr>
        <td>Область</td>
        <td>{{ $item->user ? $item->user->region : $item->region }}</td>
    </tr>
    <tr>
        <td>Город</td>
        <td>{{ $item->user ? $item->user->city : $item->city }}</td>
    </tr>
    <tr>
        <td>Комментарий к заказу</td>
        <td>{{ $item->comment }}</td>
    </tr>
</table>
<table class="table">
    <tr>
        <th>Название</th>
        <th>Количество</th>
        <th style="text-align: center;">На складе</th>
        <th style="text-align: center;">Цена за 1 товар</th>
        <th style="text-align: center;">Цена</th>
        <th style="text-align: right;">Удалить</th>
    </tr>
    @foreach($item->products as $orderProduct)
        <tr>
            <td>
                <a href="/catalog/{{ $orderProduct->product->slug }}" target="_blank">
                    {{$orderProduct->product->localization->first()->name}}    
                </a>
            </td>
            <td style="width: 10%;">
                <input type="number" value="{{ $orderProduct->count }}" name="count[]" min="1" step="1"
                       max="{{ $item->status == 1 ? $orderProduct->product->in_stock + $orderProduct->count : $orderProduct->product->in_stock }}"
                       class="form-control">
                {{ Form::hidden('orderProductId[]', $orderProduct->id) }}
            </td>
            <td style="text-align: center;">
        

                {{ $orderProduct->product->in_stock }}
       
                @if($orderProduct->count > $orderProduct->product->in_stock && $item->status != 1)
                    <i class="fa fa-exclamation-triangle" aria-hidden="true" style="font-size: 24px; color: #d9534f;" data-toggle="tooltip" title="Недостаточно товара на складе">
                    </i>
                @endif
     
            </td>
            <td style="text-align: center;">{{ $orderProduct->price }}$
                ({{ number_format($orderProduct->price * $item->currency, 1, '.', ' ') }}грн)
            </td>
            <td style="text-align: center;">{{ $orderProduct->price * $orderProduct->count }}$
                ({{ number_format($orderProduct->price * $orderProduct->count * $item->currency, 1, '.', ' ') }}грн)
            </td>
            <td style="text-align: right;">
                <button class="btn btn-default fa fa-times delete-this" data-id="{{ $orderProduct->id }}"></button>
            </td>
        </tr>
    @endforeach
</table>

{{-- <p style="text-align: right;">Итого {{ $item->total_price }}$
    ({{ number_format($item->total_price * $item->currency, 1, '.', ' ') }}грн)
    @if ($item->status == 0)
     <span class="btn btn-primary">
            Принят
           {{ Form::checkbox('apply', $item->status, false, ['id' => 'check'.$item->id, 'class' => 'custom-check']) }}
            {{ Form::label('check'.$item->id, '1', ['class' => 'custom-label-for-check']) }}
        </span>
    @else
        {{ Form::hidden('status', $item->status)}}
    @endif
</p> --}}

{{-- {{ Form::select("status", $status, null, ['class'=>'form-control'] ) }} --}}

