@extends('admin.layout')

@section('main')

    <h1>{{ $title }}
        <a href="{{ route('order.index') }}" class="btn btn-primary" title="Назад"
           data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>

        <button id="send-form" class="btn btn-success" title="Сохранить" data-toggle="tooltip"><i class="fa fa-floppy-o"
                                                                                                  aria-hidden="true"></i>
        </button>

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>

    {{ Form::model($item, ['route'=> ['order.update', $item->id], 'class' => 'form-horizontal', 'role' => 'form'] ) }}
    {{ method_field('PATCH') }}
        @include('admin.orders.form')
    {{ Form::close() }}


    <script>
        $('#send-form').click(function () {
            $('form').submit();
        });

        $(function(){
            $('.delete-this').on('click', function(event){
                button = $(this);
                event.preventDefault();
                $.post('{{ route('order.destroy', ['id' => 1]) }}', {
                    _token: "{{ csrf_token() }}",
                    _method: "DELETE",
                    id: $(this).data('id')
                }, function(response){
                    button.closest('tr').remove();
                    location.reload();
                });
            });
        });
     
    </script>
@stop