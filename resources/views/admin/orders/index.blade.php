@extends('admin.layout')
@section('main')
    <h1>
        <span class="fa fa-newspaper-o"></span>
        {{ $title }}
        <a href="{{ route('master') }}" class="btn btn-primary" title="Назад" data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>

        @if($items->total() > 0)
            <span class="btn btn-warning" title="Выбрать все" data-toggle="tooltip"
                  id="check-all"><i class="glyphicon glyphicon-ok"
                                    aria-hidden="true"></i></span>
            <button class="delete-selected btn btn-danger"
                    title="Удалить выбранные" data-toggle="tooltip">
                <span class="glyphicon glyphicon-trash"></span></button>

            <button class="btn btn-primary fa fa-check" title="Отметить как выполненные" data-toggle="tooltip"></button>

            <span class="label label-primary" style="font-size: 14px;">
    			{{ $items->total() }}
                {{ Lang::choice('заказ|заказа|заказов', $items->total(), [], 'ru') }}
            </span>

        @else
            <div class="message message-warning">
                У вас пока нет заказов
            </div>
        @endif

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>

    @if ($items->total() > 0)

        {!! Form::open(['route' => ['order.destroy', 'destroy']]) !!}
        {{ method_field('DELETE') }}
        <table class="table">
            <tr>
                <th style="width: 20%;">
                    Название
                    <a href="{{ route('order.index', ['sort' => 'id', 'by' => 'asc']) }}" class="fa fa-sort-numeric-asc"></a>
                    <a href="{{ route('order.index', ['sort' => 'id', 'by' => 'desc']) }}" class="fa fa-sort-numeric-desc"></a>
                </th>
                <th>Покупатель</th>
                <th>Телефон покупателя</th>
                <th>Сумма покупки</th>
                <th>К-ство товаров</th>
                <th>Дата покупки</th>
                <th>
                    Статус
                    <a href="{{ route('order.index', ['sort' => 'status', 'by' => 'asc']) }}" class="fa fa-sort-numeric-asc"></a>
                    <a href="{{ route('order.index', ['sort' => 'status', 'by' => 'desc']) }}" class="fa fa-sort-numeric-desc"></a>
                </th>
                <th style="text-align: right;">Управление</th>
            </tr>
            @foreach ($items as $item)
            {{-- {{dd($item)}} --}}
                <tr style="background: {{ $statusColors[$item->status] }}">
                    <td style="vertical-align: middle">
                        {{ Form::checkbox('check[]', $item->id, false, ['id' => 'check'.$item->id, 'class' => 'custom-check']) }}
                        {{ Form::label('check'.$item->id, '1', ['class' => 'custom-label-for-check']) }}
                        <a class="custom_link" title="Просмотр заказа" data-toggle="tooltip"
                           href="{{ route('order.edit', $item->id) }}">
                            Заказ №{{ $item->id }}</a>
                    </td>
                    <td>
                        {{ $item->name }}
                    </td>
                    <td>
                        {{ $item->phone }}
                    </td>
                    <td>
                        {{ $item->total_price }}$ ({{ $item->total_price * $item->currency }}грн)
                    </td>
                    <td >
                        <span class="label label-info">
                            {{ $item->products->count() }}
                        </span>
                    </td>
                    <td>
                        {{ $item->created_at }}
                    </td>
                    <td>
                        <?php $disabled = false; ?>
                        @foreach($item->products as $product)
                            @if ($product->count > $product->product->in_stock && $item->status != 1 && $item->status != 2)
                                <span style="font-size: 14px;" class="label label-danger">НЕДОСТАТОЧНО ТОВАРА</span>
                                <?php $disabled = true ?>
                                @break
                            @endif
                        @endforeach

                        <span id="status{{$item->id}}">{{ $statusText[$item->status] }}</span>
                    </td>
                    <td style="text-align: right;">
                          {{--   <a title="Редактировать" href="{{ route('news.edit', ['id' => $item->id]) }}"
                               data-toggle="tooltip"
                               class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span></a> --}}
                            

                            <button class="btn {{ $item->status == 1 ? 'btn-primary' : 'btn-default'}} fa fa-cart-arrow-down change-status" title="Принят" data-toggle="tooltip" data-id="{{$item->id}}" data-status="apply" {{ $disabled ? 'disabled' : ''}} {{ $item->status == 2 ?'disabled':'' }}></button>
                            <button class="btn {{ $item->status == 2 ? 'btn-success' : 'btn-default'}} fa fa-check change-status" title="Отметить как выполнен" data-toggle="tooltip" data-id="{{$item->id}}" {{ $item->status == 2 ?'disabled':'' }}
                            data-status="done" {{ $disabled ? 'disabled' : ''}}></button>
                            <button class="btn {{ $item->status == 3 ? 'btn-danger' : 'btn-default'}} fa fa-exclamation-triangle change-status" title="Отменить заказ" data-toggle="tooltip" data-id="{{$item->id}}" data-status="cancel" {{ $disabled ? 'disabled' : ''}} {{ $item->status == 2 ?'disabled':'' }}></button>
                            

                            <span class="delete-this-id btn btn-default" title="Удалить"
                                  data-toggle="tooltip"><span
                                        class="glyphicon glyphicon-remove"></span>
                            </span>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ Form::close() }}

    @endif
        {{ $items->links() }}

    <script>
        $(function () {

            var flag = false;
            $(".delete-selected, .unconfirm-delete, .delete-this-id").click(function () {
                flag = !flag;
                if (flag)
                    move = "50%";
                else move = "-50%";

                $('.confirm-message').stop().animate({
                    top: move
                }, 300);
            });
            $(".confirm-delete").click(function () {
                $('form').submit();
            });
            $(".unconfirm-delete").click(function () {
                $('input[type=checkbox]:checked').prop('checked', false);
            });
            $(".delete-this-id").click(function () {
                $(this).closest('tr').find('input:checkbox').prop('checked', true);
            });
            $("#check-all").on('click', function () {
                $('input[type="checkbox"][name*="check"]').prop('checked', $('input[type="checkbox"][name*="check"]:not(:checked)').length > 0);
            });

            var status = {
                apply: 'btn-primary',
                done: 'btn-success',
                cancel: 'btn-danger'
            }

            $('.change-status').click(function(event){
                event.preventDefault();
                $.post("/master/order/"+ $(this).data('id'), {
                    _token: "{{ csrf_token() }}",
                    _method: "PATCH",
                    status: $(this).data('status')
                }, function(res){
                    if (res.status == 'apply' || res.status == 'cancel')
                        location.reload();

                    $("button[data-id='" + res.id +"']").removeClass('btn-primary btn-success btn-danger');
                    $("button[data-id='" + res.id +"']").addClass('btn-default');
                    $('#status'+res.id).html(res.statusText);
                    buttonChange = $("button[data-id='" + res.id +"'][data-status='"+ res.status +"']");
                    buttonChange.addClass(status[res.status]);
                    buttonChange.closest('tr').css({background:res.color});
                });
            });
       
        })
    </script>
@endsection