@extends('admin.layout')

@section('main')

    <h1>{{ $title }}
        <a href="{{ route('master') }}" class="btn btn-primary" title="Назад"
           data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>

        <button id="send-form" class="btn btn-success" title="Импорт" data-toggle="tooltip"><i
                    class="fa fa-cloud-upload"
                    aria-hidden="true"></i>
        </button>

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>

    {!! Form::open(['route'=> ['import.store'], 'class' => 'form-horizontal', 'role' => 'form', 'files' => true] ) !!}
    <div class="col-sm-4">
        {!! Form::file('catalog',array('class' => 'filestyle', 'data-value'=> '',
        'data-buttonText' => 'Выберите файл', 'data-buttonName' => 'btn-primary',
        'data-icon' => 'true' , 'multiple' => 'false', 'id' => 'catalog') ) !!}
        <span>Catalog.csv</span>
    </div>
    <div class="col-sm-4">
        {!! Form::file('price',array('class' => 'filestyle', 'data-value'=> '',
        'data-buttonText' => 'Выберите файл', 'data-buttonName' => 'btn-primary',
        'data-icon' => 'true' , 'multiple' => 'false') ) !!}
        <span>Price.csv</span>
    </div>
    <div class="col-sm-4">
        {!! Form::file('stock',array('class' => 'filestyle', 'data-value'=> '',
        'data-buttonText' => 'Выберите файл', 'data-buttonName' => 'btn-primary',
        'data-icon' => 'true' , 'multiple' => 'false') ) !!}
        <span>Stock.csv</span>
    </div>
    {!! Form::close() !!}

    <script>
        $('#send-form').click(function () {
            $('form').submit();
        });
    </script>
@stop