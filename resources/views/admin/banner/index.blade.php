@extends('admin.layout')
@section('main')
    <h1>
        {{-- <span class="fa fa-newspaper-o"></span> --}}
        {{ $title }}
        <a href="{{ route('master') }}" class="btn btn-primary" title="Назад" data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>
      {{--   <a href="{{ route('news.create') }}" id="send-form" class="btn btn-success"
           title="Cоздать баннер"
           data-toggle="tooltip"><span
                    class="fa fa-plus"></span></a> --}}

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>

    {!! Form::open(['route' => ['news.destroy', 'destroy']]) !!}
    {{ method_field('DELETE') }}
    <div class="container banner">
        <div class="row">
            <div class="col-sm-4">
                @if (isset($items[1]))
                    <a href="{{ route('banner.edit', 1) }}">
                        <div class="row" title="Редакировать" data-toggle="tooltip" 
                             style="background: url('/uploads/blocks/{{ $items[1] }}') center/cover no-repeat; height: 450px;
                                     border: 5px solid #0c6eb5;">
                            <span class="btn btn-danger fa fa-times delete-banner" data-id="1" title="Удалить изображение" data-toggle="tooltip"></span>
                        </div>
                    </a>
                @else
                    <a href="{{ route('banner.edit', 1) }}">
                        <div class="row" style="background: url('http://placehold.it/380x450') center/cover no-repeat; height: 450px; title="Добавить" data-toggle="tooltip"
                    border: 5px solid #0c6eb5;">
                        </div>
                    </a>
                @endif
            </div>
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="row">
                            @for($i = 2; $i < 6; $i++)
                                @if (isset($items[$i]))
                                    <div class="col-sm-6">
                                        <a href="{{ route('banner.edit', $i) }}">
                                            <div class="row" title="Редакировать" data-toggle="tooltip"
                                                 style="background: url('/uploads/blocks/{{ $items[$i] }}') center/cover no-repeat; height: 225px;
                                                         border: 5px solid #0c6eb5;">
                                                <span class="btn btn-danger fa fa-times delete-banner" data-id="{{ $i }}" title="Удалить изображение" data-toggle="tooltip"></span>
                                            </div>
                                        </a>
                                    </div>
                                @else
                                    <div class="col-sm-6">
                                        <a href="{{ route('banner.edit', $i) }}">
                                            <div class="row" title="Добавить" data-toggle="tooltip"
                                                 style="background: url('http://placehold.it/290x220') center/cover no-repeat; height: 225px;
                                                border: 5px solid #0c6eb5;">
                                            </div>
                                        </a>
                                    </div>
                                @endif
                            @endfor
                        </div>
                    </div>
                    <div class="col-sm-4">
                        @if (isset($items[6]))
                            <a href="{{ route('banner.edit', 6) }}">
                                <div class="row" title="Редакировать" data-toggle="tooltip"
                                     style="background: url('/uploads/blocks/{{ $items[6] }}') center/cover no-repeat; height: 450px;
                                             border: 5px solid #0c6eb5;">
                                        <span class="btn btn-danger fa fa-times delete-banner" data-id="6" title="Удалить изображение" data-toggle="tooltip"></span>
                                </div>
                            </a>
                        @else
                            <a href="{{ route('banner.edit', 6) }}">
                                <div class="row" title="Добавить" data-toggle="tooltip"
                                     style="background: url('http://placehold.it/190x450') center/cover no-repeat; height: 450px;
                                                    border: 5px solid #0c6eb5;">
                                </div>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

    {{--    {{ $items->links() }}--}}

    <script>
        $(function () {

            var flag = false;
            $(".delete-selected, .unconfirm-delete, .delete-this-id").click(function () {
                flag = !flag;
                if (flag)
                    move = "50%";
                else move = "-50%";

                $('.confirm-message').stop().animate({
                    top: move
                }, 300);
            });

            $('.delete-banner').on("click", function(event){
                event.preventDefault();
                $.post('/master/banner/'+$(this).data('id'), {
                    _method: 'DELETE',
                    _token: "{{ csrf_token() }}",
                }, function(response){
                    location.reload();
                });
            });

            $(".confirm-delete").click(function () {
                $('form').submit();
            });
            $(".unconfirm-delete").click(function () {
                $('input[type=checkbox]:checked').prop('checked', false);
            });
            $(".delete-this-id").click(function () {
                $(this).closest('tr').find('input:checkbox').prop('checked', true);
            });
            $("#check-all").on('click', function () {
                $('input[type="checkbox"][name*="check"]').prop('checked', $('input[type="checkbox"][name*="check"]:not(:checked)').length > 0);
            });
        })
    </script>
@endsection