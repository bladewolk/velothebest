<div class="form-group">
    {{ Form::label('url', 'Ссылка', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-9">
        {{ Form::text("url", $item ? $item->url : '', ['class'=>'form-control'] ) }}
    </div>
</div>

<div class="col-sm-12">
    <div class="form-group">
        {!! Form::label('image', 'Изображение', array('class'=>'col-sm-2 control-label') ) !!}
        <div class="col-sm-9">
            {!! Form::file('image' ,array('class' => 'filestyle', 'data-value'=> '',
            'data-buttonText' => 'Выберите файлы', 'data-buttonName' => 'btn-primary',
            'data-icon' => 'true' , 'multiple' => 'false') ) !!}
        </div>
    </div>
    @if($item)
        <div class="form-group">
            {!! Form::label('image'.$id, 'Текущее изображение', array('class'=>'col-sm-2 control-label') ) !!}
            <img  src="{{ asset('uploads/blocks/'.$item->image) }}" alt="slide">
        </div>
    @endif
</div>