@extends('admin.layout')
@section('main')
    <h1>
        <span class="fa fa-usd"></span>
        {{ $title }}
        <a href="{{ route('master') }}" class="btn btn-primary" title="Назад" data-toggle="tooltip"><span
                    class="fa fa-arrow-circle-left"></span></a>
        <button id="send-form" class="btn btn-success" title="Сохранить" data-toggle="tooltip"><i class="fa fa-floppy-o"
                                                                                                  aria-hidden="true"></i>
        </button>

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="btn alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </h1><br>

    {{ Form::model($item, ['route'=> ['currency.update', $item->id], 'class' => 'form-horizontal', 'role' => 'form'] ) }}
    {{ method_field('PATCH') }}
    <div class="form-group">
        {{ Form::label('currency_uah', 'USD (грн.)', ['class'=>'col-sm-1 control-label'] ) }}
        <div class="col-sm-3">
            {{ Form::number("currency_uah", $item->currency_uah, ['class'=>'form-control', 'min' => 1] ) }}
        </div>
    </div>
    {{ Form::close() }}

    <script>
        $(function () {
            $('#send-form').click(function () {
                $('form').submit();
            });
        })
    </script>
@endsection