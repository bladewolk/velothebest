<?php $count = 0; ?>
<ul class="nav nav-tabs" id="myTab">
    @foreach(LaravelLocalization::getSupportedLocales() as $key => $value)
        <li @if($count == 0) class="active" @endif data-toggle="tooltip" title="{{ $value['native']}}">
            <a href="#tab_{{ $key }}" data-toggle="tab">&nbsp;&nbsp;&nbsp;&nbsp;<span
                        class="flag flag-{{ $key }}"></span>
            </a>
        </li>
        <?php $count++ ?>
    @endforeach
</ul>
<?php $count = 0; ?>

<div class="form-group minicolors">
    {!! Form::label('color', 'Цвет', array('class'=>'col-sm-2 control-label') ) !!}
    <div class="col-sm-10">
        {{ Form::text('color', $item->color, ['class'=>'form-control minicolors-input']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('name', 'Название', ['class'=>'col-sm-2 control-label'] ) }}
    <div class="col-sm-10">
        {{ Form::text("name", $item->name, ['class'=>'form-control'] ) }}
    </div>
</div>

<div class="tab-content">
    @foreach(LaravelLocalization::getSupportedLocales() as $key => $value)
        @forelse($item->localization as $data)
            @if ($data['locale'] == $key)
                <div class="tab-pane @if ($count == 0) active @endif" id="tab_{{ $key }}">
                    <div class="tab-content">
                        <div class="form-group">
                            {{ Form::label('description' . $key, 'Текст лейбы', ['class'=>'col-sm-2 control-label'] ) }}
                            <div class="col-sm-10">
                                {{ Form::text("description[$key]", $data->description, ['class'=>'form-control'] ) }}
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @empty
            <div class="tab-pane @if ($count == 0) active @endif" id="tab_{{ $key }}">
                <div class="tab-content">

                    <div class="form-group">
                        {{ Form::label('description' . $key, 'Текст лейбы', ['class'=>'col-sm-2 control-label'] ) }}
                        <div class="col-sm-10">
                            {{ Form::text("description[$key]", '', ['class'=>'form-control'] ) }}
                        </div>
                    </div>

                </div>
            </div>
        @endforelse
        <?php $count++ ?>
    @endforeach
</div>

